Wizard Mizgog - My beads! Where are my beads?

---->Select an Option<----
Can I help you? 
Goodbye

Player -
	Can I help you?
Wizard Mizgog - Wizard Grayzag next door decided that he didn�t like me for some reason, so he enlisted an army of imps.
The imps stole all sorts of things. Most of there were things I don�t really care about like eggs, balls of wool, things like that �
But they stole my magic beads! There was a red one, a yellow one, a black one, and a white one.
The imps have spread out all over the kingdom by now. Could you get my beads back for me?
The imps will be all over the kingdom by now. You should kill any imps you find and collect any beads that they drop. I need a red one, a yellow one, a black one, and a white one.

//Returning with all 4 beads
Wizard Mizgog - How are you doing finding my beads?
Player - I�ve got all four beads.
Wizard Mizgog - Thank you! Give them here and I�ll check that they really are my beads, before I give you your reward. You�ll like it, it�s an amulet of accuracy.

    You give four coloured beads to Wizard Mizgog. * 