Pirate's treasure
Written by: Rock On
Video used: http://www.youtube.com/watch?v=SInMQzx1K1I

---------------------------------------------------------------------

(PLAYER CLICKS ON REDBEARD FRANK)

Redbeard Frank: Arr, Matey!

*OPTION MENU OPENS*

Option 1: I'm in search of treasure.
Option 2: Arr!
Option 3: Do you have anything for trade?

*Continueing with option 2*

Player: Arr!

Redbeard Frank: Arr!

*BACK TO OPTION MENU*
*CONTINUEING WITH OPTION 3*

Player: Do you have anything for trade?

Redbeard Frank: Nothin' at the moment, but then again the Customs
                        Agents are on the warpath right now.
                                   *EXIT*

*CONTINUEING WITH OPTION 1*

Player: I'm in search of treasure.

Redbeard Frank: Arr, treasure you be after eh? Well I might be able to 
                      tell you where to find some... For a price...

Player: What sort of price?

Redbeard Frank: Well for example if you can get me a bottle of rum... 
                             Not just any rum mind...

Redbeard Frank: I'd like some rum made on Karamja Island. There's no 
                                rum like Karamja Rum!

*OPTION MENU OPENS*

Option 1: Ok, I will bring you some rum.
Option 2: Not right now.

*Continueing with option 2*

Player: Not right now.

Redbeard Frank: Fair enough. I'll still be here and thirsty whenever you
                                  feel like helpin' out.
                                       *EXIT*

*CONTINUEING WITH OPTION 1*

Player: Ok, I will bring you some rum.

Redbeard Frank: Yer a saint, although it'll take a miracle to get it off
                                      Karamja.

Player: What do you mean?

Redbeard Frank: The Customs office has been clampin' down on the
              export of spirits. You seem like a resourceful young lad,
              I'm sure ye'll be able to find a way to slip the stuff past
                                         them.

Player: Well I'll give it a shot.

Redbeard Frank: Arr, that's the spirit!
                   *EXIT*


*AT KARAMJA*


(Player clicks on Luthas)

Luthas: Hello I'm Luthas. I run the banana plantation here.

*OPTION MENU OPENS*

Option 1: Could you offer me employment on your plantation?
Option 2: That customs officer is annoying isn't she?


*Continueing with option 2*

Player: That customs officer is annoying isn't she?

Luthas: Well I know here pretty well. She doesn't cause me
               any trouble any more.

Luthas: She doesn't even search my export crates any more.
        She knows they only contain bananas.

Player: Really? How interesting. Whereabouts do you send
                   those to?

Luthas: There is a little shop over in Port Sarim that buys them 
        up by the crate. I believe it is run by a man called Wydin.
                  *EXIT*

*Continueing with option 1*

Player: Could you offer me employment on your plantation?

Luthas: Yes, I can sort something out. There's crate ready to
                        be loaded onto the ship.

Luthas: You wouldn't believe the demand for bananas from
        Wydin's shop over in Port Sarim. I think this is the
            third crate I've shipped him this month...

Luthas: If you could fill it up with bananas, I'll pay you 30
                                 gold.
                                *EXIT*



(PLAYER BOUGHT KARAMJAN RUM AND PICKS BANANA'S)
*Player uses Karamjan rum on crate*
*DIALOQUE BOX*

You stash the rum in the crate.

*RIGHT CLICK CRATE AND CLICK FILL*

*DIALOQUE BOX*

You fill the crate with bananas.



(PLAYER CLICKS ON LUTHAS)

Player: I've filled a crate with bananas.

Luthas: Well done, here's your payment.

*30 coins appear in your inventory*
*Game tab says:*

Luthas hands you 30 coins.



(Player clicks on Redbeard Frank after being back from Karamja)

Redbeard Frank: Arr, Matey!

Redbeard Frank: Have ye brought some rum for yer ol' mate Frank?

(IF PLAYER DOESN'T HAVE THE RUM)

Player: No, not yet.

Redbeard Frank: Not suprising, 'tis no easy task to get it off Karamja.

Player: What do you mean?

Redbeard Frank: The Customs office has been clampin' down on the
              export of spirits. You seem like a resourceful young lad,
              I'm sure ye'll be able to find a way to slip the stuff past
                                         them.

Player: Well, I'll give it another shot.

Redbeard Frank: Was there anything else?

*OPTION MENU OPENS*

Option 1: Arr!
Option 2: Do you have anything for trade?


(Player clicks on Wydin)

Wydin: Welcome to my food store! Would you like to buy
                         anything?

*OPTION MENU "A" OPENS*

Option 1: Yes please.
Option 2: No, thank you.
Option 3: What can you recommend?
Option 4: Can I get a job here?

*Continueing with option 1*

Player: Yes please.

*SHOP OPENS*

*Continueing with option 2*

Player: No, thank you.
            *EXIT*

*Continueing with option 3*

Player: What can you recommend?

Wydin: We have this really exotic fruit all the way from Karamja.
                          It's called a banana.

*SECOND OPTION MENU OPENS*

Option 1: I'll try one.
Option 2: I don't like the sound of that.

*Continueing with option 2*

Player: I don't like the sound of that.

Wydin: Well, it's your choice, but I do recommend them.
                   *EXIT*

*Continueing with option 1*

Player: I'll try one.

Wydin: Great. You might as well take a loot at the rest
                      of my wares as well.
                         *SHOP OPENS*

*continueing with option 4, option menu A*

Player: Can I get a job here?

Wydin: Well, you're keen, I'll give you that. Okay I'll give you
            a go. Have you got your own white apron?

(IF PLAYER DON'T HAVE WHITE APRON)

Player: No, I haven't

Wydin: Well, you can't work here unless you have a white apon.
            Health and safety regulations, you understand.

Player: Where can I get one of those?

Wydin: Well, I get all of mine over at the clothing shop in Varrock.
                         They sell them cheap there.

Wydin: Oh, and I'm sure that I've seen a spare one over in
     Gerrant's fish store somewhere. It's the little place just
                           north of here.
                              *EXIT*

(IF PLAYER GOT WHITE APRON)

Player: Yes, I have one right here.

Wydin: Wow - you are well prepared! You're hired. Go through
              to the back and tidy up for me, please.



(IF PLAYER CLICKS ON DOOR WITHOUT WEARING THE WHITE APRON)

Wydin: Can you put your white apron on before going in there,
                              please?
                               *EXIT*

*Search the crate*
*Game tab says:*

There are a lot of bananas in the crate.
You find your bottle of rum in amongst the bananas.

*Rum placed in your inventory*


(Player clicks on redbeard frank while having the rum in inventory)

Redbeard Frank: Arr, Matey!

Redbeard Frank: Have ye brought some run for yer ol' mate Frank?

Player: Yes, I've got some.

Redbeard Frank: Now a deal's a deal, I'll tell ye about the treasure. I
                 used to serve under a pirate captan called One-Eyed
                                       Hector.

Redbeard Frank: Hector were very succeful and became very rich.
               But about a year ago we boarded by the Customs
                                and Excise Agents.

Redbeard Frank: Hector were killed along with many of the crew, I were
                  one of the few to escape and I escaped with this.

*DIALOQUE BOX*

Frank happily takes the rum... ... and hands you a key.

Redbeard Frank: This be Hector's key. I believe it opens his chest in his
                     old room in the Blue Moon Inn in Varrock.

Redbeard Frank: With any luck his treasure will be in there.

*OPTION MENU OPENS*

Option 1: Ok thanks, I'll go and get it.
Option 2: So why didn't you ever get it?

*Continueing with option 2*

Player: Ok thanks, I'll go and get it.

Redbeard Frank: I'm not allowed in the Blue Moon Inn. Apparently I'm
                        a drunken trouble maker.
                                *EXIT*

*Continueing with option 1*

Player: Ok thanks, I'll go and get it. 
     *EXIT*




(Player clicks "open" on locked chest in Blue Moon Inn)
*GAME TAB SAYS:*

The chest is locked.

(Player uses key on locked chest)
*GAME TAB SAYS:*

All that's in the chest is a message...

*PLAYER CLICKS SEARCH ON CHEST*
*GAMETAB SAYS:*

You take the message from the chest.

*THE PIRATE MESSAGE APPEARS IN YOUR INVENTORY*
*WHEN READING A "CLUE SCROLL INTERFACE" POPS UP*

Visit the city of the White Knights. In the park,
Saradomin points to the X which marks the spot.


(PLAYERS CLICKS ON SPADE AND DIGS ON THE SPOT OF THE TREASURE)
*A GARDENER STARTS ATTACKING YOU SAYING:*

First moles, now this! Take this, vandal!

*ARROW APPEARS ABOVE HIS HEAD*


(PLAYER DIGS AGAIN)
(GAME TAB SAYS:)

You dig a hole in the ground...


*QUEST REWARD INTERFACE POPS UP*

               Congratulations!
You have completed the Pirate's Treasure Quest!

               You are awarded:
                2 Quest Points
           One-Eyed Hector's Treasure
                     Chest