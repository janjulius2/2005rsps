Cook: What am I to do?
 
Player: 
=Select an Option=
What's wrong?
Can you make me a cake?
You Don't look very happy.
Nice hat!

Player: What's Wrong?

Cook: Oh dear, oh dear, oh dear, I'm in a terrible terrible mess! It's The Duke's birthday today, and I should be making him a lovely big birthday cake.

Cook: I've forgotten to buy the ingredients. I'll never get them in time now. He'll sack me! What will i do? I have four children and a goat to look after. Would you help me? Please?
 
Player:
=Select an Option=
I'm always happy to help a cook in distress.
I can't right now maybe later.

Player: Yes, I'll help you.

Cook: Oh thank you, thank you. I need Milk, an egg and flour. I'd be very grateful if you can get them for me.

Player: So where do I find these ingredients then?

Player:
=Select an Option=
Where do I find some flour?
How about milk?
And eggs? Where are they found?
Actually, I know were to find this stuff.

Player:  Actually, I know were to find this stuff.

Cook: How are you getting on with finding the ingredients?

Player: Here's a bucket of milk.

Player: Here's a pot of flour.

Player: Here's a fresh egg.

Cook: You've brought me everything I need! I am saved! Thank you!

Player: So do I get to go to the Duke's party?

Cook: I'm afraid not, only the big cheeses get to dine with the Duke.

Player: Well, maybe one day I'll be important enough to sit on the Duke's table.

Cook: Maybe, but I won't be holding my breath.


===Reward===
1 Quest point

300 Cooking xp