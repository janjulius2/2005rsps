Underground Pass Dialogue

Start in Ardougne, speak to king Lathas

P: Hello King Lathas.

L: Adventurer, thank Saradomin for your arrival!

P: Have your scouts found a way through the mountains?

L: Not quite, we found a path to where we expected to find the Well of Voyage, an ancient portal to west RuneScape. (Click here to continue) However during recent times a cluster of cultists have settled there, run by a madman named Iban.

P: Iban?

L: A crazy loon who claims to be the son of Zamorak. (Click here to continue) Go meet my main tracker, Koftik. He will help you. He waits for you at the west side of Ardougne. (Click here to continue) We must find a way through these caverns if we are to stop my brother Tyras.

P: I�ll do my best Lathas. 

L: A warning traveller, the underground pass is lethal. We lost many men exploring those caverns. Go prepared with food and armour or you won�t last long. 


Head out of the castle, speak to Koftik

If you talk to Koftik before starting the quest:


�



P: Hello there, are you the King�s scout?

K: That I am, brave adventurer. King Lathas informed me that you need to cross these mountains. (Click here to continue) I�m afraid you�ll have to go through the ancient underground pass�

P: That�s OK, I�ve travelled through many a cave in my time. 


K: These caves are different� They�re filled with the spirit of Zamorak! (Click here to continue) You can feel it as you wind your way round the stalagmites� an icy chill that penetrates the very fabric of your being� (Click here to continue) Not so many travellers come down here these days� but there are some who are still foolhardy enough.

P:  -I�ll take my chances.
 -Tell me more. ..

1. If the player chooses I�LL TAKE MY CHANCES�

The conversation will stop.

2. If the player chooses TELL ME MORE�

K: I remember seeing one such warrior. Going by the name of Randas� he stood tall and proud like an Elven King. (Click here to continue) �That same pride made him vulnerable to Zamorak�s calls. Randas� worthy desire to be a great and mighty warrior also made him corruptible to Zamorak�s promises of glory. (Click here to continue) �Zamorak showed him a way to achieve his goal by appealing to that most base and dark nature� that resides in all of us.

P: What happened to him?

K: No one knows�

If you talk to koftik AGAIN for both scenarios:

K: I know it�s scary in there, but you�ll have to go in alone. I�ll catch up as soon as I can. 

Enter the cave, you will receive the message: 

You cautiously enter the cave�

At the same time, you unlock the music track Cursed. Continue on in the caverns, climb over the rockslide. Take the northern pass, climb the rockslides. If you fail, you get hit 3 damage and you will receive a message:

You climb onto the rock�
�but you slip back down


If you succeed: 

You climb onto the rock�
�and step down the other side. 

Talk to Koftik:

P: Koftik, how can we cross the bridge?

K: I�m not sure, seems as if others were here before us though� (Click here to continue) I found this cloth amongst the charred remains of some arrows.

P: Charred arrows? They must have been trying to burn something. Or someone! (Click here to continue) interesting� we better keep our eyes open.

K: I have also found the remains of a book�

P:  - Not to worry, probably just litter.
- WHAT DOES IT SAY?


K: It seems to be written by the adventurer Randas. It reads� (Click here to continue)

The book will open up on screen. 

Page 1: 

It began as a whisper in my ears. Dismissing the sounds as the whistling of the wind I steeled myself against these forces, and continued on my way. 

But the whispers became moans. 

Page 2:

At once fearsome and enticing like the call of some beautiful siren.

Join us!

Our greatness lies within you, but only Zamorak can unlock your potential�

When you close the book, you will receive a damp cloth; however, there is no message. Use your arrows with the damp cloth to make a fire (insert metal of the arrow used here) arrow. Broad, poisoned and ice arrows WILL NOT WORK:

You wrap the damp cloth around the arrow head�


You can repeat the process of collecting fire arrows an infinite amount of times. Use these arrows on the fire next to Koftik. No message will be received. 


Head north and fire at the Guide rope. 

If you succeed: 

You fire your arrow at the rope supporting the bridge�
The arrow impales the rope support. 

 After you get it, you will AUTOMATICALLY walk back to the bridge (where koftik is) and it will fall. See video here: at 6:10.

The bridge falls.
You rush across the bridge. 

Pick up plank and head south until you reach a crevice. On the northern crevice, there will be a spike on each side, use a rope with it.

You throw the rope around the opposite peg�
You balance across the rope which falls after you cross.
You�ve lost your rope. 

You obviously cross automatically and the rope disappears. Continue east. Climb down the rocks, you will receive the message:

You climb down the rock�
�and step down the other side. 

Once again, cross the next rocks.

You climb down the rock�
�and step down the other side. 

Randomly, while in the tunnels, you COULD receive a different message at the end.
A few I found:

-Blood, pain and hate.
-Death is only the beginning.
-The power of the gods could be yours.
-Make them all pay!
-You feel a cold shudder run down your spine. 
-I�ll swallow your soul.
-Iban will save you� He�ll save us all. 
-Make them all pay!
-I will release you�
-Join us!
-Hear me�
-I see you adventurer� you can�t hide. 
-I�ll swallow your soul. 



THE GRID FLOORS:

Talk to Koftik:

P: Hello Koftik.

K: Do you hear them? The voices tell me things.

P: Are you OK?
K: The path of the righteous man is beset on all sides by the iniquities of the selfish and the tyranny of evil men.

P: Tyranny of the righteous? What?

K: So many paths to choose� Here we must all take our own path.

Now the path is different for everyone. You must walk across the traps.

If you fall: 

It�s a trap!
You fall onto the spikes.

You unlock the music track: underground pass and get hit for 15 damage.  The player says: Ouch! To leave, you must climb the protruding rocks.

You crawl out of the pit.
 
If you pass the traps, there is NO MESSAGE.

When completed the 5*5 traps, pull a level. 

You pull the lever�
The gate opens. 

The player goes under. 
The gate closes.

Continue towards the well. There are random spikes that will hit you between the portcullis and the well. South of the well, between two skeletons, there is a book, pick it up, you will receive the message:

The journal is old and worn.
It reads�


Page 1:

I came to cleanse these mountain passes of the dark forces that dwell here. I knew my journey would be treacherous, so I deposited Spheres of Light in some of the tunnels. These spheres are a beacon of safety for all who come. The spheres were created by





Page 2: 

Saradominist mages.
When held they boost our faith and courage. I still feel�

Iban relentlessly
Tugging�

At my weak soul�

Bringing out any innate

Page 3:

Goodness to one�s heart, illuminating the dark caverns with the light of Saradomin, bringing fear and pain to all who embrace the dark side. My men are still reelled by �Iban�s will� � it seems as if their pure hearts bar them from entering Iban�s realm. My turn

Page 4:
Has come. I hare not admit it to my loyal men, but I fear for the welfare of my soul. 

Pick up the 4 orbs of light. 

On the 4th orb, most south, there is a trap.

Right click �search orb of light�

The rock appears to move slightly as you touch it� It�s a..
[5:39:41 AM] Matthew Colucci: The rock appears to move slightly as you touch it� It�s a trap!

(In red) Do you want to try and disarm it?

-YES, I�LL GIVE IT A GO!
-No thanks, I�ll leave it alone. 


You try to disarm the trap�
� and succeed long enough to take the Orb.

Go back to the zombies and the fire, there is a furnace. Use the orbs with the furnace.

You throw the glowing orb into the furnace�
Its light quickly dims and then dies. 

You can now enter the well.

You climb into the well�
You feel the grip of icy hands all around you�
�slowly dragging you further down into the caverns.

When you reach the bottom, there is a crate (middle one) with food inside, when you search them, you will receive the message:

You search the crate�
�inside you find some food. 

OR if you search the empty crates:

You search the crate but find nothing (SEE VIDEO) 

You will receive: 

-2 salmon
-2 meat pies
-2 tuna

Head to the zombies in the cages (west)

If you attempt to pick the locks to the cages:

You attempt to pick the lock�
You manage to pick the lock.
You walk through.
The cage slams shut behind you. 

In the most south-east cage, there is a mud pile. If you use a spade with it, you will receive the message:

You dig into the pile of mud�
�and find it�s a filled in tunnel!
You push your way through the tunnel. 

And you end up on the other side. Next, cross the ledge, you will receive the message:

You put your foot on the ledge and try to edge across�

Go onto the bridge, there is a gap, jump it.

You start to cross the rock bridge�
�and make it. 

You must head to the other side with the giant bats.  South there is an obstacle pipe, go through it, you will receive the message:

You crawl through the pipe. 

 If you search the cage with the unicorn, you will receive the message:

You search the cage�
You find a loose railing lying on the floor. 

And you will obtain a piece of loose railing. Go around to the other side of the cage and push the boulder.

Player: It�s too heavy to move with just my hands. 

Use the piece of railing with the boulder, you will receive the message:

You use the pole as leverage�
�and tip the boulder onto its side�
�It tumbles down the slope�

Player: I heard something breaking. 

Search the smashed cage, you will receive the message:

The unicorn was killed by the boulder.

Player: All that remains is a damaged horn. 

You will receive a unicorn horn. Now pass through the tunnel entrance just north of the gate. Continue down east, talk to Sir Jerro, he will give you food. Kill all three of the warriors.  


Before killing them:

If you talk to Sir Carl/Sir Henry:

P: Hello there. 

C/H: Take care down here. Evil things are abroad. 

If you talk to Sir Jerro:

P: Hello Paladin.

J: Traveller, what are you doing in this most unholy place?

P: I�m looking for a safe route through the caverns under order of King Lathas. 

J: Ou�ve done well to get this far, traveller, here eat�

P: Great, thanks a lot.

You will receive the following:

-Prayer potion (2)
-Stew
-2* Meat pie
-2* bread
- Attack potion (2)

Then you can murder these men. They will each drop a paladin�s badge. When you finish killing them, use the plank with flat rock, you will receive the message:

You place the plank across the flat rock�
�and quickly walk over.
 
If you search the stone structure:

You search the stone structure.
On the side you find an old inscription,
It reads�


A scroll will appear on the screen (much resembling that of a clue scroll):

While I sense the soft beating of a
Good heart I will not open.
Feed me three crests of the blessed
And the creatures remains.
Throw them to me as an offering�
A gift of hatred, a token.
Then finally rejoice as all goodness
Dies in my flames.

This obviously makes reference to the unicorn horn and the three badges from the paladins. Use these objects with it.

You throw the unicorn horn into the flames�
You hear a howl in the distance.

After the fourth object:

You throw the unicorn horn into the flames�
You hear a howl in the distance.
It sounded like it came from the skull above the door. 

Open the door. 

3:01

