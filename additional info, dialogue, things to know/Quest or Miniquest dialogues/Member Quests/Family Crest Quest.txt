-Talking to Dimintheis
Dimintheis: Hello. My name is Dimintheis, of the noble family Fitzharmon.

---->Select an Option<----
Why would a nobleman live in a dump like this?
You're rich then? Can I have some money?
Hi, I am a bold adventurer.

-third option
Player: Hi, I am a bold adventurer.
Dimintheis: An adventurer hmmm? How lucky. I may have an adventure for you. I desperately need my family crest returning to me. It is of utmost importance.

---->Select an Option<----
Why are you so desperate for it?
So where is this crest?
I'm not interested in that adventure right now.

-second option
Player: So where is this crest?
Dimintheis: Well, my three sons took it with them many years ago when they rode out to fight in the war against the undead necromancer and his army in the battle to save Varrock.
For many years I had assumed them all dead, as I had heard no word from them, but recently I heard word that my son Caleb is alive and well, and trying to earn his fortune as a great fish chef in the lands beyond White Wolf Mountain, although I know not where.

---->Select an Option<----
Ok, I will help you.
I'm not interested in that adventure right now.

-first option
Player: Ok, I will help you.
Dimintheis: I thank you greatly adventurer!
You might want to focus your search around the cost. I hear that good chefs require freshly caught ingredients.
Also... if you find Caleb, or my other sons... please... let them know their father still loves them...

-Talking to Caleb
Caleb: Who are you? What are you after?

---->Select an Option<----
Are you Caleb Fitzharmon?
Nothing, I will be on my way.
I see you are a chef, will you cook me anything?

-first option
Player: Are you Caleb Fitzharmon?
Caleb: Why... yes I am, but I don't believe I know you... how did you know my name?
Player: I have been sent by your father. He wishes the Fitzharmon Crest to be restored.
Caleb: Ah... well... hmmm... yes... I do have a piece of it anyway... 

---->Select an Option<----
Uh... what happened to the rest of it?
So can I have your bit?

-first option
Player: Uh... what happened to the rest of it?
Caleb: Well... my brothers and I had a slight disagreement about it... we all wanted to be heir to my fathers' lands, and we each ended up with a piece of the crest.
None of us wanted to give up our rights to our brothers, so we didn't want to give up our pieces of the crest, but none of us wanted to face our father by returning to him with an incomplete crest...
We each went our separate ways many years past, none of us seeing our father or willing to give up our fragments.
Player: So can I have your bit?
Caleb: Well, I am the oldest son, so by the rules of chivalry, I am most entitled to be the rightful bearer of the crest.
Player: It's not really much use without the other fragments is it though?
Caleb: Well that is true... perhaps it is time to put my pride aside... I'll tell you what: I'm struggling to complete this fish salad of mine,
so if you will assist me in my search for the ingredients, then I will let you take my piece as reward for your assistance.
Player: So what ingredients are you missing?
Caleb: I require the following cooked fish: Swordfish, Bass, Tuna, Salmon and Shrimp.

---->Select an Option<----
Ok, I will get those.
Why don't you just give me the crest?

-first option
Player: Ok, I will get those.
Caleb: You will? It would help me a lot!

-Talking to Caleb AGAIN
Caleb: How is the fish collecting going?
Player: Got them all with me.

You exchange the fish for Caleb's piece of the crest.

---->Select an Option<----
Uh... what happened to the rest of it?
Thank you very much!

-first option
Player: Uh... what happened to the rest of it?
Caleb: Well... my brothers and I had a slight disagreement about it... we all wanted to be heir to my fathers' lands, and we each ended up with a piece of the crest.
None of us wanted to give up our rights to our brothers, so we didn't want to give up our pieces of the crest, but none of us wanted to face our father by returning to him with an incomplete crest...
We each went our separate ways many years past, none of us seeing our father or willing to give up our fragments.
Player: So do you know where I could find any of your brothers?
Caleb: Well, we haven't really kept in touch... what with the dispute over the crest and all... I did hear from ym brother Avan a while ago though... 
He said he was on some kind of search for treasure, or gold, or something out near Al Kharid somewhere.
Avan always had expensive tastes, so you might try asking the gem trader for his whereabouts.

-second option
Player: Thank you very much!
Caleb: You're welcome.

-Talking to Gem trader
Gem trader: Good day to you, traveller. Would you be interested in buying some gems? to give up our fragments.

---->Select an Option<----
Yes, please.
No, thank you. 
I'm in search of a man named Avan Fitzharmon.

-third option
Gem trader: Fitzharmon, eh? Hmmm... If I'm not mistaken, that's the family name of a member of the Varrockian nobility.
You know, I have seen someone of that persuasion around here recently... wearing a 'poncey' yellow cape, he was.
Came in here all la-di-dah, high and mighty, asking for jewellery made from 'perfect gold' - whatever that is - like 'normal' gold just isn't good enough for 'little lord fancy pants' there!
I told him to head to the desert 'cos I know there's gold out there, in them there sand dunes. And it's not up to his lordship's high standards of 'gold perfection', then...
Well, maybe we'll all get lucky and the scorpions will deal with him.


-Talk to Man (with yellow cape)
---->Select an Option<----
Why are you hanging around in a scorpion pit?
I'm looking for a man named Avan Fitzharmon.

-second option
Player: I'm looking for a man... his name is Avan Fitzharmon.
Man: Then you have found him. My name is Avan Fitzharmon.
Player: You have a part of your family crest. I am on a quest to retrieve all of the fragmented pieces and restore the crest.
Man: Ha! I suppose one of my worthless brothers has sent you on this quest then?
Player: No, it was your father who has asked me to do this for him.
Man: My... my father wishes this? Then that is a different matter. I will let you have my crest shard, adventurer, but you must first do something for me.
There is a certain lady I am trying to impress. As a man of noble birth, I can not give her just any old trinket to show my devotion. What I intend to give her, is a golden ring, embedded with the finest precious red stone available, and a necklace to match this ring. The problem for me, is that
not just any old gold will be suitable. I seek only the purest, the most high quality of gold - what I seek, if you will, is perfect gold.
None of the gold around here is even remotely suitable in terms of quality. I have searched far and wide for the perfect gold I desire, but have had no success so in finding it I am afraid. 
If you can find me my perfect gold, make a ring and necklace from it, and add rubies to them, I will gladly hand over my fragment of the family crest to you.
Player: Can you give me any help on finding this 'perfect gold'?
Man: I thought I had found a solid lead on its whereabouts when I heard of a dwarf who is an expert on gold who goes by the name of 'Boot'.
Unfortunately he has apparently returned to his home, somewhere in the mountains, and I have no idea how to find him.
Player: Well, I'll see what I can do.

-Talking to Boot
Boot: Hell tall person.

---->Select an Option<----
Hello. I'm in search of very high quality gold. 
Hello short person.
Why are you called boot?

-first option
Player: Hello, I'm in search of very high quality gold.
Boot: High quality gold eh? Hmmm... Well, the very best quality gold that I know of can be found in an underground ruin near witchaven.
I don't believe it's exactly easy to get to though...

-Talking to Man
Man: So how are you doing getting me my perfect gold jewelry?
Player: I have the ring and necklace right here.

You hand Avan the perfect gold ring and necklace.
Man: These... these are exquisite! EXACTLY what I was searching for all of this time! Please, take my crest fragment!
Now, I suppose you will be wanting to find my brother Johnathon who is in possession of the final piece of the family's crest?
Player: That's corrct.
Man: Well, the last I heard of my brother Johnathon, he was studying the magical arts, and trying to hunt some demon or other out in The Wilderness.
Unsurprisingly, I do not believe he is doing a particularly good job of things, and spends most of his time recovering from his injuries in some tavern or other near the eastern edge of The Wilderness. You'll probably find him still there.
Player: Thanks Avan.

-Talking to Johnathon
Player: Greetings. Would you happen to be Johnathon Fitzharmon?
Johnathon: That... I am...
Player: I am here to retrieve your fragment of the Fitzharmon family crest.
Johnathon: The... poison... it is all... too much... My head... will not... stop spinning...

Sweat is pouring down the Johnathons' face.

-use antipoison with johnathon
You use the potion on Johnathon.
Johnathon: Ooooh... thank you... Wow! I'm feeling a lot better now! That potion really seems to have done the trick!
How can I reward you?
Player: I've come here for your piece of the Fitzharmon family crest.
Johnathon: You have? Unfortunately I don't have it any more... in my attempts to slay the fiendish Chronozon, the blood demon, I lost a lot of equipment in our last battle when he bested me and forced me away from his den. He probably still has it now.

---->Select an Option<----
So is this Chronozon hard to defeat?
Where can I find Chronozon?
So how did you end up getting poisoned?

-first option
Player: Where can I find Chronozon?
Johnathon: The fiend has made his lair in the Wilderness below the Obelisk of Air.

---->Select an Option<----
So is this Chronozon hard to defeat?
So how did you end up getting poisoned?
I will be on my way now.

-first option
Player: So is this Chronozon hard to defeat?
Johnathon: Well... you will have to be a skilled Mage to defeat him, and my pwoers are not good enough yet. You will need to hit him once with each of the four elemental spells of death before he will be defeated.

---->Select an Option<----
Where can I find Chronozon?
So how did you end up getting poisoned?
I will be on my way now.

-second option
Player: So how did you end up getting poisoned?
Johnathon: Those accursed poison spiders that surround the entrance to Chronozon's lair... I must have take a nip from one of them as I attempted to make my escape.

---->Select an Option<----
So is this Chronozon hard to defeat? 
Where can I find Chronozon?
I will be on my way now.

-third option
Player: I will be on my way now.
Johnathon: My thanks for the assistance adventurer.

-Talking to Dimintheis
Player: I have retrieve your crest.
Dimintheis: Adventurer... I can only thank you for your kidness, although the words are insufficient to express the gratitude I feel!
You are truly a hero in every sense, and perhaps your efforts can begin to patch the wounds that have torn this family aprt...
I know not how I can adequately reward you for your efforts... although I do have the mystical gauntlets, a family heirloom that through some power unknown to me, have always returned to the head of the family whenever lost, or if the owner has died. 
I will pledge these to you, and if you should lose them return to me, and they will be here.
They can also be granted extra powers. Take them to one of my sons, they should be able to imbue them with a skill for you.

