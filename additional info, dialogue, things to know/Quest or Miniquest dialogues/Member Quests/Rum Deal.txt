Rum Deal Quest Dialogue:

-Talk to Pirate Pete

Player: Hello there!

Pirate Pete: Mornin'
Hey...you're an adventurer, right?
Player: Yes I am!
Got any quests for me?

Pirate Pete: Yeah, I do, as a matter of fact!
(Ahem.)
I am a poor, dispossessed nobleman, forced by circumstance to lurk in the middle of nowhere, soliciting help from passers-by.
You see, my fiendish half-brother has seized my estates and forced me into exile.
The simple lemon farmers suffer under his tyrannous yoke, and only a brave adventurer can lift his iron boot from the neck of the poor.
To reclaim my lands I will need to have my family sword returned to me so that I may present it as proof of my rulership.
Will you help me find my family sword?

---->Select an option<----
Yes!
No.

Option 1. 

Player: Yes! Your uncorroborated sob story has touched my heart. When do we set off?

Pirate Pete: You'll help! Wonderful!
But, alas, my half brother has a powerful ally, the might demon...
Err...
Err...Barrelor!
Yes the mighty, fearsome, tall, deadly, oaken, round demon Barrelor the Destroyer.

Player: Barrelor?

Pirate Pete: That's what I said!
Barrelor is an awesome opponent, and to reclaim my family sword you will need to defeat him, for he guards it within the deadly Trapped Pit of Barrelor.
Wanna give it a shot?

---->Select an option<----
Of Course, I fear no demon!
Not a chance, this sounds too dangerous.

Option 1. 
Player: Of Course, I fear no demon!

Party Pete: Atta girl/boy (note: depends on gender)
When I am reinstated in my rightful place, I will not be a very wealthy man, as my half-broher has squandered my family fortune.
However, I will gladly give you every bent penny of what is left, and starve in the gutter with my many, many adorable children, if you say you will help me.

---->Select an option<----
Nonsense! Keep the money!
Great, Ill take the cash in used coins please.

Option 1.

Player: Nonsense! Keep the money! I will dispose of this evil half-brother of yours and leave you what little money is left to feed your family.

Pirate Pete: Wonderful! Just pick up your diversion and we'll leave!

Player: What diversion?

Player: (in chat box) Ow! (says out loud)
*yellow birds fly above head*

-Enter Cut scence-

Captain Braindeath: Arrr... 'Tis looking' bleak...

Pirate Pete: Cap'n!
Good news Cap'n!
I found us a hero down by the docks!

Captain Braindeath: Be they heroic, brave and true?

Pirate Pete: Aye! They also be gullible, tied up and unconscious!
They were willing to help out some random strangers with a good enough sob story, so I smacked them with a bottle and towed them over.

Captain Braindeath: Brilliant! The island's location will remain a secret!
Bring 'em here and wake 'em up.
We may make it though this yet...

-Exit cut scence-

*Yellow birds still flying above players head*

Captain Braindeath: Are ye alright, lass?

Player: Ohhhh... My head...
It feels like someone has smacked me one with a bottle...

Captain Braindeath: Arr.. Those devils gave ye a nasty knock when ye came to aid us.
But now yer here we'll run those evil brain-eatin' dogs off the island fer good!

Player: What? What is going on here?
I can't seem to remember anything beyond chatting to a man at the docks.

Captain Braindeath: Arr. Well, lass, that would be Pete, one of my men.
He's been lookin' fer heroes like yerself to aid us in our peril.
When ye arrived ye took a nasty knock to the head, so ye probably don't remember agreein' to help us out. But I swear that ye did.

Player: Okay... I'll buy that. It sounds like something I would do.
So where am I, and what is going on?

Captain Braindeath: Yer on Braindeath Island!
Where it lies is a secret, because ye are standin' in the brewery of Cap'n Braindeath, purveyor of the most virriloic alcoholic beverages in the world!

Player: Wow!

Captain Braindeath: I am notorious alchemist Cap'n Braindeath, and this whole operation be my idea!
With my crew of sturdy, upright pirate brewers, we sail the seven seas, distributing cheap 'alcohol' to all and sundry.
Well, fer a price, at any rate.

Player: Oooh!

Captain Braindeath: These be dark times, though, lass. See, a week ago we awoke to find ourselves besieged.
The lads and I held them off so far, but 'tis only a matter of time before they sweep through the buildin' and put us all to the sword.

Player: Who?

Captain Braindeath: Them!

The captain points out the window... 
Click here to continue

-Enter Cut Scence-

* Zombie Pirate roaming around*

*Rum Deal Banner appears on screen* Between Rum and Deal there is a Skull.*

* Zombies move around more*
-Exit Cut Scence-

Player: Are they...
...protesting?

Captain Braindeath: Arr, lass! That they are!
Day and night they seek to break out with their chantin', and their singin' and their passive resistance!
Seems they lost their fightin' spirit after the first few days. Now most of them just protest all the time.

Player: So, what do you want me to do?

Captain Braindeath: Well, me and the lads got our heads together and decided that if we can get their Cap'n drunk enough, perhaps they'll stop protesting'.
If that happens, we'll slip out the back and set up shop somewhere else.

Player: Well, how can I help?

Captain Braindeath: Well, first of all, we need someone to go out the front and grow us some Blindweed.
'Tis one of the ingredients of our 'rum'.
The only problem is that those rottin' fiends have torn up and destroyed all but one of the Blindweed Patches.
Here lass, I'll give ye the seed you'll need fer growin' the herb. Help yourself to the gardenin' equipment in the basement.
I'll warn ye again those devils are sat right on top of the patch.
Try Hecklin' 'em from a distance. Those Swabs may talk a good fight, but if ye can put a scare in 'em they'll keep our of yer way!

(In chat box) *red text* You have unlocked a new music track: Aye Car Rum Ba

*Climb down ladder* 
(in chat box) You climb down the ladder.

*Open cupboard*
*Search open cupboard*
There is a tangle of tusty old farming equiptment in here.
Click here to continue

What would you like to take?
Click here to continue

---->Select an option<----
A rake
A seed dibber
A watering can
All of the above!

Option 4.

You help yourself to what you need.
Click here to continue


*Use watering can on sink*
(In chat box) You fill the watering can from the sink.

*Zombie Pirate holding Sign says outloud as you pass him* 
Give us yer rum, ye scurvy dog!

*Another zombie says outloud as you pass him* 
Ye'll never beat us all!

*Another zombie says outloud* Where d'ye think yer goin?
Arr! Come back here!

*Use Rake on Blindweed Patch*

*Use Blindweed seed on patch*

(In chat box)
You plant a seed in the blindweed patch.

*Right click Intimidate on Zombie Swab*

Player: Hey you!

Zombie Swab: Hahahaha!
Yer going ter die!

"We Interupt this insult to bring you this soothing picture of a kitten. Normal service will be resumed shortly. THank you for your patience."
Click here to continue

*Cat playing with Ball of red wool appears on screen*
*Rum Deal sign above cat*

(In chat box)
He seems to have vanished...you must have scared him off.

(In chat box) I wonder how my Blindweed is coming along?

*Runs past zomibes, zombies say out loud*
Arr! Come back here!
Give us your rum, ye scurvy dog!

-Talk to Captain Brain

Player: Here! I have your Blindweed!

Captain Braindeath: Splendid, lass! Go shove it the Intake Hopper upstairs.
We'll beat them zombies yet! Arr!

*Use Blindweed on Pressure Barrel*

(In chat box)
You stuff the Blindweed into the Hopper.

-Talk to Captain Braindeath

Player: Well, that takes care of the Blindweed. What now?

Captain Braindeath: Well, ye've shoved the Blindwood into the mix, what we need is a bucket of stagnant water.

Player: Where can I get some of that?

Captain Braindeath: Ye won't have to go far, lass, we have a pool of the stuff here!

Player: Here in the brewery?

Captain Braindeath: No, lass, that would be a strange thing to have in a brewery!
It's up the mountain to the north.

Player: Up a mountain?

Captain Braindeath: Well, 'tis technically a volcano, but ye get the general idea.

Player: And I assume the place is crawling with these zombies?

Captain Braindeath: No lass, there be not a zombie in sight.

Player: Oh, good!

Captain Braindeath: All ye have to do is get past the keen-eyed lookout that's been spottin' my men when I send 'em.
I tell ye that it won't be easy!

Player: When is it ever...

*Walks past protesting zombies again. One shouts ouloud.
Whadda we want? Rum!

*Open gate*

50% Luke: Arr! Tryin' ter get away eh? Well ye'll never sneak past me. I'm the best lookout this crew has ever seen!

Player: Oh my! Is that a genuine 3rd Age Diversion?

*50% Luke says outloud* Where?

*As you keep walking up the island*
(in chat box)
You have unlocked a new music track: Blistering Barnacles.

*Use bucket with Stagnant lake*
(in chat box)
You scoop up a bucket of the stagnant water.

-Talk to Captain Braindeath

Player: So is that everything?

Captain Braindeath: No, lass, next ye'll need to go outside and catch five loads of Sluglings fer the brew.

Player: What? Sluglings? That's disgusting!

Captain Braindeath: Arr, lass, that it be.
To a weak-stomached, knocked-kneed landlubber...

Player: Why Sluglings?

Captain Braindeath: 'Cos lass they're one of our super-secret ingredients!
Yer not too susceptible to mind control are ye lass?

Player: Why?

Captain Braindeath: Because they have been know te, well, influence people every now and again.
I'm sure ye've got nothing te worry about!

Player: They aren't related to those Seaslugs are they?

Captain Braindeath: No lass, we just call them 'Sluglings' 'cos of a long and convoluted story involvin' a metal pipe and three dead seagulls.
Aye they're related! Tis a good job they'll starve if they tried te eat yer brains!

Player: So what should I do with these Sluglings anyway?

Captain Braindeath: Well, ye shove them in the Pressure Barrel in the attic.
And the ye...

Player: And then I...

Captain Braindeath: And then ye...
Pressurise 'em

Player: Pressurise them?!

Captain Braindeath: Just a little.
Look, we don't have all day, get movin'.
Here. Ye'll need this to catch them.
*He gives you a fishbowl and net tangled together*

Player: What should I do with it?

Captain Braindeath: Just dunk it in the water. I'm sure a clever lass like yerself will have no problem.
Oh, and if ye haul up some squiddy-looking things, don't hesitate to shove 'em in the barrel too.
They add a special, fishy texture to the drink.

*Walks past protesting zombies again and they say outloud*
United we stagger!
You'll never beat us all!
United we stagger!

*Fish Fishing Spot*
(in chat box)
You dunk the bowl in the water...
...and you catch some Sluglings!

*Use Sluglings with Pressure Barrel*
(In chat box)
You stuff the squirming Sluglings into the barrel.

*Pull Pressure lever*
(In chat box)
You pressurise the assorted sea creatures.

-talk to Captain Braindeath

Player: How could I kill my sluggy brethren...?

Captain Braindeath: Snap out of it, lass! Yer in slug-shock!

Player: What? Who?
Gah! Sorry about that.

Captain Braindeath: No problem. Well, now ye have just one more ingredient to grab, and then we can get this 'rum' flowin!

Player: Well, how far away will I have to go to grab it?

Captain Braindeath: Not far at all, lass. Ye've just got to get it from the basement!

Player: Great! What is it?

Captain Braindeath: Hold yer horses, lass! While ye was off gallivantin' with yer slimy aquatic playmates, the 'rum' achieved spiritual critical mass.
To put it in term ye'll understand: the brewin' equiptment is possessed.

Player: Possessed!

Captain Braindeath: Don't ye worry yerself about it! This happens all the time.
Well, to tell the truth, my lads are a little quicker off the mark, so it only happens occasionally.
Not that I'm criticisin' yer performance, lass.
Give the controls a couple of belts with this wrench.
*Captain Braindeath gives wrench*
One of the lads did a little priestin' on the side before he came here. Get him to bless it and ye'll do fine.

-talks to Davey

Player: You used to be a priest right?

Davey: I didn't nick anything, miss. I've got twenty people who'll swear blind I was...

Player: What?
Never mind. I need some help with the spirit in the brewing equipment.

Davey: Trust me, miss, lots of people need help after coming into contact with the spirits we produce here.

Player: Can you bless this wrench for me?

Davey: I might well do. Remember, only the first one's free.
Dominoes Ad Naseum, Romanes Eunt Domus.
Sorted.

Player: Is that it?

Davey: Oh, you want the full package deal.
All right. Brace yourself.

*Davey performs some kind of magic on player*

*Zooms in on player*
*Player does some sort of magical jig*

*Player says outloud*
Groovy.

Player: Thanks!

Davey: No problem, miss. Good luck with your little problem.
You might find that little wrench worth hanging on to after you're done with the Spirit.

Player: Really? I mean, it's holy and everything, but I don't think it looks al that useful.

Davey: Well, it may not look much, but you'll find that you might need a few less prayer potions if you have it in your pack, if you know what I mean.

Player: Well, no. I don't know what you mean, but I'm sure I'll find out!

*Wrench is now a "Holy wrench"*

*Use Holy wrench on Brewing control*

*Player says outloud*
The power of Guthix compels you!
(in chat box)
The Evil spirit is forced from the controls!

*Attack Evil spirit*
*After killing Evil spirit*
(in chat box)
You have banished the Evil Spirit!

-talks to Captain Braindeath

Captain Braindeath: Well, now that ye've got that spirit out of there ye can dump in the final ingredient.

Player: And that is?

Captain Braindeath: We need the body of a diseased Fever Spider!

Player: Remind me never to drink anything you have ever made. Or touched.

Captain Braindeath: When yer quite done flappin' yer lips, go down into the basement and whack spiders until ye find a fever spider body.
Shove it in the hopper, and then we're in business.
I see yer already wearin' some Slayer Gloves. That'll keep the Fever spiders from gnawin' yer hands off!

*Attack fever spider*
*Pick up fever spider body*

*User Fever spider body on Hopper*
(in chat box)
You cram the diseased Fever Spider body into the hopper.

-talks to Captain Braindeath

Player: Well, I stuck your spider in the hopper, what now?

Captain Braindeath: Now ye stand well back and watch the glory of brewin' at its best!

*Zooms on Brewer machine*

Player: Is that it?

Captain Braindeath: Aye lass! Now get outside and feed that stuff to the pirates.
Try givin' it to the Captain, he's in charge. Get him bladdered and the rest will fall!
Ye'll need to use that bucket of yours. Most stuff can't stand bein' in contact with our 'rum fer too long.
Took us a lot of dissolvin' to work that one out.

*Turn output tap*
(in chat box)
You carefully fill the bucket with the foul smelling-swill.

*walks past protesting zombies and they say outloud*
Rum! Rum! Rum!
Drink!
Yer rum or yer brains!

*Use Unsanitary swill with Captain Donnie*

Captain Donnie: Be that the finest, most abrasive 'rum' I've ever smelled?

Player: Yes! That it be!

Captain Donnie: Hand it over or I'll run ye through!

The Captain drinks the 'rum' as quickly as possible
Click here to continue

Captain Donnie: Arr.
Ye be a good lass, fer a filthy livin' landlubber.

Player: So... I take your boss will be pleased?

Captain Donnie: Arr, that he will. I'll tell...
Wait a minute...
Arr, ye tricky dog!
Ye tried to trick old Donnie!

Player: Oh well, I guess I'll have to try again.

Captain Donnie: Arr, lass, you tried to trick me, but I was too clever for ye!
Besides, Rabid Jack would have my hide if I told ye it were him that sent me!

Player: I'm sure he would. Good job you caught me out, eh!

Captain Donnie: Aye! Now get ye gone, and don't return without more 'rum'!

-talks to Captain Braindeath

Captain Braindeath: So, what did he say?

Player: Not much that was coherent.
Who is Rabid Jack?

Captain Braindeath: Rabid Jack!
THE Rabid Jack!
Egad...I haven't hear that name...
...before.

Player: So, who is he?

Captain Braindeath: Dunno lass.
Almost as if I aren't changing subjects, well done!
With those rottin' dogs legless they'll never keep fighting us now, so we've decided to stay here and keep the 'rum' flowin!
Thanks, lass. We'd never have managed without ye!

Congratulations!
You have completed Rum Deal!

You are awarded:
2 Quest points
Holy Wrench
7000 Prayer Xp, 7000 Fishing
XP and 7000 Farming XP

(in chat box)
Congratulations! Quest complete!