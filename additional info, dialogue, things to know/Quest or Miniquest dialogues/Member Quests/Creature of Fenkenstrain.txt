Creature of Fenkenstrain
A- Reading the signpost
Message: The signpost has a note pinned onto it. The note says:
'---Braindead Butler wanted---
Gravedigging skills essential - Hunchback advantageous
See Dr Fenkenstrain at the castle NE of Canifis'

B- Talking to Dr Fenkenstrain
Dr Fenkenstrain: Have you come to apply for the job?
Select an Option	1.Yes.
			2. No.
Option 1
Player: Yes, if it pays well.
Dr Fenkenstrain: I'll have to ask you some questions first.
Player: Okay...
Dr Fenkenstrain: How would you describe yourself in one word?
Select an Option	1.1 Stunning
			1.2 Awe-inspiring
			1.3 Breathtaking
			1.4 Braindead
Option 1.1-1.3 1.4.1-1.4.3
Dr Fenkenstrain: Mmmm, I see.
Dr Fenkenstrain: I'm sorry, but I don't think you have the apitude for this position. [End]

Option 1.4
Dr Fenkenstrain: Mmmm, I see.
Dr Fenkenstrain:  Just one more question. What would you say is your  greatest skill?
Select an Option	1.4.1 Combat
			1.4.2 Magic
			1.4.3 Cooking
			1.4.4 Grave-digging
Option 1.4.4
Dr Fenkenstrain: Mmmm, I see.
Dr Fenkenstrain: Looks like you're just the man for this job! Welcome aboard!
Player: Is there anything you'd like me to do for you, sir?
Dr Fenkenstrain: Yes, there is. You're highly skilled at grave-digging, yes?
Player: Err...yes, that's what I said.
Dr Fenkenstrain: Excellent. Now listen carefully.
Dr Fenkenstrain: I need you to find some...stuff...for me.
Player: Stuff?
Dr Fenkenstrain: That's what I said...stuff.
Player: What kind of stuff?
Dr Fenkenstrain: Well...dead stuff.
Player: Go on...
Dr Fenkenstrain: I need you to get me enough dead body parts for me to switch together a complete body, which I play to bring to life.
Player: Right...okay...if you insist. [End]

Option 2
Player: No thanks, I wouldn't work here if you paid me.
Dr Fenkenstrain: Charming. [End]

C- 2nd time talking
Select an Option	1. Do you know where I could find body parts?
			2. Remind me what you want me to do.
			3. Why are you trying to make this creature?
			4. Will this creature put me out of a job.
			5. I must get back to work, sir.
Opiton 1
Player: Do you know where I could find body parts?
Dr Fenkenstrain: The soil of Morytania is unique in its ability to preserve the bodies of the dead, which is one reason why I have chosen to carry out my experiments here.
Dr Fenkenstrain: I recommend digging up some graves in the local area.
To the south-east you will find the Haunted Woods; I believe there are many graves there.
Dr Fenkenstrain: There is also a mausoleum on an island west of this castle. I expect the bodies that are buried there to be extremely well preserved, as they were wealthy in life. [End]

Option 2
Player: What do you want me to do, again?
Dr Fenkenstrain: I need you to get me enough dead body parts for me to stitch together a complete body, which I plan to bring to life.
Player: Right...okay...if you insist. [End]

Option 3
Player: Why do you want to make a creature?
Dr Fenkenstrain: I came to the lands of Morytania many years ago, to find a safe sanctuary for my experiments. This abandoned castle suited my purposes exactly.
Player: What were you experimenting in?
Dr Fenkenstrain: Oh, perfectly innocent experiments - for the good of mankind.
Player: Then why did you need to come to Morytania?
Dr Fenkenstrain: Enough questions, now. Get back to your work. [End]

Option 4
Player: Will this creature put me out of a job?
Dr Fenkenstrain: No, my friend. I have a very special purpose in mind for this creature. [End]

Option 5
Player: I must get back to work, sir. [End]

D- Talking to the ghost in the he castle
Select an Option	1. Tell me about Fenkenstrain.
			2. What happened to you head?
			(5. Do you know where the key to the shed is? [After 4. and replaces 				3.])
			(4. Do you know where a conductor mould is?)			
			3. What's your name?

When not wearing a ghostspeak amulet:
The headless gardener's neck twitches at you, but the lack of any head prevents him from speaking.

Option 1
Player: Can you tell me anything about Fenkenstrain?
Message: You feel the power emanate from the Amulet of Ghostspeak and the air around you vibrates with the ghostly voice of the headless gardener.  

[Only happens if this is the first question he answers to you, if something else is asked before, the message appears there, and not here.]

Gardener Ghost: Oi could tell you a few things about old Fenky, yeah.
Player: Go on.
Gardener Ghost: Once, this castle were full o' good folk - my friends. Frenky was just the castle doctor, you know, to the lord and the castle folk.
Gardener Ghost: I don't know what happened to them all, but one by one they all disappeared. When they were gone a while, I went an dug grave for 'em in the forest.
Gardener Ghost: After a while there weren't no one left, but the lord, Fenkenstrain and meself.
Gardener Ghost: Old Fenky sent me into the forest to dig 'im a pit - never said what for - then would you believe it, someone chops me 'ead off.
Player: Did you see who did it...before...?
Gardener Ghost: Before oi kicked the bucket, you mean?
Player: Umm...
Gardener Ghost: Don't worry yerself, I'm not worried about bein' dead.
Worse things could happen, I suppose.
Gardener Ghost: One thing I do know is, there ain't  no lord of the castle anymore, 'cept for old Fenky. Makes ya think a bit, don't it? [End]

Option 2
Player: What happened to your head?
Gardener Ghost: Oi was in the old 'aunted Forest to the south, diggin' a pit for moi old maaster, old Fenkenstrain, when would you believe it, someone chops me head off. Awful bad luck, weren't it?
Player: Oh yes, dreadful bad luck. 
Gardener Ghost: So oi thinks to meself, I don't needs any 'ead to be gettin on with me gardenin', long as I got me hands and me spade.
Player: Would you show me where this place was?
Gardener Ghost: Well, oi s'pose oi've got ten minutes to spare. [End]
Or
Gardener Ghost: No mate, you've found my head already, oi think. [End]

Option 3
Player: What's your name?
Gardener Ghost: Me name? It's been a moivellous long while, mate, since I had any use for such a thing as a name.
Player: Don't worry, I was just trying to make conversation.
Gardener Ghost: No, no, I can't be havin' that. I'll rember in a minute...
Player: Please, don't worry.
Gardener Ghost: Lestwit, that's it! Ed Lestwit. [End]

Option 4
Player: Do you know where I can find a lightning conductor mould?
Gardener Ghost: A conductor mould, you say? Let me see...
Gardener Ghost: There used to be a bloke 'ere, sort of an 'andyman 'e was. Did everything 'round the place - fixed what was broke, swept the chimneys and the like. He would 'ave had a mould, I imagine.
Player: Wher is he now?
Gardener Ghost: E's dead, just like everyone else round 'ere... 'cept for me. [End]

Option 5
Player: Do you know where the key to the shed is?
Gardener Ghost: Got it right 'ere in my pocket. Here you go.
Message: The headless gardener hands a rusty key. [End]

Message: You find a garden brush in the cupboard.
Chatbox: You attach the cane to the brush.
Chatbox: You stick the garden brush up the chimney, but it is not long enough to clear the blockage.
Chatbox: You give the chimney a jolly good clean out.

Message: A lightning conductor mould falls down out of the chimney.

E
Select an Option	1. I have some body parts for you.	 [Except for this, eveything is 									the same as at C]

Option 1
Player: I have some body parts for you.
Dr Fenkenstrain: Great, you've brought me some arms.
Dr Fenkenstrain: Excellent, you've brought me some legs.
Dr Fenkenstrain: Splendid, you've brought me a torso.
Dr Fenkenstrain: Fantastic, you've brought me a head.
Dr Fenkenstrain: Superb!! Those are all the parts I need. Now to sew them together ...
Dr Fenkenstrain: Oh bother! I haven't got a needle or thread!
Dr Fenkenstrain: Go and get me a needle, and I'll need 5 lots of thread. [End]

F- When using the head on Dr Fenkenstrain
Dr Fenkenstrain: I can't use this head, [player name]. It's missing the most important part - the old grey matter. [End]

G- Talking to roavar option 2.
Player: Can I buy something to eat?
Roavar: If you've got the money, I've got a real treat for you.
Player: What have you got? [Continues at I]

H- When you try to pick up the pickled brains
Roavar: You're interested in our speciality, I see. Would you like to buy some?
Player: What exactly is in the jar? [Continues at I]

I
Roavar: Pickled brain, my friend. Only 50 gold to you.
Player: Err...pickled brain from what animal?
Roavar: Animal? Don't be disgusting, man! No, this is a human brain - only the best for my customers.
Select an Option	1. I'll buy one.
			2. I'm not hungry.
Option 1
Player: I'll buy one, please.
Roavar: A very, wise choice, sir. Don't eat it all at once, savour every morsel - that's my advice to you. [End]

Option 2
Player: I'm afraid I'm not really hungry at the moment. [End]

J- Random messages when getting the parts
Search west bookcase
Which book would you like to read?	1. 1001 Ways To Eat Fried Gizzards
					2. Practical Gardening For The Headless
					3. Human Taxidermy for Nicompoops
					4. The Joy of Gravedigging
Option 1
Message: This books leaves you contemplating vegetarianism. [End]

Option 2
Message: This book has some very enlightening points to make, but you are at a loss to know how anyone without a head could possibly read it. [End]

Option 3
Message: This book seems to have been read hundreds of times, and has scribbles and formulae on every page. One such scribble says 'None good enough - have had to lock them in the caverns...' [End]

Option 4
Message: As you pull the book a hidden latch springs into place, and the bookcase swings open, revealing a secret compartment.
Message: You find a marble amulet in the secret compartment. [Or] The secret compartment is empty. [End]

Search west bookcase
Which book would you like to read?	1. Men are from Morytania, Women are from 							Lumbridge
					2. Chimney Sweeping on a Budget
					3. Handy Maggot Avoidance Techniques
					4. My Family and Other Zombies
Option 1
Message: You discover some fascinating insights into the mind of the female kind. [End]

Option 2
Book: Chimney Sweeping on a Budget
Page 1:
Page 26
that sometimes a sweep may find themselves brushless and without funds to purchase the one tool that is most essential to their trade. What is a chimney sweep without his or her brush? In this kind of situation any normal long-handled

Page2: 
brush might be a suitable replacement, although when attaching extensions to the handle make sure to use something sturdy like wire, otherwise a sweep may find themselves losing their brush and livelyhood to the forces of gravity

Option 3
Message: As you pull teh book a hidden latch springs into place, and the bookcase swings open, revealing a secret compartment.
Message: You find an obsidian amulet in the secret compartment.  [Or] The secret compartment is empty. [End]

Option 4
Message: The book is a fantastic read. [End]

Message: The marble and obsidian amulets snap together tightly to form a six-sixpointed amulet.

Message: The star amulet fits exactly into the depression on the coffin lid.

Chatbox: You start digging...
Message: ... and you uneath a torso.
Or
Message: ... and you unearth a pair of arms.
Or
Message: ... and you unearth a pair of legs.
Message: You take a key out of the chest.

K- 
Dr Fenkenstrain: Where are my needle and thead, [Player Name]? [End, or:]
Dr Fenkenstrain: Some thread, excellent.
Dr Fenkenstrain: Ah, a needle. Wonderful.
Message: Fenkenstrain uses the needle and thread to sew the body parts together. Soon, a hideous creature lies inanimate on the ritual table. 
Dr Fenkenstrain: Perfect. But I need one more thing from you - flesh and bones by themselves do not make life.
Player: Really?
Dr Fenkenstrain: I have honed to perfection an ancient ritual that will give live to this creature, but for this I must harness the very power of Nature.
Player: And what power is this?
Dr Fenkenstrain: The power of lightning.
Player: Sorry, can't make lightning, you've got the wrong man-
Dr Fenkenstrain: Silence your insolent tongue! The storm that brews overhead will create the lightning. What I need you to do is to repair the lightning conductor on the balcony above.
Player: Repair the lightning conductor, right. Can I have a break, soon? By law I'm entitled 15 minutes every-
Dr Fenkenstrain: Repair the conductor and BEGONE!!! [End]

L- 2nd time talking
Player: How do I repair the lightining conductor?
Dr Fenkenstrain: Oh, it would be easier to do it myself! If you find a conductor mould you should be able to cast a new one.
Dr Fenkenstrain: Remember this, [Player Name]: my expirement will only work with a conductor made from silver. [End]

M-
Select an Option	1. Tell me about Fenkenstrain.
			2. Do you know where the key to the shed is?
			3. Do you know where a conductor mould is?
			4. What happened to your head?

Option 2
Player: Do you know where the key to the shed is?
Gardener Ghost: Got it right 'ere in my pocket. Here you go.
Message: The headless gardener hands you a rusty key. [End]

N- When you wind the clock in the southwest tower.
Message: As you wind the clock a letter fals out. Judging by the thick covering of dust it must have been here for some time.
http://runescape.wikia.com/wiki/Letter_(Creature_of_Fenkenstrain)

O - Repairing the conductor
Message: You repair the lightning conductor not one moment too soon - a tremendous bolt of lightning melts the new lightning conductor, and power blazes throughout the castle, if only briefly.

P - Fenky again
Player: So did it work, then?
Dr Fenkenstrain: Yes, I'm afraid it did, [Player Name] - all too well.
Player: I can't see it anywhere.
Dr Fenkenstrain: I tricked it into going up to the Tower, and there it remains, imprisoned.
Player: So the creature wasn't all you hoped, then?
Dr Fenkenstrain: ...oh, what have I done...
Player: Oh, I see, we're developing a sense of right and wron now are we?
Player: Biy late for that, I'd say.
Dr Fenkenstrain: I have no control over it! It's coming to get me!
Player: What do you want me to do about it?
Dr Fenkenstrain: Destroy it!!! Take the key to the Tower and take back the life I never should have granted!!! [End]

Q - Dr Fenkenstrain again...
Dr Fenkenstrain: So have you destroyed it?!!?
Player: Not yet.
Dr Fenkenstrain: Please, hurry - save me!!!! [End]

R - The creature
Player: I am commanded to destroy you, creature!
Fenkenstrain's Monster: Oh that's *hic* not very *hic* nice ...
Player: Are you feeling okay?
Fenkenstrain's Monster: Abso *hic* lutely. Never *buuurrp* better.
Player: You don't look very dangerous.
Fenkenstrain's Monster: How *hic* do I look?
Player: You really don't know, do you? Have a look for yourself.
Message: The creature stumbles over towards the mirror, focuses upon his reflection and...
Fenkenstrain's Monster: AAAAARRGGGGHHHH!
Message: The creature becomes instantly sober, horror all too evident in his undead eyes.
Player: I'm sorry. I suppose I'm partly to blame for this.
Fenkenstrain's Monster: No - it was him I wager - Fenkenstrain - wasn't it? He's brought me back to life!
Player: Who are - were - you?
Fenkenstrain's Monster: I was Rologarth, Lord of the North Coast - this castle was once mine. Fenkenstrain was the castle doctor.
Player: So the castle wasn't really abandoned when he found it?
Fenkenstrain's Monster: Is that what he told you? No, no, this castle was once full of people and life. Fenkenstrain advised me to sell them to the vampyres, which - I am sad to say - I did.
Player: I found your brain in a jar in Canifis, so he must have sold you too.
Fenkenstrain's Monster: Of that I will not speak. There lie memories that should rest with the dead, the living unable to bear them.
Player: That's it - I'm leaving this dreadful place, whether I get paid or not. Is there anything I can do for you before I leave?
Fenkenstrain's Monster: Only one - please stop Fenkenstrain from carrying on his experiments, once and for all, so that no other poor soul has to endure suffering such as that of my people and I. [End]

S- Second time talking
Player: Do you know how I can stop Fenkenstrain's exepriments?
Fenkenstrain's Monster: Take the Ring of Charos from him.
Player: What is the ring?
Fenkenstrain's Monster: It was my birthright, passed down to me through the ages, its origin forgotten.
Fenkenstrain's Monster: The Ring of Charos has many powers, but Fenkenstrain has bent them to his own evil purposes. Without the power of the ring, he will not be able to raise the dead from their sleep.
Fenkenstrain's Monster: It has one other, extremely important use - it confuses the werewolves' senses, making them believe that they smell one of their own kind. Without the ring, Fenkenstrain will be at their mercy. [End]

T
Dr Fenkenstrain: So have you destroyed it?!!?
Player: Never, now that he has told me the truth!
Dr Fenkenstrain: Oh my, oh my, this is exactly what I feared!
Dr Fenkenstrain: Why did you have to pick Rologarth's brain of all brains?!?
Player: I'm through working for you.
Dr Fenkenstrain: No!! I refuse to release you!! You must help me build another creature to destroy this dreadful mistake!! [End]

Chatbox: You steal the Ring of Charos from Fenkenstrain.
Chatbox: Congratulations!! Quest complete!

Dialogues after the quest! Yay!

U - Gardener Ghost
Player: How are you?
Gardener Ghost: Same as ever, mate, just gettin' on with it regardless.
Player: Good for you...err...mate. [End]

V - Lord Rologarth
Lord Rologarth: How goes it, friend?
Player: I stole the Ring of Charos from Fenkenstrain.
Lord Rologarth: I saw him climb up into the Tower to hide. It doesn't matter - soon the werewolfes will come for him, and his experiments will be forever ceased.
Player: Do you want the ring back? It is yours after all.
Lord Rologarth: No, you keep it, my friend. Werewolfes hunger for the scent of live flesh - I have no need for the ring. I have my castle back, if not my soul. [End]

W - Dr Fenkenstrain
Dr Fenkenstrain: theyrecomingtogetme theyrecomingtogetme...
Player: It is all you deserve. Lord Rologarth is master of this castle once more. Let him protect you - if he  wants to.
Dr Fenkenstrain: theyrecomingtogetme theyrecomingtogetme... [End]

X- Signpost 
The signpost has a note pinned onto it. The note says: 'AAARRGGGHHHHH!!!!!'