The Red Axe's Departure

    Commander Veldaban: [Player]! Have you heard?
    Player: You mean, half the world has suddenly become a farmer?
    Commander Veldaban: No, not that. It�s the Red Axe!
    Player: What have those grumpy little guys done now?
    Commander Veldaban: They�ve left the city! They were already boycotting the trade floor, but now they�ve completely left the city.
    Player: Oh dear, is that serious?
    Commander Veldaban: It�s never happened before! At the moment, it�s unclear yet if they�ve left the Consortium as well. The directors are in turmoil!
    Player: So what happens now?
    Commander Veldaban: Well� I may require your services again. Are you interested in hearing more?
        Player: Some other time, perhaps.
        Player: Very interested!
            Commander Veldaban: We need some more information about what the Red Axe is up to.
            Player: Are you asking me to� spy on them? Isn�t that treason?
            Commander Veldaban: The other directors of the Consortium don�t think so and are likely to expel the Red Axe completely anyway.
            Player: So where would you like me to go?
            Commander Veldaban: That�s the problem. We don�t know where they moved their headquarters to. But we may have one lead�
            Player: Yes?
            Commander Veldaban: There is a dwarf, all the way to the northeast in Keldagrim-East. He talks about kebabs a lot. Don�t suppose you�ve met him?
                Player: Yes, I do believe I have. Smashed glasses, wrecked house? Poor personal hygiene standards? Brother or cousin to that other drunken dwarf?
                    Commander Veldaban: That�s him, poor fellow. He didn�t always used to be like that. But he seems to know something about the Red Axe for some odd reason. He speaks of them sometimes in one of his many rants. Problem is, no one believes him. Everyone thinks he�s completely mad. And perhaps they�re right. But if he indeed knows something about the Red Axe or where their headquarters may be, we need to find out. What do you say, [Player]? Do you want to help the Black Guard with this?
                        Player: Sounds like just the job for me!
                            Commander Veldaban: Excellent, good luck to you in your quest! May Guthix protect you! 
                        Player: No, I try to stay away from drunken dwarves. 
                Player: No. 

�� After accepting the quest ��

    Player: Well, here I am� at the drunken dwarf�s relative�s residence, who is also drunken. I wonder what drunken tales he can tell me. 

The Only Lead...
...is a Drunken Dwarf

    Player: Hello there! Are you alright?
    Player: I need to know about the Red Axe�
        Drunken Dwarf: THE RED AXE WILL COME FOR YOU!
        Player: What, what do you mean?
        Drunken Dwarf: I don�t know� I c-c-c-can�t think straight when I�m *hic* sober.
        Player: You don�t look very sober to me!
        Drunken Dwarf: DON�T contradict me! Or I won�t tell you about ehm� about the� whatever it is you wanted to know.
        Player: Have you ever considered to stop drinking? It�s bad for you.
        Drunken Dwarf: I�ll shtop, right after you get me shome of the good shtuff! 

�� Talk again with a beer at hand ��

    Player: I need to know about the Red Axe�
    Drunken Dwarf: Did you get me shome of the good stuff yet?
    Player: Yes, here you go.
    Drunken Dwarf: What wash that? *hic* No no no, this ish noooooo good! I want some of the REALLY good stuff.
    Player: What�s the REALLY good stuff then?
    Drunken Dwarf: Some of them original Kelda Stout! Ash blue ash the river! Ash strong ash ole Dondakan�s rock. Ash foul tasting ash ash.
    Player: Hey, er, I don�t think that�s good for you.
    Drunken Dwarf: I know, but get me shome of it anyway. �ere, have a seed.
    Player: What am I supposed to do with a seed?
    Drunken Dwarf: You silly human! Silly shilly silly *hic* human. You need FOUR seeds, and grow them into hops. Hop hop hop! *hic*
    Player: Where do I get the other seeds then?
    Drunken Dwarf: Some of me drinking buddies might have �em. They�re quite hard to find, you shee. Oh yesh, quite hard to find. Drinking buddies. Yes, find me some drinking buddies. No, find the other seeds. Yes.
    Player: Right, I think what you�re trying to say is� I need to find some other drunken dwarves, collect Kelda seeds from them until I have four, then grow them into hops so I can make Kelda stout.
    Drunken Dwarf: You�re awfully smart for a human!
    Player: Maybe that�s just because I don�t drink as much as you. 

Drinking Buddies
Rowdy dwarf

    Player: Hold on a second there, I need your help!
    Rowdy dwarf: Help, from me? You musht be more drunk than I am.
    Player: No, I try not to get drunk, it�s not good for you. But I do need help in brewing some beer.
    Rowdy dwarf: Mmmm, beer.
    Player: I need to brew Kelda stout, but I haven�t got enough seeds to grow the hops.
    Rowdy dwarf: Mmmm, Kelda stout.
    Player: Do you know where I can find any seeds?
    Rowdy dwarf: I might� oh yesh, I jusht might have a sheed like that.
    Player: But you�re not going to just give it to me, are you?
    Rowdy dwarf: No.
    Player: Alright then, what do you want for it?
    Rowdy dwarf: Lemme think� oh yesh. Get me� [random item].
    Player: What?? Where am I going to get that?
    Rowdy dwarf: You can at least make the effort. 

�� Talking again without the item ��

    Player: Do you have any Kelda seeds?
    Rowdy dwarf: Where�s my ehm� What wash it again? Oh yesh� [random item].
    Player: I�m still looking for that.
    Rowdy dwarf: Look a little harder! 

�� Talking again with the item ��

    Player: Do you have any Kelda seeds?
    Rowdy dwarf: Where�s my ehm� What wash it again? Oh yesh� [random item].
    Player: Look, I�ve got it here!
    Rowdy dwarf: Shplendid! Give that to me please!
    Player: Now how aobut those seeds?
    Rowdy dwarf: Wel, I�ve got a Kelda hop seed right here! Pashed down generation after generation in my family�
    Player: Wow, really?
    Rowdy dwarf: Nah, I�m just kidding! Here, have the seed, you won�t catch me farming in shome dirt patch anyway!
    Player: I suppose you wouldn�t ever have a proper job or do some decent hard work, no.
    Rowdy dwarf: What wash that?
    Player: Nothing� 

Gauss

    Gauss: A good day to you, [sir/madam]!
    Player: And to you! Say, you know a lot about beer, right?
    Gauss: A fair bit if I say so myself, yes. What do you want to know?
    Player: Do you know anything about Kelda stout?
    Gauss: Ah, very hard to find that is! This place doesn�t stock it. Don�t think the Laughing Miner does either.
    Player: Yes, that�s what I heard, so I�m going to brew it myself.
    Gauss: Then you�ll probably need some seeds!
    Player: Have you got any?
    Gauss: Have a toast to our good health with me and I�ll tell you!
    Player: Drinking is bad for you, but I�ll make an exception this time.
    Gauss: Hurrah for the human!
    Player: Cheers!
    Gauss: Cheers! That was fun! Here, I�ve got one Kelda seed left. A sociable chap like you is more than welcome to it. 

Khorvak

    If Between a Rock� is completed beforehand
        Khorvak, a dwarven engineer: Hey it�s you again! Didn�t someone shoot you out of a cannon? Haha, that still cracks me up!
        Player: Strangely enough I survived my ordeal.
        Khorvak, a dwarven engineer: Hahahaha, you must tell me about it sometime!
        Player: I�d rather not� Anyway, I�m looking for some Kelda hop seeds. 
    Khorvak, a dwarven engineer: Oh, we�ve got an expert here! Kelda shtout is delishious!
    Player: It sounds ghastly to me. But I want to brew some to help out, errr, a friend of mine.
    Khorvak, a dwarven engineer: Oh, I�ve got a sheed you might like to borrow!
    Player: Borrow?? How can I borrow a seed? I can hardly give it back once I�ve grown it.
    Khorvak, a dwarven engineer: Then we�ve got no deal!
        Player: What if I offer you a drink?
        Player: No wait, I want to borrow it after all.
            Khorvak, a dwarven engineer: You are going to give it back after you�re done widdit, right?
            Player: Of course, of course! After I�ve grown my Kelda seeds into hops, then harvested the hops and brewed my Kelda stout� sure, I�ll give you back your seed!
            Khorvak, a dwarven engineer: That�sh very deshent of you, human.
            Player: Only too happy to help!
            Khorvak, a dwarven engineer: Hey, err� wait� wait a second! Hey, wait!
            Player: Err, yes?
            Khorvak, a dwarven engineer: How are you goin� to give me back my sheed once you�ve made the Kelda stout? You�ll be too drunk to find your way back here!
            Player: Oh, don�t worry� like I said, it�s not for myself, it�s for a friend.
            Khorvak, a dwarven engineer: That�sh alright then, here you go. 

Growing the Seeds

    Player: I need some help.
    Rind the gardener: Yes, [Player]? What can a humble gardener like myself help you with?
    Player: I have four Kelda seeds here, but need some advice on growing them.
    Rind the gardener: Marvelous, those seeds are quite scarce don�t you know! First of all, you must remember that these seeds only grow underground. Sunlight is harmful to them, for some strange reason. There is a patch in the palace garden that you�re more than welcome to use.
    Player: Thanks! Is there anything else I need to know?
    Rind the gardener: Just take care of the weed in the patch and plant your seeds. They�ll grow into hops very quickly, if you�re lucky. Kelda seeds have a remarkably fast grow rate.
    Player: Thank you! Your advice is much appreciated! 

(Optional) While You Wait...
Messenger Duty

    Player: I need some help.
    Rind the gardener: Yes, [Player]? What can a humble gardener like myself help you with?
    Player: I think I need some more help with this whole farming business.
    Rind the gardener: Just get rid of the weeds in the patch, plant your seeds and wait for the hops to grow. The miracle of nature will do the rest!
    Player: Do I have to wait here all this time then?
    Rind the gardener: Yea and no. Yes, you do have to wait, but no, you don�t have to stay here. You can help out people in the city, for instance, there�s always someone who needs help with an urgent job.
    Player: You mean the mining companies? I did jobs for them, they�re not very profitable.
    Rind the gardener: No no, just people like shopkeepers and such, they�re quite willing to teach you things in exchange for the jobs as well. But here, tell you what, I�ll give you some seeds if you can deliver this letter to a friend in Falador.
    Player: Do I have to do that? I just want to grow my hops and get on with my quest for Veldaban.
    Rind the gardener: Oh no, you don�t have to do it all, I just thought you wanted something to do while you were waiting.
        Player: Alright, I�ll deliver that letter for you.
            Rind the gardener: Happy to hear that! His name is Elstan, he lives just south of Falador.
            Player: I�ll be right back! 
        Player: I�d rather just sit here and stare at this patch, thanks. 

Far Away Falador

    Player: Hello there. Are you Elstan?
    Elstan: Why yes, I am. Have I become that well known?
    Player: Well, I�ve got a letter here from your friend in Keldagrim.
    Elstan: Ah, you mean Rind? Great to hear from the little fellow again. Can I see it please?
    Player: Of course.
    Elstan: Most enlightening. Please give him my regards next time you see him, I look forward to the next conference. 

Returning to Rind

    Player: I need some help.
    Rind the gardener: Yes, [Player]? What can a humble gardener like myself help you with?
    Player: I�ve delivered your letter to Elstan, he told me to give you his regards.
    Rind the gardener: Wonderful, that�s just wonderful! Here, have these two seeds as a reward. They may not be Kelda hop seeds, but you should be able to grow a decent amount of herbs with them. 

The Good Shtuff
�� Once the Kelda Hops are grown ��

Perhaps I should take a look and see if my Kelda hops have grown�
�� Once the Kelda Stout has finished brewing ��

Perhaps I should have a look and see if my Kelda Stout has brewed�
A Forgettable Tale...

    Player: I need to know about the Red Axe�
    Drunken Dwarf: I want my Kelda shtout!
    Player: I�ve got some Kelda stout right here. I just brewed it in the Laughing Miner!
    Drunken Dwarf: Now there�sh a good human, gimme that!
    Player: Are you sure? This is pretty bad stuff.
    Drunken Dwarf: You want to hear about the Red Kebabsh or what? I mean� the Red Axe.
    Player: Here you go then.
    Drunken Dwarf: Ooooh, THAT�sh the good shtuff! The Red Axe? You want to know about them, do you? Then let me tell you my tale. It is long and quite unforgettable� A long time ago, quite a while ago, yes� I resigned from my mining company, the Magenta Accordion.
    Player: The what? The Magenta Accordion? That�s not a mining company!
    Drunken Dwarf: It is, it is! It�s one of the many minor companies, not part of the Consortium.
    Player: Alright, if you say so�
    Drunken Dwarf: Right, so I had just left my company. I�d had enough of slaving away in the mines and decided to become an explorer. Oh yes, like my cousin. I explored this old mine, you know, just south of Keldagrim. I think it�s boarded up now. Some amazing things I saw there. First of all� 

�� Cutscene starts ��

(Fade out / in)

    Drunken Dwarf: �and then I nearly fell into the abyss! But I� 

(Fade out / in)

    Drunken Dwarf: �so I had to run for my life, but then� 

(Fade out / in)

    Drunken Dwarf: �pink dragons� 

�� Cutscene ends ��

    Drunken Dwarf: �and I don�t really remember anything after that. People think I�m a drunken fool.
    Player: That was very interesting! I�ll ignore that part with the pink dragons though� But I shall investigate that tunnel immediately! South of the cart station, just south of the carts to White Wolf mountain?
    Drunken Dwarf: If you shay sho� I just want to smash up shome shtuff now. 

The Secret to the South
Boarded Up

    Cart conductor: Tickets, tickets, get your tickets here!
        Ask about closed off tunnel.
            Player: Excuse me, can you tell me what�s behind that boarded up tunnel to the south?
            Cart conductor: Why, that part of the mines hasn�t been used in a long time. I don�t rightly know what you would find behind there.
            Player: I need to go through there. Can you open up the tunnel for me?
            Cart conductor: I don�t have the authority to do that kind of thing.
            Player: Then who do I talk to? I really need to get in.
            Cart conductor: I don�t know. Perhaps you have an influential friend? 
        Ask any other question. 

Pulling a Few Strings
�� Talk to your company director from the Giant Dwarf ��

    [Company] Director: Hello [Player]! Have you seen the statue? It looks beautiful! It�s a pity it�s been overshadowed by the departure of the Red Axe.
        Player: It�s an honour to meet you!
        Player: Do you have any more tasks for me?
        Player: Can you help me with a boarded up tunnel?
            [Company] Director: Ah, and what would be lurking behind this particular tunnel?
            Player: I think it leads to something that might be related to the Red Axe.
            [Company] Director: The Red Axe! This is indeed an important issue. They�ve gone too far this time. A major company willingly leaving the Consortium has never happened before.
            Player: Can you help me get the boarding removed then?
            [Company] Director: Forthwith, [Player], forthwith! I�ll order the removal at once!
            Player: Thanks! I�ll investigate the tunnel immediately! 
        Player: The trade floor guards keep attacking me!
        Player: Thank you for your time. 

Red Axe Revelations
Railway Romp

    Player: I�m deep underground now, but how am I supposed to get across here? 

That Pesky Human!

    Red Axe Henchman: What�s the new plan?
    Ogre shaman: Me dead human!
    Red Axe Director: No, not yet. He may still prove useful to us.
    Red Axe Henchman: My axe on his head would be more useful!
    Red Axe Director: Forget about the human and the statue for now. Our experiments are more important.
    Red Axe Henchman: We are nearly ready with our next test.
    Ogre shaman: More little fire lookers?
    Red Axe Director: This time they�ll survive, I hope.
    Red Axe Henchman: Our slaves are too weak, it will never work.
    Red Axe Director: Yes, things would have been so much easier with the statue.
    Red Axe Henchman: It was a great plan! Sabotaging the base of the statue and bribing that boatman to crash into it!
    Red Axe Director: And we would have gotten away with it too, if it wasn�t for that pesky human.
    Player: Pesky human, huh? Well, I think I have enough info now. I�d better explore the rest of these mines. 

Minecart Madness

Player: Looks like another chasm I need to cross.
Red Axe Records
�� Looking at a random bookcase ��

    Player: Statistical data, salary structures, shopping lists� no, I don�t think there�s anything interesting here. 

�� Searching the first crate ��

You find a copy of a letter, addressed to the boatman that brought you to Keldagrim a while back.

I am writing to advise you that our secret preparations to the statue have been completed. All you need to do now is hit it with your boat. Take some gullible human along when you do, the mines are crawling with them these days. The human will surely get the major blame. 
After this, the statue is going to be rebuilt in the image of our great director. You need not concern yourself with the how and why, but rest assured you will be compensated greatly.
Regards,
Colonel Grimsson of the Red Axe


�� Searching the second crate ��

You find something that seems to shed some light on what happened to the drunken dwarf in Keldagrim. You should read it carefully before moving on.

Report on Intruder
We found a dwarf wandering in our mines. We weren�t sure if he was a spy or simply an explorer, but we couldn�t take any risks. We decided to send him back with his memory wiped, rather than put him to use as one of our test subjects. We can�t afford having to explain another mysterious disappearance at this point. Additionally, we added something to the spell that would make him hungry for kebabs and thirsty for beer, so that whatever he can remember will be dismissed as the ravings of a drunken fool. Grunsh did a good job, he seems to be fitting in well. Nevertheless, the spell seems to have been a little bit too strong, the subject is unlikely to recover from his condition. His obsession with kebabs is a bit too extreme. Will tell Grunsh to adjust his spell next time.


�� Searching the bookcase ��

You find an interesting book that seems to contain a lot of data about Red Axe employees. You should read it fully before continuing.

Red Axe Employee Records

Hreidmar, director of the Red Axe (employee #1)
Information classified.

Grimsson, the butcher of Barendir (employee #4297)
Grimsson was once a soldier in the dwarven army and quickly made his way up the ranks, up to colonel. He served with the Black Guard at Ice Mountain and in Kandarin. He was present at the battle of Barendir when he fought against an army of trolls. It was here that he got his nickname, the Butcher of Barendir, as he mercilessly killed the trolls even as they retreated in defeat. Here, also, he got his distinctive scar running across his forehead in a hand-to-hand battle with a particularly ferocious troll. After this battle Grimsson became more and more aggressive, even to his fellow dwarves. Following a particularly vicious attack on a comrade, he was dismissed from the Black Guard. Soon after, he was recruited by the Red Axe. Here he�s learned to control his rage and become even stronger. By now, he has become the right hand dwarf of the director. While he holds no official rank, he is still often affectionately referred to as �The Colonel�.

Grunsh, Ogre Shaman (employee #38262)
Found lost and near death in the ice cold mountains above Keldagrim, Grunsh was nearly mistaken for a troll at first. Taken in by the Red Axe and nursed back to health, it turned out that he was in fact a powerful ogre shaman. Intelligent by ogre standards, and steadfastly loyal to his rescuers, he has taken up a prominent position in the ranks of the Red Axe. This is primarily due to the fact that since dwarves cannot use magic, they must rely on other races as spellcasters. Grunsh fits the bill perfectly.

Chasm Challenge

    Player: What the Guthix is this? Another chasm? And even more tracks! 

Caught Red-Handed

    Player: There�s the Red Axe director again!
    Red Axe Director: How did the experiment go?
    Colonel Grimsson: The test subjects are still alive, director. They will suffice as soldiers.
    Red Axe Director: Excellent! We are improving. Laneel! Return to Arposandra and tell your superiors we�ve had a partial success.
    Gnome emissary: I will tell them, but I�d like to see some of your test subjects first, if I may.
    Colonel Grimsson: Look, here comes the first group!
    Player: Chaos dwarves!!! I must tell Veldaban!!!
    Grunsh: Lookings lieks we�s finding da spying creatures�ies!
    Player: Nooooooo! 

�� The screen fades out/in back to the entrance to the tunnel ��

    Player: Er, what? Where am I? Errrrr� I must tell Veldaban� something. Yes� ehm. I don�t remember what though, Errr� 

Memory Loss
Reporting Back (in a sense)

    Player: Veldaban, I must tell you something incredibly important!
    Commander Veldaban: What is it, [Player]?
    Player: I must tell you about� about� about my incredible craving for a kebab. Mmm, kebabs. A kebab and a beer, that sounds great right now.
    Commander Veldaban: Errr, are you alright? Don�t you have any important information about the Red Axe?
    Player: Mmm, yes, I�ll be perfectly alright� just after I�ve had my beer and a kebab. Yes. I�ll have a beer and a kebab in the Laughing Miner. Yum! Watch out kebabs, I come for you!
    Commander Veldaban: Poor [Player], I think you�ve been bewitched. 

Pub Party

    Player: I knew it were you matey! �Ere, have some of the good stuff!
    Drunken Dwarf: Very kind ob you, [Player]!
    Nolar: Tell us another tale, [Player]!
    Factory Worker: Tell us more about your kebabs!
    Gauss: Haha, kebabs!
    Player: Right, these kebabsh, right? *hic* A whole g-g-group of kebabs came marching in! I�m telling you! Marching in line! And then, I�ll tell you� Then! Ehm� Then I�ll have another drink!
    Colonel Grimsson: Ha, the spell has worked its magic again. No one will believe that drunken fool now! 

�� QUEST COMPLETE ��