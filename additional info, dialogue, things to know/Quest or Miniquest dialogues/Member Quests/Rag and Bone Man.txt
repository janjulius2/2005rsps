Rag and Bone Man Quest by Draugen

X = Unique player name.
 *You find an artefact.* = Text that pops up telling you something which isn't a conversation.
"Blue typing starts" and "Blue typing ends" = Just a way of indicating what colour and where it starts/ends.
Player searching crate. = Something you do, which I have included to give a better picture of where in the quest dialogues, items or pop ups belong.

_________________________________________________________________________________________________________

Talking to Odd Old Man.

Odd Old Man: Can I help you with something?
Player: Well, err ...who are you, and what are all these bones doing here?
Odd Old Man: Err...
Sack: Mumblemumble.
Odd Old Man: I'm an archaeologist and I work with the museum.
Player: An archaeologist?
Odd Old Man: Yes.
Player: Well, that explains the bones - sort of - but what are you doing all the way out here?
Odd Old Man: Err...
Sack: Mumblemumble. Mumblemumblemumble.
Odd Old Man: I'm collecting bones for the museum.
Odd Old Man: They've asked me to rig up some displays of Second and Third Age creatures, using their bones, so that people can come and, well, look at them.
Odd Old Man: I need to get them into some sort of order before I begin, but I've run into a bit of a snag.
Player: What sort of snag?
Odd Old Man: Well, I need to have all the bones I'm going to use here first, then I've got to put them in some sort of order.
Odd Old Man: I seem to be a few bones short of a skeleton, though.

 Select an Option
1. Anything I can do to help?
2. Well, good luck with that.
3. Where is that mumbling coming from?
_________________________________________________________________________________________________________

 Option 1.
Player: Anything I can do to help?
Odd Old Man: Err...
Sack: Mumblemumble.
Odd Old Man: There is something you could do for me. I'm going to be busy, er...
Sack: Mumble.
Odd Old Man: Sorting! Yes, sorting these bones out ready for the museum. I still need a few more, though.
Odd Old Man: Will you help me out?

 Select an Option
1. Yes.
2. No.
3. Where is that mumbling coming from?

 Option 1.
Player: Yes. I'll give you a hand.
Odd Old Man: You will? Excellent!
Sack: Sniggersnigger.
Player: Where do you need me to dig?
Odd Old Man: Dig?
Sack: Mumblemumble.
Odd Old Man: Oh, you must have got the wrong end of the stick.
Odd Old Man: I need some fresh, whole bones to replace ones that have become damaged.
Player: What?
Odd Old Man: Err...
Sack: Mumblemumblemumblemumblemumble. Mumblemumblemumblemumble. Mumble.
Player: Excuse me?
Sack: Shh!
Player: Sorry...
Sack: Mumblemumble. Mumblemumblemumble.
Odd Old Man: Okay, got it.
Odd Old Man: Here's the thing: while sorting out what bones I do have I managed to lose or damage a few. If you can get me some fresh, unbroken bones to use as replacements, then I can get on with things.
Odd Old Man: Does that make things clearer?
Player: Well, it makes some sense I suppose...
Odd Old Man: Great. If you can get me a bone from a goblin, a bear, a big frog, a ram, u unicorn, a monkey, a giant rat and a giant bat, then I'll be able to move on with the, er...
Odd Old Man: ...displays.
Player: So you just want me to bring these bones and that will be that?
Odd Old Man: Well, I wouldn't mind you boiling them in vinegar first. If you don't mind.
Odd Old Man: There is a wine merchant in Draynor called Fortunato who sells the stuff you'll need.
Odd Old Man: You can even use my pot boiler if you want.
Player: Why do I need to boil them in vinegar?
Odd Old Man: It gets them bright and sparkling white...
Odd Old Man: It's an archaeologist thing.
Odd Old Man: Just put the bone in a pot of vinegar, throw some logs on the fire, put the pot on the boiler, then light the logs.
Odd Old Man: It takes a short while for the vinegar to evaporate, but the bone will be nice and clean in the end.
Player: Alright, I'll be back later.
Odd Old Man: Bye!

 Option 2.
Player: No.
Odd Old Man: Oh, I see.
Odd Old Man: Well, never mind me, young man, I'll just stagger over here under my massive burden, and continue my thankless task.
Odd Old Man: Unaided and alone...
Player: Wow, trying a guilt trip much?
_________________________________________________________________________________________________________

 Option 2.
Player: Well, good luck with that!
Odd Old Man: Thanks, stranger!
Odd Old Man: What a polite young man...
Odd Old Man: Well, back to work!
_________________________________________________________________________________________________________

 Option 3.
Player: Where is that mumbling coming from?
Odd Old Man: Err...
Sack: Mumblemumble.
Odd Old Man: What mumbling?
Odd Old Man: I have enough problems of my own without having to deal with a delusional adventurer.
_________________________________________________________________________________________________________

Talking to Odd Old Man again.

Odd Old Man: Have you brought me any bones?
Player: Not at the moment. Can you give me a rundown on which bones I have left to get?
Odd Old Man: Sure.
Sack: Mumblemumble.
Odd Old Man: You still need to bring me a goblin bone.
Odd Old Man: Goblins are relatively common. In fact, I hear there is a house full of them in Lumbridge.
Sack: Mumblemumble.
Odd Old Man: You still need to bring me a bear bone.
Odd Old Man: I heard that there are some bears over by the Legends' Guild, near Ardougne.
Sack: Mumblemumble.
Odd Old Man: You still need to bring me a big frog bone.
Odd Old Man: This one might be a little tricky. You'll need to go into the Lumbridge Swamp DUngeon. Don't forget to take a light source! Never forget your light source down there.
Sack: Mumblemumble.
Odd Old Man: You still need to bring me a ram bone.
Odd Old Man: I'm sure you will be able to find a ram wherever there are sheep.
Sack: Mumblemumble.
Odd Old Man: You still need to bring me a unicorn bone.
Odd Old Man: I seem to remember seeing some unicorns south of Varrock. I think they might still be there.
Sack: Mumblemumble.
Odd Old Man: You still need to bring me a monkey bone.
Odd Old Man: Monkeys tend to live in jungles, like those on karamja. I think they are pretty plentiful there, if I remember correctly.
Sack: Mumblemumble.
Odd Old Man: You still need to bring me a giant rat bone.
Odd Old Man: If you can't find one in a sewer, then you might want to try looking in some caves.
Odd Old Man: Mumblemumble.
Odd Old Man: You still need to bring me a giant bat bone.
Odd Old Man: Giant bats tend to live underground, but I have heard there are a few near the coal trucks in Kandarin.
Odd Old Man: Did you get all that?
Player: Yes. I'll get right on it.
Sack: Mumblemumble.
Odd Old Man: Don't forget to boil them in vinegar first.
Odd Old Man: Just chuck some logs into the pit, put the bone in the pot of vinegar and drop it into the pot boiler. Then light the logs and wait for the vinegar to boil away.
Player: Okay, I'll remember.

Note - The odd old man will ramble like this if you miss all the bones. Should you have 7 bones, he will tell you what the last one remaining is and where it can be found.
_________________________________________________________________________________________________________

Player going to Draynor Village Square.

Player talking to Fortunato.

Fortunato: Can I help you at all?
Player: Have you got any vinegar?
Fortunato: Vinegar?
Fortunato: Vinegar!
Fortunato: The very idea! My wines are the finest in the district!
Player: Sorry. I was told to come and speak with a wine merchant about getting some vinegar; I must have the wrong person.
Fortunato: Wait! Did 'HE' send you?
Player: If by HE you mean the old man with the sack of bones, then yes.
Fortunato: Aaah...say no more sir (or milady). I have some in specially.
This leads to his stock also including jug of vinegar.
_________________________________________________________________________________________________________

Talking to Fortunato again.

Fortunato: Ah! Good afternoon, your lordship (or ladyship). I take it you have come for a refill?

 Select an Option
1. Yes.
2. Not today.

 Option 1.
Player: yes.
 
 Option 2.
Player: Not today.
Fortunato: Well, just remember I have some in stock for you when you want it.
_________________________________________________________________________________________________________

Player using Jug of vinegar on Empty pot and gets pot of vinegar.

Player killing listed monsters and picking their bones.

Player using the following items on Pot of vinegar: Goblin skull, Big frog leg, Ram skull, Unicorn bone, Monkey paw, Giant rat bone, Giant bat wing.
By doing this you will recieve 8 Bone in vinegar.
_________________________________________________________________________________________________________

Player returning to odd old man's museum in lack of a better word.

Player using Bone in vinegar on Odd Old Man.

Player: I have some bones for you.
Odd Old Man: Great! Just clean them up in the pot boiler and hand them over.

Player using logs on Pot boiler.

Player using Bone in vinegar on Pot boiler.

Player lighting Pot boiler.

Player removing the polished bone from the Pot boiler.

This is repeated 8 times.
_________________________________________________________________________________________________________

Talking to Odd Old Man.

Player: I have some bones for you.
Odd Old Man: Great! Let me take a look at them.
Odd Old Man: That's the last of them!
Odd Old Man: the museum will be thrilled to know I've completed the collection.
Player: Well, I'm just glad I could help.
Sack: Mumblemumble.
Odd Old Man: You've been a big help and no mistake.
Odd Old Man: I'm always on the lookout for fresh bones, so if you see any of the ones on my wish list, bring them right over.
Odd Old Man: I have it piined to the outside of my shack whenever you want to check it.
Player: No problem, I'll be sure to bring you anything you might like.
Player: I can't wait to see the displays once they are finished.
_________________________________________________________________________________________________________

"Gold/orange typing starts" Congratulations! "Gold/orange typing ends"

You have completed Rag and Bone Man!

You are awarded: 

2 Quest points
500 Cooking XP and 500
Prayer XP

"Gold/orange typing starts" Quest Points: Insert here "Gold/orange typing ends"