Sea Slug Quest by Draugen

X = Unique player name.
 *You find an artefact.* = Text that pops up telling you something which isn't a conversation.
"Blue typing starts" and "Blue typing ends" = Just a way of indicating what colour and where it starts/ends.
Player searching crate. = Something you do, which I have included to give a better picture of where in the quest dialogues or pop ups belong.
_________________________________________________________________________________________________________

Player talking to Caroline a little east of Ardougne near the coast.

Player: Hello there.
Caroline: Is there any chance you could help me?
Player: What's wrong?
Caroline: It's my husband, he works on a fishing platform. Once a month he takes our son, Kennith, out with him.
Caroline: They usually write to me regularly, but I've heard nothing all week. It's very strange.
Player: Maybe the post was lost!
Caroline: Maybe, but no'one's heard from the other fishermen on the platform. Their families are becomming quite concerned.
Caroline: Is there any chance you could visit the platform and find out what's going on?

 Select an Option
1. I suppose so, how do I get there?
2. I'm sorry, I'm too busy.

 Option 1.
Player: I suppose so, how do I get there?
Caroline: That's very good of you X. My friend Holgart will take you there.
Player: Ok, I'll go and see if they're ok.
Caroline: I'll reward you for your time. It'll give me peace of mind to know Kennith and my husband, Kent, are safe.
 
 Option 2.
Player: I'm sorry, I'm too busy.
Caroline: That's a shame.
Player: Bye.
Caroline: Bye.
_________________________________________________________________________________________________________

Talking to Caroline again.

Player: Hello Caroline.
Caroline: Brave X, have you any news about my son and his father?
Player: I'm working on it now Caroline.
Caroline: Please bring them back safe and sound.
Player: I'll do my best.
_________________________________________________________________________________________________________

Player talking to Hogard.

Player: Hello.
Holgart: Hello m'hearty.
Player: I would like a ride on your boat to the fishing platform.
Holgart: I'm afraid it isn't sea worthy, it's full of holes. To fill the holes I'll need some swamp paste.
Player: Swamp paste?
Holgart: Yes, swamp tar mixed with flour and heated over a fire.
Player: Where can I find swamp tar?
Holgart: Unfortunately the only supply of swamp tar is in the swamps below Lumbridge. It's too far for an old man like me to travel.
Holgart: If you make me some swamp paste I'll give you a ride in my boat.
Player: I'll see what I can do.
_________________________________________________________________________________________________________

Talking to Holgart again without swamp paste.

Player: Hello.
Holgart: Hello m'hearty. Did you manage to make some swamp paste?
Player: I'm afraid not.
Holgart: It's simply swamp tar mixed with flour heated over a fire. Unfortunately the only supply of swamp tar is in the swamps below Lumbridge.
Holgart: I can't fix my row boat without it.
Player: OK, I'll try to find some.
_________________________________________________________________________________________________________

Player talking to Holgart with Swamp paste.

Player: Hello.
Holgart: Hello m'hearty. Did you manage to make some swamp paste?
Player: Yes, I have some here.

 *You give Holgart the swamp paste.*

A ten seconds long cutscene appears.

Holgart: Right me hearty. Let's get this boat fixed.
Holgart: Hozzah! We've fixed the leaky old tub.
_________________________________________________________________________________________________________

Player talking to Holgart after boat is fixed.

Player: Hello Holgart.
Holgart: Hello again land lover. There's some strange goings on, on that platform, I tell you.

 Select an Option
1. Will you take me there?
2. I'm keeping away from there.

 Option 1.
Player: Will you take me there?
Holgart: Of course m'hearty. If that's what you want.

 Option 2.
Player: I'm keeping away from there.
Holgart: Fair enough m'hearty.
_________________________________________________________________________________________________________

Player travelling with row boat to the fishing platform.

 *You arrive at the fishing platform.*
_________________________________________________________________________________________________________

Player talking to Holgart at the platform.

Player: Hey, Holgart.
Holgart: Have you had enough of this place yet? It's really starting to scare me.

 Select an Option
1. No, I'm going to stay a while.
2. Okay, let's go back.

 Option 1.
Player: No, I'm going to stay a while.
Holgart: Okay... you're the boss.

 Option 2.
Player: Okay, let's go back.
Holgart: Okay m'hearty, jump on.

 *The boat arrives at Witchaven.*
_________________________________________________________________________________________________________

Player going to the kitchen on the platform.

Player talking to Bailey.

Player: Hello there.
Bailey: What? Who are you? Come inside quickly!
Bailey: What are you doing here?
Player: I'm trying to find out what happened to a boy named Kennith.
Bailey: Oh you mean Kent's son. He's around somewhere, probably hiding if he knows what's good for him.
Player: Hiding from what? What's got you so frightened?
Bailey: Haven't you seen all those things out there?
Player: The sea slugs?
Bailey: It all began about a week ago. We pulled up a haul of deep sea flatfish. Mixed in with them we found those slug things, but thought nothing of it.
Bailey: Not long after that my friends began to change, now they spend all day pulling in hauls of fish, only to throw back the fish and keep those nasty sea slugs.
Bailey: What am I supposed to do with those? I haven't figured out how to kill one yet, if I put them near the stove they squirm and jump away.
Player: I doubt they would taste too good.
Bailey: This is no time for humour.
Player: I'm sorry, I didn't mean to upset you.
Bailey: That's okay. I just can't shake the feeling that this is the start of something... Terrible.

Player climbing up ladder.

Player opening door to room.

Player talking to Kenneth through crates.

Player: Are you okay young one?
Kennith: No, I want my daddy!
Player: Where is your father?
Kennith: He went to get help days ago.
Kennith: The nasty fishermen tried to throw me and daddy into the sea. So he told me to hide here.
Player: That's a good advice, you stay here and I'll go try and find your father.
_________________________________________________________________________________________________________

Talking to Kennith again results in the following.

Player: Are you okay?
Kennith: I want to see daddy!
Player: I'm working on it.
_________________________________________________________________________________________________________

Player climbing down ladder.

Player talking to Holgart.

Player: Holgart, something strange is going on here.
Holgart: You're telling me, none of the sailors seem to remember who I am.
Player: Apparently Kennith's father left for help a couple of days ago.
Holgart: That's a worry, no-one's heard from him on shore. Come on, we'd better go look for him.
_________________________________________________________________________________________________________

Player travelling with row boat.

 *You arrive on a small island.*
_________________________________________________________________________________________________________

Player talking to Holgart.

Player: Where are we?
Holgart: Someway off mainland still. You'd better see if me old matey's okay.

Player talking to Kent.

Kent: Oh thank Saradomin! I thought I'd be left out here forever.
Player: Your wife sent me out to find you and your boy. Kennith's fine by the way, he's on the platform.
Kent: I knew the row boat wasn't sea worthy. I couldn't risk bringing him along but you must get him off that platform.
Player: What's going on here?
Kent: Five days ago we pulled in a huge catch. As well as fish we caught small slug like creatures, hundreds of them.
Kent: That's when the fishermen began to act strange.
Kent: It was the sea slugs, they attach themselves to your body and somehow take over the mind of the carrier.
Kent: I told Kennith to hide until I returned but I was washed up here.
Kent: Please go back and get my boy, you can send help for me later.

Kent: X wait!

Kent: A few more minutes and that thing would have full control of your body.
Player: Yuck! Thanks Kent.
_________________________________________________________________________________________________________

Talking to Kent again results in this.

Player: Hello.
Kent: Oh my, I must get back to shore.
_________________________________________________________________________________________________________

Player talking to Holgart.

Player: We'd better get back to the platform so we can see what's going on.
Holgart: You're right. It all sounds pretty creepy.

 *You arrive at the fishing platform.*
_________________________________________________________________________________________________________

Player going go kitchen and talking to Bailey again.

Player: Hello.
Bailey: Oh, thank the gods it's you. They've all gone mad I tell you, one of the fishermen tried to throw me into the sea!
Player: They're all being controlled by the sea slugs.
Bailey: I figured as much.
Player: I need to get Kennith off this platform, but I can't get past the fishermen.
Bailey: The sea slugs are scared of heat, I figured that out when I tried to cook them.
Bailey: Here.
 
 *Bailey gives you a torch.*

Bailey: I doubt the fishermen will come near you if you can get this torch lit.
Bailey: The only problem is all the wood and flint are damp... I can't light a thing!
_________________________________________________________________________________________________________

Player rubbing together dry sticks, making the torch lit.

Player climbing up ladder.
_________________________________________________________________________________________________________

Player talking to Kennith.

Player: Hello Kennith, are you okay?
Kennith: No, I want my daddy.
Player: You'll be able to see him soon. First we need to get you back to land, come with me to the boat.
Kennith: No!
Player: What, why not?
Kennith: I'm scared of those nasty sea slugs. I won't go near them.
Player: Okay, you wait here and I'll figure another way to get you out.
_________________________________________________________________________________________________________

Player kicking badly repaired wall.
_________________________________________________________________________________________________________

Player talking to Kennith.

Player: Kennith, I've made an opening in the wall. You can come out through there.
Kennith: Are there any sea slugs on the other side?
Player: Not one.
Kennith: How will I get downstairs?
Player: I'll figure that out in a moment.
Kennith: Ok, when you have I'll come out.
_________________________________________________________________________________________________________

Player rotating crane. 

A 10 seconds long cutscene appears of player lifting the boy down to the row boat using the crane.

 *Kennith scrambles through the broken wall...*

 *Down below, you see Holgart collect the boy from the crane and lead him away to safety.*

Player climbing down ladder.
_________________________________________________________________________________________________________

Player talking to Holgart.

Player: Did you get the kid back to shore?
Holgart: Yes, he' safe and sound with his parents. Your turn to return to land now adventurer.
Player: Looking forward to it.

 *The boat arrives at Witchaven.*
_________________________________________________________________________________________________________

Player talking to Caroline.

Player: Hello.
Caroline: Brave X, you've returned!
Caroline: Kennith told me about the strange goings-on at the platform. I had no idea it was so serious.
Caroline: I could have lost my son and my husband if it wasn't for you.
Player: We found Kent stranded on an island.
Caroline: Yes, Holgart told me and a rescue party went out. Kent's back home now, resting with Kennith. I don't think he'll be doing any fishing for a while.
Caroline: Here, take these Oyster pearls as a reward. They're worth quite a bit and can be used to make lethal crossbow bolts.
Player: Thanks!
Caroline: Thank you. Take care of yourself X.
_________________________________________________________________________________________________________

"Gold/orange typing starts" Congratulations! "Gold/orange typing ends"

You have completed the Sea Slug Quest!

You are awarded: 

1 Quest Point
7,125 Fishing XP
Oyster pearls

"Gold/orange typing starts" Quest Points: Insert here "Gold/orange typing ends"