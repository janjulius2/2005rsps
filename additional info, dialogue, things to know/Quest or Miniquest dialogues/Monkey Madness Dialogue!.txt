case ?:
player.getDialogue().sendOption("", "");
return true;
case ?:
switch(optionId) {
case 1:
player.getDialogue().sendPlayerChat("", CONTENT);
player.getDialogue().setNextChatId(?);
return true;
case 2:
player.getDialogue().sendPlayerChat("", CONTENT);
player.getDialogue().setNextChatId(?);
return true;
}
break;



player.getDialogue().sendGiveItemNpc("", new Item(2407, 1));

player.getDialogue().sendPlayerChat("", CONTENT);
player.getDialogue().sendNpcChat("", CONTENT);


Player: 
Monkey minder: 


Player: 
Monkey minder: 
b
Player: Ok!
Monkey minder: 
Player: 
Monkey minder: 
�� Speak to the Monkey ��
Player: Hello there, little monkey.
Monkey: Hello there!
Player: How would you like to get out of here?
Monkey: Escape!? It's all I ever think about!
Player: That's convenient. When would you like to leave?
Monkey: Where will you be taking me?
Player: Erm... to the happy, sunny jungle of Karamja...
Monkey: Wowee! I was born there you know!
Player: That's nice. Are you ready to go?
Monkey: Yes. Actually, can I bring some of my friends?
Player: No, I only have space for one.
Monkey: Pleeeeease?
Player: No!
Monkey: Pretty pleeeeease?
Player: No!!
Monkey: Pretty please with a banana on top?
Player: Look, I already said no. If you want to come then jump into my backpack.
Monkey: Ook!
�� Speak to the Monkey minder again ��
Monkey minder: My word - what are you doing in there?
Player: I... er... I don't know! One minute I was asleep, and the next minute I was here surrounded by monkeys!
Monkey minder: Well, don't worry. We'll have you out of there shortly.
Player: Thank you.
Monkey minder: No problem.
A Monkey King's TrustEdit
Awowogei: Have you brought with you a captive?
Player: Yes, I have.
Awowogei: Well done! You have shown yourself to be very resourceful. You have managed to complete an extremely long journey remarkably quickly.
Player: Thank you.
Awowogei: You are clearly well acquainted with the ways of this world. We will talk more on this later. In the meantime, feel free to remain as long as you like on my island.
Player: What about the proposed alliance, Awowogei?
Awowogei: I must think upon it some more and discuss the matter with my advisors. We will contact you when we are ready.
The Conspirators OverheardEdit
Garkor: Well done on winning Awowogei's trust. I overheard everything from here. However, your efforts may be in vain...
Player: What do you mean?
Garkor: Progress in the caves has been slow. Whilst you were in Ardougne, Bunkwicket and Waymottin overheard a slightly disturbing conversation.
Player: Who was speaking? What was said?
Garkor: Listen closely whilst I narrate the details...
Somewhere far below the Ape Atoll...
G.L.O. Caranock: Good evening, Awowogei.
Awowogei: It is always dark here, Gnome. Why have you asked to see me in private?
Waydar: Caranock and I have a suggestion to make.
Awowogei: Then be quick about it.
Waydar: The foot soldiers of the Royal Guard in your jail...
G.L.O. Caranock: Would it not be easier if they were somehow just to... die?
Awowogei: Why would I want to do that? Your king would declare war on my island.
G.L.O. Caranock: I can assure you he will not. We will lay the blame at the humans' feet. Narnode will indeed declare war: not against you, but against humankind.
Waydar: You are of course welcome to your share of the profits.
Awowogei: Intriguing. I have recently secured an alliance with the northern monkeys, which may prove useful. What would you have me do?
G.L.O. Caranock: Kill the foot soldiers and the rest of the 10th squad. My superior has sent you a few tricks which may prove useful.
Awowogei: Such as?
G.L.O. Caranock: High magic: the ability to summon the entire 10th squad to a single location. And -
Awowogei: Even those who escaped?
G.L.O. Caranock: Yes. And of course, you will also receive access to one of his �pets'. You must be careful with these, as you have only one use of each. Ensure you set your trap well - none must survive lest they spread the truth.
Waydar: What of my human?
Awowogei: What human??
G.L.O. Caranock: Ignore him. My colleague's official mission was to look after a human in the area, but don't worry: it is probably dead already.
Awowogei: I should hope so, for both of your sakes. Very well. I shall let you know when I have dealt with the Royal Guard.
G.L.O. Caranock: Good luck, Awowogei.
Awowogei: With access to one of Glough's �pets', I don't think I'll need it...
Monkey Madness: Chapter 4Edit
The Final Battle
Joining the TeamEdit
Player: What shall we do now?
Garkor: Zooknock and I have come up with a plan.
Player: What kind of a plan?
Garkor: I hope you were listening closely. The teleportation spell that was provided will teleport ALL of the 10th squad, no matter where we may be. In effect, the spell will break Lumo, Bunkdo and Carado out of the jail for us.
Player: But you will be teleported straight into whatever trap they have prepared!
Garkor: Indeed. This is where you come in. Do not forget that we are the 10th squad of the Royal Guard, and that we are more than capable of holding our own. With your assistance, we should be able to defeat whatever is thrown at us.
Player: But how will I join you?
Garkor: Simple. We fool the teleportation spell that you are in fact a member of our squad.
Player: What?
Garkor: Zooknock knows Glough's grasp of magic well. He believes the spell is linked to the sigils that all of our squad carry. It is these sigils that identify us as a member of the squad.
Garkor hands you some kind of medallion.
Garkor: Welcome to the 10th squad, [Player].
Player: What is it?
Garkor: It is a replica Waymottin has made of our squad sigils. If you wear that when the spell is cast, you will be summoned along with the rest of us. You should prepare. Collect your thoughts and belongings and then wear the sigil. Hurry, human, we do not wish to enter this fight without you.
Player: All I have to do is wear this sigil?
Garkor: Yes - but do not do so until you are ready.
Dance with a DemonEdit
Garkor: Ready yourself, human: the final battle begins!

�� Defeat the Jungle Demon ��
Garkor: Well done, human! That was a most impressive display of skill.
Player: Thank you.
Garkor: You should report to King Narnode immediately. Tell him that the 10th squad still survives and has suffered no casualties.
Player: Rest assured, I will do so. How do I leave this place?
Garkor: Speak to Zooknock. He will arrange for you to leave.

�� Speak to Zooknock ��
Zooknock: Well done, human. Bear with me now.
Finishing UpEdit
Player: King Narnode!
King Narnode Shareen: Yes? How is the mission going...it has been quite some time since I sent you on your way.
Player: It's over - it's finally over.
King Narnode Shareen: What do you mean �over'?
Player: I mean �finished'.
King Narnode Shareen: Yes, alright. Report on what happened.
Player: With all due respect sir, if I told you, you would not believe me. I expect Sergeant Garkor will be sending you a full report soon enough.
King Narnode Shareen: And what of my 10th squad?
Player: They all live - we suffered no casualties.
King Narnode Shareen: �We', [Player]?
Player: I, uh, I'm part of the 10th squad now, I even have the sigil.
King Narnode Shareen: Well, now. It appears I cannot argue with that. Garkor obviously thinks highly of you, as do I. No service such as what you have done for me goes unrewarded in my kingdom. I personally made a visit to the Royal Treasury to withdraw your reward.