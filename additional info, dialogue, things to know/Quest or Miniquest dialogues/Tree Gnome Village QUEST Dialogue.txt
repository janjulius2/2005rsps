player.getDialogue().sendPlayerChat("", CONTENT);
player.getDialogue().sendNpcChat("", CONTENT);

------------------------------------------------------------------

NPC ELKOY: We're out the maze now. Please hurry, we must have
the orb if we are to survive.
------------------------------------------------------------------

PLAYER: Hello.

COMMANDER MONTAL: Hello traveller, are you here to help or just to watch?

PLAYER: I've been sent by King Bolren to retrieve the orb of
protection.

COMMANDER MONTAL: Excellent we need all the help we can get.

COMMANDER MONTAL: I'm commander Montal. The orb is in the Khazard
stronghold to the north, but until we weaken their
defences we can't get close.

PLAYER: What can I do?

COMMANDER MONTAL: Firstly we need to strenghten our own defences. We
desperately need wood to make more battlements, once
the battlements are gone it's all over. Six loads of
normal logs should do it.

COMMANDER MONTAL: Option1 - Ok, I'll gather some wood.
                  Option2 - Sorry, I no longer want to be involved.

PLAYER: Ok, I'll gather some wood.

COMMANDER MONTAL: Please be as quick as you can, I don't know how much
longer we can hold out.
------------------------------------------------------------------

PLAYER: Hello.

COMMANDER MONTAL: Hello again, we're still desperate for wood soldier.

PLAYER: I have some here.
(You give six loads of logs to the commander.)

COMMANDER MONTAL: That's excellent, now we can make more defensive
battlements. Give me a moment to organise the troops
and then come speak to me. I'll inform you of our next
phase of attack.
------------------------------------------------------------------

PLAYER: So what's the problem?

COMMANDER MONTAL: From this distance we can't get an accurate enough
shot. We need the correct coordinates of the
stronghold for a direct hit. I've sent out three tracker
gnomes to gather them.

PLAYER: Have they returned?

COMMANDER MONTAL: I'm afraid not, and we're running out of time. I need
you to go into the heart of the battlefield, find the
trackers, and bring back the coordinates.

COMMANDER MONTAL: Do you think you can do it?

PLAYER: Option1 - No, I've had enough of your battle?
        Option2 - I'll try my best.

PLAYER[OP2]: I'll try my best.

COMMANDER MONTAL: Thank you, you're braver than most.

COMMANDER MONTAL: I don't know how long I will be able to hold out. Once
you have the coordinates come back and fire the ballista
right into those monsters.

COMMANDER MONTAL: If you can retrieve the orb and bring safety back to
my people, none of the blood spilled on this field will be
in vain.
------------------------------------------------------------------

PLAYER: Do you know the coordinates of the Khazard
stronghold?

NPC TRACKER GNOME 1: I managed to get one, although it wasn't easy.

*The gnome tells you the HEIGHT coordinate.*

PLAYER: Well done.

NPC TRACKER GNOME 1: The other two tracker gnomes should have the other
coordinates if they're still alive.

PLAYER: OK, take care.
------------------------------------------------------------------

PLAYER: Are you OK?

NPC TRACKER GNOME 2: They caught me spying on the stronghold. They beat
and tortured me.

NPC TRACKER GNOME 2: But I didn't crack. I told them nothing. They can't break me!

PLAYER: I'm sorry little man.

NPC TRACKER GNOME 2: Don't be. I have the position of the stronghold!

*The gnome tells you the Y COORDINATES.*

PLAYER: Well done.

NPC TRACKER GNOME 2: Now leave before they find you and all is lost.

PLAYER: Hang in there.

NPC TRACKER GNOME 2: Go!
------------------------------------------------------------------

PLAYER: Are you OK?

NPC TRACKER GNOME 3: OK? Who's OK? Not me! Hee hee!

PLAYER: What's wrong?

NPC TRACKER GNOME 3: You can't see me, no one can. Monsters, demons,
they're all around me!

PLAYER: What do you mean?

NPC TRACKER GNOME 3: They're dancing, all of them, hee hee.

*He's clearly lost the pilot.*

PLAYER: Do you have the coordinate for the Khazard
stronghold?

NPC TRACKER GNOME 3: Who holds the stronghold?

PLAYER: What?

NPC TRACKER GNOME 3: My legs and your legs, ha ha ha!

PLAYER: You're mad.

NPC TRACKER GNOME 3: Dance with me, and Khazard's men are beat.

*The toll of war has affected his mind.*

PLAYER: I'll pray for you little man.

NPC TRACKER GNOME 3: All day we pray in the hay, hee hee.
------------------------------------------------------------------

OBJ BALLISTA: That tracker gnome was a bit vague about the x
coordinate! What could it be?

Enter the x-coordinate of the stronghold: option1 - 0001
                                          option2 - 0002
                                          option3 - 0003
                                          option4 - 0004

PLAYER[OP4]: *You enter the height an y coordinates you got from the tracker
gnomes.

*The huge spear flies through the air and screams down directly into
the Khazard stronghold. A deafening crash echoes over the battlefield
as the front entrance is reduced to rubble.*
------------------------------------------------------------------

*The wall has been reduced to rubble. It should be possible to climb
over the remains.*
------------------------------------------------------------------

*You search the chest. Inside you find the gnomes' stolen orb of
protection.*
------------------------------------------------------------------

PLAYER: Hello Elkoy.

NPC ELKOY: You're back! And the orb?

PLAYERS: I have it here.

NPC ELKOY: You're our saviour. Please return it to the village and
we are all saved. Would you like me to show you the
way to the village?

NPC ELKOY: Option1 - Yes please.
           Option2 - No thanks Elkoy.

PLAYER[OP1]: Yes please.

NPC ELKOY: Ok then, follow me.
------------------------------------------------------------------

NPC ELKOY: Here we are. Take the orb to King Bolren, I'm sure
he'll be pleased to see you.

PLAYER: I have the orb.

NPC KING BOLREN: Oh my... The misery, the horror!

PLAYER: King Bolren, are you OK?

NPC KING BOLREN: Thank yu traveller, but it's too late. We're all doomed.

PLAYER: What happened?

NPC KING BOLREN: They came in the night. I don't know how many, but
enough.

PLAYER: Who?

NPC KING BOLREN: Khazard troops. They slaughtered anyone who got in their way. Women, children, my wife.

PLAYER: I'm sorry.

NPC KING BOLREN: They took the other orbs, now we are defenceless.

PLAYER: Where did they take them?

NPC KING BOLREN: They headed north of the stronghold. A warlord carries
the orbs.

NPC KING BOLREN: Option1 - I will find the warlord and bring back the orbs.
                 Option2 - I'm sorry but I can't help.

PLAYER[OP1]: I will find the warlord and bring back the orbs.

NPC KING BOLREN: You are brave, but this task will be tough even for you.
I wish you the best of luck. Once again you are our
only hope.

NPC KING BOLREN: I will safeguard this orb and pray for your safe return.
My assistant will guide you out.

NPC ELKOY: Good luck friend.
------------------------------------------------------------------

PLAYER: You there, stop!

NPC KHAZARD WARLORD: Go back to your pesky little green friends.

PLAYER: I've come for the orbs.

NPC KHAZARD WARLORD: You're out of your depth traveller. These orbs are part
of a much larger picture.

PLAYER: They're stolen goods, now give them here!

NPC KHAZARD WARLORD: Ha, you really think you stand a chance? I'll crush
you.
------------------------------------------------------------------
(When killing Khazard Warlord): *As the warlord falls to the ground, a ghostly vapour floats upwards
from his battle-worn armour. Out of sight you hear a schrill scream in
the still air of the valley. You search his satchel and find the orbs of
protection.*
------------------------------------------------------------------

PLAYER: Hello Elkoy.

NPC ELKOY: You truly are a hero.

PLAYER: Thanks.

NPC EKLOY: You saved us by returning the orbs of protection. I'm
humbled and wish you well.

NPC ELKOY: Would you like me to show you the way to the village?

NPC ELKOY: Option1 - Yes please.
           Option2 - No thanks Elkoy.

PLAYER[OP1]: Yes please.

NPC ELKOY: Ok then, follow me.
------------------------------------------------------------------

NPC ELKOY: Here we are. Feel free to have a look around.

PLAYER: Bolren, I have returned.

NPC KING BOLREN: You made it back! Do you have the orbs?

PLAYER: I have them here.

NPC KING BOLREN: Hooray, you're amazing. I didn't think it was possible
but you've saved us.

NPC KING BOLREN: Once the orbs are replaced we will be safe once more.
We must begin the ceremony immediately.

PLAYER: What does the ceremony involve?

NPC KING BOLREN: The spirit tree has looked over us for centuries. Now
we must pay our respects.

*The gnomes begin to chant. Meanwhile, King Bolren holds the orbs
of protection out in front of him.*

*The orbs of protection come to rest gently in the branches of the ancient spirit tree.*

NPC KING BOLREN: Now at last my people are safe once more. We can live
in peace again.

PLAYER: I'm pleased I could help.

NPC KING BOLREN: You are modest brave traveller.

NPC KING BOLREN: Please, for your efforts take this amulet. It's made
from the same sacred stone as the orbs of protection. It
will help keep you safe on your journeys.

PLAYER: Thank you King Bolren.

NPC KING BOLREN: The tree has many other powers, some of which I 
cannot reveal. As a friend of the gnome people, I can
now allow you to use the tree's magic teleport to 
other trees grown from related seeds.
------------------------------------------------------------------

Congratulations!
You have completed the Tree Gnome Village Quest!
You are awarded:
2 Quest Points
11,450 Attack XP
Gnome Amulet of Protection
------------------------------------------------------------------
Dream Desire/Krayziefaken