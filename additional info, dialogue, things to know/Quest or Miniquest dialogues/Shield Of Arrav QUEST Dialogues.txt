player.getDialogue().sendNpcChat("", CONTENT);
player.getDialogue().sendPlayerChat("", CONTENT);


NPC CURATOR HAIG HALEN: Welcome to the museum of Varrock.

PLAYER: I have the half shield of Arrav here. Can I get a reward?

NPC CURATOR HAIG HALEN: The Shield of Arrav! Goodness, the Museum has been searching for that for years! The late King Roald II offered a reward for it years ago!

PLAYER: Well, I'm here to claim it.

NPC CURATOR HAIG HALEN: Let me have a look at it first.

*The curator peers at the shield.*

NPC CURATOR HAIG HALEN: This is incredible!

NPC CURATOR HAIG HALEN: That shield has been missing for over twenty-five years!

NPC CURATOR HAIG HALEN: Leave the shield here with me and I'll write you out a certificate saying that you have returned the shield, so that you can claim your reward from the King.

PLAYER: Can I have two certificates please?

NPC CURATOR HAIG HALEN: Yes, certainly. Please hand over the shield.

*You hand over the shield half.*

*The curator writes out two certificates.
---------------------------------------------------------------

PLAYER: Greetings, your majesty.

PLAYER: Your majesty, I have come to claim the reward for the return of the Shield Of Arrav.

*You show the certificate to the king.*

NPC KING ROALD: My goodness! This claim is for the reward offered by my father many years ago!

NPC KING ROALD: I never thought I would live to see the day when someone came forward to claim this reward!

NPC KING ROALD: I heard that you found half of the shield, so I will give you half of the bounty. That comes to exactly 600gp!

*You hand over a certificate. The king gives you 600gp.*
---------------------------------------------------------------Congratulations!
You have completed the Shield Of Arrav Quest!

You are awarded:
1 Quest Point
600 Coins.
---------------------------------------------------------------
Dream Desire/Krayzie




























