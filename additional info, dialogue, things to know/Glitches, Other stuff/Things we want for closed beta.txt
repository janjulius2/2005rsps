Things for closed Beta

1 - Barrows - 100% (Minimap glitch must also be fixed)
2 - Magebank - 100%
3 - Client sided multi glitch (Add bones instead of arrows)
4 - Finish quest system
5 - Tutorial Island has to be atleast working
6 - Barrows Armour (Doesn't degrade, doesn't degrade on death, bob has to be able to fix it)
7 - All F2P NPCs & Try and get most of them talking with most the right dialogues
8 - Fix some duel arena glitches - Poison after duel & make it so sideicons are hidden when you open up duel
9 - Fix Dragons (Fire)
10 - Finish every boss 100% (Chaos Ele, Giant Mole, King Black Dragon, Kalphite Queen, Dagganoth Kings)
11 - fix the desert
13 - Code Tzhaar Area



Mod Josh
1 -Full screen interface texts
2 -Dragons
3 -KBD
4 -Add all quests in Playersave.java

Mod Kieran
1 -Barrows
2 -Magebank
3 -Giant Mole & cave lighting system
4 -Do ghostly robe miniquest

Mod Zoro
1 -Try and add all F2P NPCs & hits with them
2 -Work on some shops
3 -Jad
4 -Kalphite Queen