package com.runescape.network.packet;

import com.runescape.model.player.Client;

public interface PacketDecoder {
	
	public void processPacket(Client c, int packetType, int packetSize);
}
