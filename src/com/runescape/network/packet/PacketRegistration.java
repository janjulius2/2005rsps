package com.runescape.network.packet;

import com.runescape.model.player.Client;
import com.runescape.network.packet.impl.ActionButtons;
import com.runescape.network.packet.impl.AttackPlayer;
import com.runescape.network.packet.impl.BankAll;
import com.runescape.network.packet.impl.BankFive;
import com.runescape.network.packet.impl.BankSetAmountOne;
import com.runescape.network.packet.impl.BankSetAmountTwo;
import com.runescape.network.packet.impl.BankTen;
import com.runescape.network.packet.impl.ChallengePlayer;
import com.runescape.network.packet.impl.ChangeAppearance;
import com.runescape.network.packet.impl.ChangeRegions;
import com.runescape.network.packet.impl.ClickItem;
import com.runescape.network.packet.impl.ClickNPC;
import com.runescape.network.packet.impl.ClickObject;
import com.runescape.network.packet.impl.Commands;
import com.runescape.network.packet.impl.Communication;
import com.runescape.network.packet.impl.DefaultPacket;
import com.runescape.network.packet.impl.Dialogue;
import com.runescape.network.packet.impl.DropItem;
import com.runescape.network.packet.impl.EnterName;
import com.runescape.network.packet.impl.EquipItem;
import com.runescape.network.packet.impl.FollowPlayer;
import com.runescape.network.packet.impl.IdleLogout;
import com.runescape.network.packet.impl.InterfaceInteraction;
import com.runescape.network.packet.impl.ItemOnGroundItem;
import com.runescape.network.packet.impl.ItemOnItem;
import com.runescape.network.packet.impl.ItemOnNpc;
import com.runescape.network.packet.impl.ItemOnObject;
import com.runescape.network.packet.impl.ItemOptionThree;
import com.runescape.network.packet.impl.ItemOptionTwo;
import com.runescape.network.packet.impl.MagicOnFloorItems;
import com.runescape.network.packet.impl.MagicOnItems;
import com.runescape.network.packet.impl.MoveItems;
import com.runescape.network.packet.impl.Movement;
import com.runescape.network.packet.impl.PickupItem;
import com.runescape.network.packet.impl.PrivateMessaging;
import com.runescape.network.packet.impl.RemoveItem;
import com.runescape.network.packet.impl.Trading;

public class PacketRegistration {

	private static PacketDecoder packetId[] = new PacketDecoder[256];

	static {
		DefaultPacket u = new DefaultPacket();
		packetId[3] = u;
		packetId[202] = u;
		packetId[241] = u;
		packetId[77] = u;
		packetId[86] = u;
		packetId[78] = u;
		packetId[36] = u;
		packetId[226] = u;
		packetId[246] = u;
		packetId[148] = u;
		packetId[183] = u;
		packetId[230] = u;
		packetId[136] = u;
		packetId[189] = u;
		packetId[152] = u;
		packetId[200] = u;
		packetId[85] = u;
		packetId[165] = u;
		packetId[238] = u;
		packetId[150] = u;
		packetId[40] = new Dialogue();
		ClickObject co = new ClickObject();
		packetId[132] = co;
		packetId[252] = co;
		packetId[70] = co;
		packetId[57] = new ItemOnNpc();
		ClickNPC cn = new ClickNPC();
		packetId[72] = cn;
		packetId[131] = cn;
		packetId[155] = cn;
		packetId[17] = cn;
		packetId[21] = cn;
		packetId[16] = new ItemOptionTwo();
		packetId[75] = new ItemOptionThree();
		packetId[122] = new ClickItem();
		packetId[4] = new Communication();
		packetId[236] = new PickupItem();
		packetId[87] = new DropItem();
		packetId[185] = new ActionButtons();
		packetId[130] = new InterfaceInteraction();
		packetId[103] = new Commands();
		packetId[214] = new MoveItems();
		packetId[237] = new MagicOnItems();
		packetId[181] = new MagicOnFloorItems();
		packetId[202] = new IdleLogout();
		AttackPlayer ap = new AttackPlayer();
		packetId[73] = ap;
		packetId[249] = ap;
		packetId[128] = new ChallengePlayer();
		packetId[139] = new Trading();
		packetId[39] = new FollowPlayer();
		packetId[41] = new EquipItem();
		packetId[145] = new RemoveItem();
		packetId[117] = new BankFive();
		packetId[43] = new BankTen();
		packetId[129] = new BankAll();
		packetId[101] = new ChangeAppearance();
		PrivateMessaging pm = new PrivateMessaging();
		packetId[188] = pm;
		packetId[126] = pm;
		packetId[215] = pm;
		packetId[59] = pm;
		packetId[95] = pm;
		packetId[133] = pm;
		packetId[135] = new BankSetAmountOne();
		packetId[208] = new BankSetAmountTwo();
		Movement w = new Movement();
		packetId[98] = w;
		packetId[164] = w;
		packetId[248] = w;
		packetId[53] = new ItemOnItem();
		packetId[192] = new ItemOnObject();
		packetId[25] = new ItemOnGroundItem();
		ChangeRegions cr = new ChangeRegions();
		packetId[121] = cr;
		packetId[210] = cr;
		packetId[60] = new EnterName();
	}

	public static void processPacket(Client c, int packetType, int packetSize) {
        
		PacketDecoder p = packetId[packetType];
        
        if(p != null && packetType > 0 && packetType < 257 && packetType == c.packetType && packetSize == c.packetSize) {

            try {
            	
                p.processPacket(c, packetType, packetSize);
                
            } catch(Exception exception) {
            	
                exception.printStackTrace();
            }
        } else {

            System.out.println(c.playerName + "is sending invalid PacketType: " + packetType + ". PacketSize: " + packetSize);
        }
    }
}