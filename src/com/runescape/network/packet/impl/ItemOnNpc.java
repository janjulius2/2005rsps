package com.runescape.network.packet.impl;

import com.runescape.model.npc.NPCHandler;
import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;

public class ItemOnNpc implements PacketDecoder {

	@Override
	@SuppressWarnings("unused")
	public void processPacket(Client c, int packetType, int packetSize) {
		int itemId = c.getInStream().readSignedWordA();
		int i = c.getInStream().readSignedWordA();
		int slot = c.getInStream().readSignedWordBigEndian();
		int npcId = NPCHandler.npcs[i].npcType;
		 if(!c.getItems().playerHasItem(itemId, 1, slot)) {
	            return;
	        }
	}
}
