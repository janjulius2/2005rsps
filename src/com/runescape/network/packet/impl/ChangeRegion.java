package com.runescape.network.packet.impl;

import com.runescape.Application;
import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;
import com.runescape.world.doors.OpenDoor;

public class ChangeRegion implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		c.getPA().removeObjects();
		OpenDoor.loadRegional(c);
		Application.objectHandler.loadObjects(c);
	}
}
