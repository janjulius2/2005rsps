package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;
import com.runescape.world.utilities.Utilities;

public class EnterName implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		String textSent = Utilities.longToPlayerName2(c.getInStream().readQWord());
		textSent = textSent.replaceAll("_", " ");
	}
}
