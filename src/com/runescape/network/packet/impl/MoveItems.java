package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;

/**
 * Move Items
 **/
public class MoveItems implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int somejunk = c.getInStream().readUnsignedWordA(); // junk
		int itemFrom = c.getInStream().readUnsignedWordA();// slot1
		int itemTo = (c.getInStream().readUnsignedWordA() - 128);// slot2
		// c.sendMessage("junk: " + somejunk);
		if (c.inTrade) {
			return;
		}

		c.getItems().moveItems(itemFrom, itemTo, somejunk);
	}
}
