package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;
import com.runescape.world.utilities.Utilities;

public class ItemOnGroundItem implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		c.getInStream().readSignedWordBigEndian();
		int itemUsed = c.getInStream().readSignedWordA();
		int groundItem = c.getInStream().readUnsignedWord();
		c.getInStream().readSignedWordA();
		c.getInStream().readSignedWordBigEndianA();
		c.getInStream().readUnsignedWord();
        
		switch (itemUsed) {

		default:
			if (c.playerRights == 3)
				Utilities.println("ItemUsed " + itemUsed + " on Ground Item "
						+ groundItem);
			break;
		}
	}

}
