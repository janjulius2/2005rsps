package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;


public class Trading implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int tradeId = c.getInStream().readSignedWordBigEndian();
		c.resetSkillingActions();
		c.getPA().resetFollow();
		if (c.arenas()) {
			c.sendMessage("You can't trade inside the arena!");
			return;
		}
		if (tradeId != c.playerId) {
			c.getTradeAndDuel().requestTrade(tradeId);
		}
	}
}
