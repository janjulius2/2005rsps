package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;

public class ChangeAppearance implements PacketDecoder {
	
	@Override
	public void processPacket(Client player, final int packetType, final int packetSize) {
	    
		final int gender = player.getInStream().readSignedByte();
	    
		final int head = player.getInStream().readSignedByte();
	    
		final int jaw = player.getInStream().readSignedByte();
	    
		final int torso = player.getInStream().readSignedByte();
	    
		final int arms = player.getInStream().readSignedByte();
	    
		final int hands = player.getInStream().readSignedByte();
	    
		final int legs = player.getInStream().readSignedByte();
	    
		final int feet = player.getInStream().readSignedByte();
	    
		final int hairColour = player.getInStream().readSignedByte();
	    
		final int torsoColour = player.getInStream().readSignedByte();
	    
		final int legsColour = player.getInStream().readSignedByte();
	    
		final int feetColour = player.getInStream().readSignedByte();
	    
		final int skinColour = player.getInStream().readSignedByte();
	    
		if (player.canChangeAppearance) {
			
			player.playerAppearance[0] = gender; 
			
			player.playerAppearance[6] = feet; 
			
			player.playerAppearance[7] = jaw; 
			
			player.playerAppearance[8] = hairColour; 
			
			player.playerAppearance[9] = torsoColour; 
			
			player.playerAppearance[10] = legsColour; 
			
			player.playerAppearance[11] = feetColour; 
			
			player.playerAppearance[12] = skinColour;
			
			if (head < 0)
				player.playerAppearance[1] = head + 256;
			else
				player.playerAppearance[1] = head;
			
			if (torso < 0)
				player.playerAppearance[2] = torso + 256;
			else
				player.playerAppearance[2] = torso;
			
			if (arms < 0)
				player.playerAppearance[3] = arms + 256;
			else
				player.playerAppearance[3] = arms;
			
			if (hands < 0)
				player.playerAppearance[4] = hands + 256;
			else
				player.playerAppearance[4] = hands;
			
			if (legs < 0)
				player.playerAppearance[5] = legs + 256;
			else
				player.playerAppearance[5] = legs;

			player.getPA().removeAllWindows();
			
			player.getPA().requestUpdates();
			
			player.canChangeAppearance = true;
		}
	}
}