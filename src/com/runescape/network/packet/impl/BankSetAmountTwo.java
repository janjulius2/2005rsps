package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;

public class BankSetAmountTwo implements PacketDecoder {
	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int Xamount = c.getInStream().readDWord();
		if (Xamount < 0)
		{
			Xamount = c.getItems().getItemAmount(c.xRemoveId);
		}
		if (Xamount == 0) {
			Xamount = 1;
		}

		switch (c.xInterfaceId) {
		case 5064:
			if(!c.getItems().playerHasItem(c.xRemoveId, Xamount)) {
				return;
			}
			c.getItems().bankItem(c.playerItems[c.xRemoveSlot], c.xRemoveSlot,
					Xamount);
			break;

		case 5382:
			c.getItems().fromBank(c.bankItems[c.xRemoveSlot], c.xRemoveSlot,
					Xamount);
			break;

		case 3322:
			if(!c.getItems().playerHasItem(c.xRemoveId, Xamount)) {
				return;
			}
			if (c.duelStatus <= 0) {
				c.getTradeAndDuel().tradeItem(c.xRemoveId, c.xRemoveSlot,
						Xamount);
			} else {
				c.getTradeAndDuel().stakeItem(c.xRemoveId, c.xRemoveSlot,
						Xamount);
			}
			break;

		case 3415:
			if (c.duelStatus <= 0) {
				c.getTradeAndDuel().fromTrade(c.xRemoveId, c.xRemoveSlot,
						Xamount);
			}
			break;

		case 6669:
			c.getTradeAndDuel().fromDuel(c.xRemoveId, c.xRemoveSlot, Xamount);
			break;
		}
	}
}