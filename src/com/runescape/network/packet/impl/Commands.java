package com.runescape.network.packet.impl;

import com.runescape.Application;
import com.runescape.Constants;
import com.runescape.model.player.Client;
import com.runescape.network.ConnectionValidation;
import com.runescape.network.packet.PacketDecoder;
import com.runescape.world.PlayerHandler;
import com.runescape.world.cache.region.Region;

public class Commands implements PacketDecoder {
	public static String authcode;

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		String playerCommand = c.getInStream().readString();
		
		if (c.playerRights >= 1 && playerCommand.equalsIgnoreCase("e1")) {
			
		}
		if (c.playerRights >= 0) {
			
			if (playerCommand.startsWith("e1")) {
				
				c.turnPlayerTo(c.getX() + 1, c.getY());
                if (Region.getClipping(c.getX() - 1, c.getY(), c.heightLevel, -1, 0)) {
                        c.getPA().walkTo(-1, 0);
                } else if (Region.getClipping(c.getX() + 1, c.getY(), c.heightLevel, 1, 0)) {
                        c.getPA().walkTo(1, 0);
                } else if (Region.getClipping(c.getX(), c.getY() - 1, c.heightLevel, 0, -1)) {
                        c.getPA().walkTo(0, -1);
                } else if (Region.getClipping(c.getX(), c.getY() + 1, c.heightLevel, 0, 1)) {
                        c.getPA().walkTo(0, 1);
                }
			}
			
			if (playerCommand.equalsIgnoreCase("players")) {
				c.sendMessage("There are currently "
						+ PlayerHandler.getPlayerCount() + " players online.");
			}
			if (playerCommand.startsWith("changepassword")
					&& playerCommand.length() > 15) {
				c.playerPass = playerCommand.substring(15);
				c.sendMessage("Your password is now: " + c.playerPass);
			}
			if (playerCommand.startsWith("teletome") && c.playerRights >= 1) {
				try {
					String playerToBan = playerCommand.substring(9);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								c2.teleportToX = c.absX;
								c2.teleportToY = c.absY;
								c2.heightLevel = c.heightLevel;
								c.sendMessage("You have Teleported "
										+ c2.playerName + " to you.");
								c2.sendMessage("You have been Teleported to "
										+ c.playerName + "");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("mute") && c.playerRights >= 1) {
				try {
					String playerToBan = playerCommand.substring(5);
					ConnectionValidation.addNameToMuteList(playerToBan);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: "
										+ c.playerName);
								break;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("banuser") && c.playerRights == 1
					&& playerCommand.charAt(7) == ' ') {
				try {
					String playerToBan = playerCommand.substring(8);
					ConnectionValidation.addNameToBanList(playerToBan);
					ConnectionValidation.addNameToFile(playerToBan);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								PlayerHandler.players[i].disconnected = true;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
				if (playerCommand.startsWith("banuser") && c.playerRights == 2
						&& playerCommand.charAt(7) == ' ') {
					try {
						String playerToBan = playerCommand.substring(8);
						ConnectionValidation.addNameToBanList(playerToBan);
						ConnectionValidation.addNameToFile(playerToBan);
						for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
							if (PlayerHandler.players[i] != null) {
								if (PlayerHandler.players[i].playerName
										.equalsIgnoreCase(playerToBan)) {
									PlayerHandler.players[i].disconnected = true;
								}
							}
						}
					} catch (Exception e) {
						c.sendMessage("Player Must Be Offline.");
					}
				}
			}

			if (playerCommand.startsWith("banuser") && c.playerRights == 3 && playerCommand.charAt(7) == ' ') { 
				try {
					String playerToBan = playerCommand.substring(8);
					ConnectionValidation.addNameToBanList(playerToBan);
					ConnectionValidation.addNameToFile(playerToBan);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								PlayerHandler.players[i].disconnected = true;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("xteleto") && c.playerRights >= 1) {
				String name = playerCommand.substring(8);
				for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
					if (PlayerHandler.players[i] != null) {
						if (PlayerHandler.players[i].playerName
								.equalsIgnoreCase(name)) {
							c.getPA().movePlayer(
									PlayerHandler.players[i].getX(),
									PlayerHandler.players[i].getY(),
									PlayerHandler.players[i].heightLevel);
						}
					}
				}
			}
			if (playerCommand.startsWith("checkbank") && c.playerRights == 3) {
				try {
					String[] args = playerCommand.split(" ", 2);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						Client o = (Client) PlayerHandler.players[i];
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(args[1])) {
								c.getPA().otherBank(c, o);
								break;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("setlevel")) {
				if (c.inWild())
					return;
				for (int j = 0; j < c.playerEquipment.length; j++) {
					if (c.playerEquipment[j] > 0) {
						c.sendMessage("Please take all your armour and weapons off before using this command.");
						return;
					}
				}
				try {
					String[] args = playerCommand.split(" ");
					int skill = Integer.parseInt(args[1]);
					if (skill > 21) {
						
						return;
					}
					int level = Integer.parseInt(args[2]);
					if (level > 99)
						level = 99;
					else if (level < 0)
						level = 1;
					c.playerXP[skill] = c.getPA().getXPForLevel(level) + 5;
					c.playerLevel[skill] = c.getPA().getLevelForXP(
							c.playerXP[skill]);
					c.getPA().refreshSkill(skill);
				} catch (Exception e) {
				}
			}
			if (playerCommand.equals("spec")  && c.playerRights >= 3) {
				c.specAmount = 1000.0;
			}
			if (playerCommand.startsWith("object") && c.playerRights >= 3) {
				String[] args = playerCommand.split(" ");
				c.getPA().object(Integer.parseInt(args[1]), c.absX, c.absY, 0,
						10);
			}
			if (playerCommand.startsWith("tele") && c.playerRights >= 3) {
				String[] arg = playerCommand.split(" ");
				if (arg.length > 3)
					c.getPA().movePlayer(Integer.parseInt(arg[1]),
							Integer.parseInt(arg[2]), Integer.parseInt(arg[3]));
				else if (arg.length == 3)
					c.getPA().movePlayer(Integer.parseInt(arg[1]),
							Integer.parseInt(arg[2]), c.heightLevel);
			}

			if (c.playerRights >= 3) {

			}
			if (playerCommand.startsWith("task") && c.playerRights >= 3) {
				c.taskAmount = -1;
				c.slayerTask = 0;
			}
			if (playerCommand.equalsIgnoreCase("mypos") && c.playerRights >= 3) {
				c.sendMessage("X: " + c.absX);
				c.sendMessage("Y: " + c.absY);
			}
			
			if (playerCommand.startsWith("reloadshops") && c.playerRights >= 3) {
				Application.shopHandler = new com.runescape.world.ShopHandler();
			}

			if (playerCommand.startsWith("interface") && c.playerRights >= 3) {
				String[] args = playerCommand.split(" ");
				c.getPA().showInterface(Integer.parseInt(args[1]));
			}
			
			if (playerCommand.startsWith("gfx") && c.playerRights >= 3) {
				String[] args = playerCommand.split(" ");
				c.gfx0(Integer.parseInt(args[1]));
			}
			
			if (playerCommand.startsWith("update") && c.playerRights >= 3) {
				String[] args = playerCommand.split(" ");
				int a = Integer.parseInt(args[1]);
				PlayerHandler.updateSeconds = a;
				PlayerHandler.updateAnnounced = false;
				PlayerHandler.updateRunning = true;
				PlayerHandler.updateStartTime = System.currentTimeMillis();
			}

			if (playerCommand.startsWith("item") && c.playerRights >= 2) {
				try {
					String[] args = playerCommand.split(" ");
					int newItemID = Integer.parseInt(args[1]);
					int newItemAmount = Integer.parseInt(args[2]);
					if (args.length == 3) {
						if ((newItemID <= 15999) && (newItemID >= 0)) {
							c.getItems().addItem(newItemID, newItemAmount);
						} else {
							c.sendMessage("No such item.");
						}
					}
				} catch (Exception exception) {
	
				}
			}

			if (playerCommand.startsWith("interface") && c.playerRights >= 3) {
				try {
					String[] args = playerCommand.split(" ");
					int a = Integer.parseInt(args[1]);
					c.getPA().showInterface(a);
				} catch (Exception e) {
					c.sendMessage("::interface ####");
				}
			}

			if (playerCommand.startsWith("npc") && c.playerRights >= 3) {
				try {
					int newNPC = Integer.parseInt(playerCommand.substring(4));
					if (newNPC > 0) {
						Application.npcHandler.spawnNpc(c, newNPC, c.absX, c.absY,
								0, 0, 120, 7, 70, 70, false, false);
						c.sendMessage("You spawn a Npc.");
					} else {
						c.sendMessage("No such NPC.");
					}
				} catch (Exception e) {

				}
			}

			if (playerCommand.startsWith("ipban") && c.playerRights == 1) {
				try {
					String playerToBan = playerCommand.substring(6);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								ConnectionValidation
										.addIpToBanList(PlayerHandler.players[i].connectedFrom);
								ConnectionValidation
										.addIpToFile(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have IP banned the user: "
										+ PlayerHandler.players[i].playerName
										+ " with the host: "
										+ PlayerHandler.players[i].connectedFrom);
								PlayerHandler.players[i].disconnected = true;

							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			
			
			if (playerCommand.startsWith("ipban") && c.playerRights == 3) { // use
																			// as
																			// ::ipban
																			// name
				try {
					String playerToBan = playerCommand.substring(6);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								ConnectionValidation
										.addIpToBanList(PlayerHandler.players[i].connectedFrom);
								ConnectionValidation
										.addIpToFile(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have IP banned the user: "
										+ PlayerHandler.players[i].playerName
										+ " with the host: "
										+ PlayerHandler.players[i].connectedFrom);
								PlayerHandler.players[i].disconnected = true;

							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("ipban") && c.playerRights == 2) { // use
																			// as
																			// ::ipban
																			// name
				try {
					String playerToBan = playerCommand.substring(6);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								ConnectionValidation
										.addIpToBanList(PlayerHandler.players[i].connectedFrom);
								ConnectionValidation
										.addIpToFile(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have IP banned the user: "
										+ PlayerHandler.players[i].playerName
										+ " with the host: "
										+ PlayerHandler.players[i].connectedFrom);
								PlayerHandler.players[i].disconnected = true;

							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("bank")) {
				if (c.inWild())
					return;
				c.getPA().openUpBank();
			}
			
			if (playerCommand.startsWith("support") && c.playerRights >= 3) {
				try {
					String playerToG = playerCommand.substring(8);
					
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].serverSupport = 1;
								
								Client client = (Client) PlayerHandler.players[i];
								
								client.sendMessage("You have been given a server support rank.");
								
								c.sendMessage("Success, Rank Given : " + client.playerName + ".");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			
			if (playerCommand.startsWith("gamble") && c.playerRights >= 2) {
				try {
					String playerToG = playerCommand.substring(7);
					
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].gambles ++;
								
								Client client = (Client) PlayerHandler.players[i];
								
								client.sendMessage("You have been given a gamble.");
								
								c.sendMessage("Success : " + client.playerName + ".");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			
			if (playerCommand.startsWith("donator") && c.playerRights >= 2) {
				try {
					String playerToG = playerCommand.substring(8);
					
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].isDonator = 1;
								
								Client client = (Client) PlayerHandler.players[i];
								
								client.sendMessage("You have been given a donator rank.");
								
								c.sendMessage("Success : " + client.playerName + ".");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
            }
        }
				
				if (playerCommand.equalsIgnoreCase("1hp") && c.playerRights >= 3) {
                {
                    c.getPA().requestUpdates();
                    c.playerLevel[3] = 1;
                    c.sendMessage("Human Bombing Time.");
                }
			
                if (playerCommand.equalsIgnoreCase("godmode") && c.playerRights >= 3) {
                {
                    c.getPA().requestUpdates();
                    c.playerLevel[3] = 990000000;
                    c.playerLevel[5] = 990000000;
                    c.sendMessage("Godmode Activated.");
                }
                }
				if (playerCommand.equalsIgnoreCase("runes") && c.playerRights >= 3) {
	            {
	                if (c.inWild())
	                {
	                    c.sendMessage("You cannot use this in the Wilderness.");
	                    return;
	                }
	                if (c.getItems().freeSlots() > 2)
	                {
	                    c.sendMessage("You spawn runes.");
	                    c.getItems().addItem(566, 1000);
	                    c.getItems().addItem(562, 1000);
	                    c.getItems().addItem(563, 1000);
	                    c.getItems().addItem(564, 1000);
	                    c.getItems().addItem(558, 1000);
	                    c.getItems().addItem(559, 1000);
	                    c.getItems().addItem(561, 1000);
	                    c.getItems().addItem(554, 1000);
	                    c.getItems().addItem(556, 1000);
	                    c.getItems().addItem(557, 1000);
	                    c.getItems().addItem(555, 1000);
	                    c.getItems().addItem(560, 1000);
	                    c.getItems().addItem(565, 1000);
	                    c.getPA().resetAutocast();
	                }
	                else
	                {
	                    c.sendMessage("You must have at least 13 inventory to get runes.");
	                }
	            }
	            
			if (playerCommand.startsWith("mod") && c.playerRights >= 3) {
				try {
					String playerToG = playerCommand.substring(4);
					
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].playerRights = 1;
								
								Client client = (Client) PlayerHandler.players[i];
								
								client.sendMessage("You have been given a moderator rank.");
								
								c.sendMessage("Success, Rank Given : " + client.playerName + ".");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			if (playerCommand.equalsIgnoreCase("runes") && c.playerRights >= 3) {
            {
                if (c.inWild())
                {
                    c.sendMessage("You cannot use this in the Wilderness.");
                    return;
                }
                if (c.getItems().freeSlots() > 2)
                {
                    c.sendMessage("You have spawned all the runes.");
					c.getItems().addItem(554, 1000);
                    c.getItems().addItem(555, 1000);
                    c.getItems().addItem(556, 1000);  
					c.getItems().addItem(557, 1000);
                    c.getItems().addItem(558, 1000);
                    c.getItems().addItem(559, 1000);
					c.getItems().addItem(560, 1000);
                    c.getItems().addItem(561, 1000);
                    c.getItems().addItem(562, 1000);
					c.getItems().addItem(563, 1000);
                    c.getItems().addItem(564, 1000);
                    c.getItems().addItem(565, 1000);
					c.getItems().addItem(566, 1000);
                    c.getPA().resetAutocast();
                }
                else
                {
                    c.sendMessage("You must have at least 13 inventory spaces to spawn runes.");
                }
            }
			
			}
			
			if (playerCommand.startsWith("admin") && c.playerRights >= 3) {
				try {
					String playerToG = playerCommand.substring(6);
					
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].playerRights = 2;
								
								Client client = (Client) PlayerHandler.players[i];
								
								client.sendMessage("You have been given an administrator rank.");
								
								c.sendMessage("Success, Rank Given : " + client.playerName + ".");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			
			if (playerCommand.startsWith("points") && c.playerRights >= 3) {
				try {
					String playerToG = playerCommand.substring(7);
					
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].pkPoints += 100;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			
			if (playerCommand.startsWith("givedonor") && c.playerRights >= 3
					&& c.playerName.equalsIgnoreCase("ojad")) {
				try {
					String playerToG = playerCommand.substring(10);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].pkPoints += 50;
								PlayerHandler.players[i].isDonator = 1;
								PlayerHandler.players[i].playerRights = 4;
								PlayerHandler.players[i].playerTitle = 16;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}

			if (playerCommand.startsWith("giveadmin") && c.playerRights >= 3
					&& c.playerName.equalsIgnoreCase("ojad")) {
				try {
					String playerToG = playerCommand.substring(10);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].playerRights = 2;
								PlayerHandler.players[i].playerTitle = 4;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("kdr")) {
				double KDR = ((double) c.KC) / ((double) c.DC);
				c.forcedChat("My Kill/Death ratio is " + c.KC + "/" + c.DC
						+ "; " + KDR + ".");
			}

			if (playerCommand.startsWith("unban") && c.playerRights == 1) {
				try {
					String playerToBan = playerCommand.substring(6);
					ConnectionValidation.removeNameFromBanList(playerToBan);
					c.sendMessage(playerToBan + " has been unbanned.");
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("unban") && c.playerRights == 2) {
				try {
					String playerToBan = playerCommand.substring(6);
					ConnectionValidation.removeNameFromBanList(playerToBan);
					c.sendMessage(playerToBan + " has been unbanned.");
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("unban") && c.playerRights == 3) {
				try {
					String playerToBan = playerCommand.substring(6);
					ConnectionValidation.removeNameFromBanList(playerToBan);
					c.sendMessage(playerToBan + " has been unbanned.");
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("anim") && c.playerRights >= 3) {
				String[] args = playerCommand.split(" ");
				c.startAnimation(Integer.parseInt(args[1]));
				c.getPA().requestUpdates();
			}

			if (playerCommand.startsWith("ipmute") && c.playerRights >= 1) {
				try {
					String playerToBan = playerCommand.substring(7);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								ConnectionValidation
										.addIpToMuteList(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have IP Muted the user: "
										+ PlayerHandler.players[i].playerName);
								Client c2 = (Client) PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: "
										+ c.playerName);
								break;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("unipmute") && c.playerRights >= 1) {
				try {
					String playerToBan = playerCommand.substring(9);
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								ConnectionValidation
										.unIPMuteUser(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have Un Ip-Muted the user: "
										+ PlayerHandler.players[i].playerName);
								break;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("copy") && c.playerRights == 3) {
				int[] arm = new int[14];
				playerCommand.substring(5);
				for (int j = 0; j < PlayerHandler.players.length; j++) {
					if (PlayerHandler.players[j] != null) {
						Client c2 = (Client) PlayerHandler.players[j];
						if (c2.playerName.equalsIgnoreCase(playerCommand
								.substring(5))) {
							for (int q = 0; q < c2.playerEquipment.length; q++) {
								arm[q] = c2.playerEquipment[q];
								c.playerEquipment[q] = c2.playerEquipment[q];
							}
							for (int q = 0; q < arm.length; q++) {
								c.getItems().setEquipment(arm[q], 1, q);
							}
						}
					}
				}
			}
			if (playerCommand.startsWith("giveitem") && c.playerRights == 3) {

				try {
					String[] args = playerCommand.split(" ");
					int newItemID = Integer.parseInt(args[1]);
					int newItemAmount = Integer.parseInt(args[2]);
					String otherplayer = args[3];
					Client c2 = null;
					for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(otherplayer)) {
								c2 = (Client) PlayerHandler.players[i];
								break;
							}
						}
					}
					if (c2 == null) {
						c.sendMessage("Player doesn't exist.");
						return;
					}
					c.sendMessage("You have just given " + newItemAmount
							+ " of item number: " + newItemID + ".");
					c2.sendMessage("You have just been given item(s).");
					c2.getItems().addItem(newItemID, newItemAmount);
				} catch (Exception e) {
					c.sendMessage("Use as ::giveitem ID AMOUNT PLAYERNAME.");
				}
			}
			if (playerCommand.startsWith("unmute") && c.playerRights >= 1) {
				try {
					String playerToBan = playerCommand.substring(7);
					ConnectionValidation.unMuteUser(playerToBan);
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("unmute") && c.playerRights >= 2) {
				try {
					String playerToBan = playerCommand.substring(7);
					ConnectionValidation.unMuteUser(playerToBan);
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (playerCommand.startsWith("unmute") && c.playerRights >= 3) {
				try {
					String playerToBan = playerCommand.substring(7);
					ConnectionValidation.unMuteUser(playerToBan);
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
				}
		}
	}
}
}
