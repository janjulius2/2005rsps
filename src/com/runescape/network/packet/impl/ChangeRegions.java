package com.runescape.network.packet.impl;

import com.runescape.Application;
import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;
import com.runescape.world.doors.OpenDoor;

public class ChangeRegions implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		Application.itemHandler.reloadItems(c);
		Application.objectHandler.loadObjects(c);
		OpenDoor.loadRegional(c);
		c.saveFile = true;
		if (c.skullTimer > 0) {
			c.isSkulled = true;
			c.headIconPk = 0;
			c.getPA().requestUpdates();
		}
	}
}
