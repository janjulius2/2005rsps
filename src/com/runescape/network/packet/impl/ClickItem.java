package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.model.player.content.skill.Prayer;
import com.runescape.network.packet.PacketDecoder;

public class ClickItem implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		@SuppressWarnings("unused")
		int widget = c.getInStream().readSignedWordBigEndianA();
		int itemSlot = c.getInStream().readUnsignedWordA();
		int itemId = c.getInStream().readUnsignedWordBigEndian();
		if (itemId != c.playerItems[itemSlot] - 1) {
			return;
		}

		if (itemId >= 5509 && itemId <= 5514) {
			int pouch = -1;
			int a = itemId;
			if (a == 5509)
				pouch = 0;
			if (a == 5510)
				pouch = 1;
			if (a == 5512)
				pouch = 2;
			if (a == 5514)
				pouch = 3;
			c.getPA().fillPouch(pouch);
			return;
		}
		if (Prayer.buryBone(itemId, itemSlot, c)) {
			
		}
		if (c.getFood().isFood(itemId))
			c.getFood().eat(itemId, itemSlot);
		if (c.getPotions().isPotion(itemId))
			c.getPotions().handlePotion(itemId, itemSlot);
		if (itemId == 8007) {
			c.getItems().deleteItem(8007, c.getItems().getItemSlot(8007), 1);
			c.startAnimation(4731);
			c.gfx0(678);
			c.getPA().Tab(3210, 3424, 0);
			c.tab = 4;
		}
		if (itemId == 8008) {
			c.getItems().deleteItem(8008, c.getItems().getItemSlot(8008), 1);
			c.startAnimation(4731);
			c.gfx0(678);
			c.getPA().Tab(3222, 3218, 0);
			c.tab = 4;
		}
		if (itemId == 8009) {
			c.getItems().deleteItem(8009, c.getItems().getItemSlot(8009), 1);
			c.startAnimation(4731);
			c.gfx0(678);
			c.getPA().Tab(2964, 3378, 0);
			c.tab = 4;
		}
		if (itemId == 8010) {
			c.getItems().deleteItem(8010, c.getItems().getItemSlot(8010), 1);
			c.startAnimation(4731);
			c.gfx0(678);
			c.getPA().Tab(2757, 3477, 0);
			c.tab = 4;
		}
		if (itemId == 8011) {
			c.getItems().deleteItem(8011, c.getItems().getItemSlot(8011), 1);
			c.startAnimation(4731);
			c.gfx0(678);
			c.getPA().Tab(2662, 3305, 0);
			c.tab = 4;
		}
		if (itemId == 8013) {
			c.getItems().deleteItem(8013, c.getItems().getItemSlot(8013), 1);
			c.startAnimation(4731);
			c.gfx0(678);
			c.getPA().Tab(3087, 3502, 0);
			c.tab = 4;
		}
		if (itemId == 621) {
			c.getItems().deleteItem(621, c.getItems().getItemSlot(621), 1);
			c.pkPoints++;
		}
		if (itemId == 952) {
			if (c.inArea(3553, 3301, 3561, 3294)) {
				c.teleTimer = 3;
				c.newLocation = 1;
			} else if (c.inArea(3550, 3287, 3557, 3278)) {
				c.teleTimer = 3;
				c.newLocation = 2;
			} else if (c.inArea(3561, 3292, 3568, 3285)) {
				c.teleTimer = 3;
				c.newLocation = 3;
			} else if (c.inArea(3570, 3302, 3579, 3293)) {
				c.teleTimer = 3;
				c.newLocation = 4;
			} else if (c.inArea(3571, 3285, 3582, 3278)) {
				c.teleTimer = 3;
				c.newLocation = 5;
			} else if (c.inArea(3562, 3279, 3569, 3273)) {
				c.teleTimer = 3;
				c.newLocation = 6;
			}
		}
	}
}