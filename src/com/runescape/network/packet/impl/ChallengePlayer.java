package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;
import com.runescape.world.PlayerHandler;

/**
 * Challenge Player
 **/
public class ChallengePlayer implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		switch (packetType) {
		case 128:
			int answerPlayer = c.getInStream().readUnsignedWord();
			if (PlayerHandler.players[answerPlayer] == null) {
				return;
			}

			if (c.arenas() || c.duelStatus == 5) {
				c.sendMessage("You can't challenge inside the arena!");
				return;
			}
			c.sendMessage("");

			c.getTradeAndDuel().requestDuel(answerPlayer);
			break;
		}
	}
}
