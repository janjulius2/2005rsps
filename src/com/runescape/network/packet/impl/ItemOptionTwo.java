package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;
import com.runescape.world.utilities.Utilities;

/**
 * Item Click 2 Or Alternative Item Option 1
 * 
 * @author Ryan / Lmctruck30
 * 
 *         Proper Streams
 */

public class ItemOptionTwo implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int itemId = c.getInStream().readSignedWordA();

		if (!c.getItems().playerHasItem(itemId, 1))
			return;

		switch (itemId) {
		case 11694:

			c.sendMessage("Dismantling has been disabled due to duping");
			break;
		case 11696:
			c.sendMessage("Dismantling has been disabled due to duping");
			break;
		case 11698:
			c.sendMessage("Dismantling has been disabled due to duping");
			break;
		case 11700:
			c.sendMessage("Dismantling has been disabled due to duping");
			break;
		default:
			if (c.playerRights == 3)
				Utilities.println(c.playerName + " - Item3rdOption: " + itemId);
			break;
		}

	}

}
