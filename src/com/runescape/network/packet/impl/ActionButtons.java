package com.runescape.network.packet.impl;

import com.runescape.Constants;
import com.runescape.model.item.GameItem;
import com.runescape.model.player.Client;
import com.runescape.model.player.content.TanHide;
import com.runescape.model.player.content.TanHide.TanningData;
import com.runescape.network.packet.PacketDecoder;
import com.runescape.world.PlayerHandler;
import com.runescape.world.utilities.Utilities;

public class ActionButtons implements PacketDecoder {

	@Override
	@SuppressWarnings({ "null" })
	public void processPacket(final Client c, int packetType, int packetSize) {
		int actionButtonId = Utilities.hexToInt(c.getInStream().buffer, 0, packetSize);
		if (c.isDead) {
			return;
		}
		if (c.playerRights == 3 && Constants.SERVER_DEBUG) {
			Utilities.println("Widget Sub: [" + actionButtonId + "].");
		}
		if (c.isAutoButton(actionButtonId)) {
			c.assignAutocast(actionButtonId);
		}
		for (TanningData t : TanningData.values()) {
			if (actionButtonId == t.getButtonId(actionButtonId)) {
				TanHide.tanHide(c, actionButtonId);
			}
		}
		switch (actionButtonId) {

			
		case 25120:
			if(c.inDuelArena()) {	
				if(c.duelStatus == 5) {
					break;
				}
				Client o1 = (Client) PlayerHandler.players[c.duelingWith];
				if(o1 == null) {
					c.getTradeAndDuel().declineDuel();
					return;
				}

				c.duelStatus = 4;
				if(o1.duelStatus == 4 && c.duelStatus == 4) {				
					c.getTradeAndDuel().startDuel();
					o1.getTradeAndDuel().startDuel();
					o1.duelCount = 4;
					c.duelCount = 4;
					c.duelDelay = System.currentTimeMillis();
					o1.duelDelay = System.currentTimeMillis();
				} else {
					c.getPA().sendFrame126("Waiting for other player...", 6571);
					o1.getPA().sendFrame126("Other player has accepted", 6571);
				}
			} else {
					Client o = (Client) PlayerHandler.players[c.duelingWith];
					c.getTradeAndDuel().declineDuel();
					o.getTradeAndDuel().declineDuel();
					c.sendMessage("You can't stake out of Duel Arena.");
			}
				break;
				
		case 26018:	
			if(c.inDuelArena()) {
				Client o = (Client) PlayerHandler.players[c.duelingWith];
				if(o == null) {
					c.getTradeAndDuel().declineDuel();
					o.getTradeAndDuel().declineDuel();
					return;
				}
					
				if (c.duelStatus == 5) {
					return;
				}
				
				if(c.duelRule[2] && c.duelRule[3] && c.duelRule[4]) {
					c.sendMessage("You won't be able to attack the player with the rules you have set.");
					break;
				}
				c.duelStatus = 2;
				if(c.duelStatus == 2) {
					c.getPA().sendFrame126("Waiting for other player...", 6684);
					o.getPA().sendFrame126("Other player has accepted.", 6684);
				}
				if(o.duelStatus == 2) {
					o.getPA().sendFrame126("Waiting for other player...", 6684);
					c.getPA().sendFrame126("Other player has accepted.", 6684);
				}
				
				if(c.duelStatus == 2 && o.duelStatus == 2) {
					c.canOffer = false;
					o.canOffer = false;
					c.duelStatus = 3;
					o.duelStatus = 3;
					c.getTradeAndDuel().confirmDuel();
					o.getTradeAndDuel().confirmDuel();
				}
			} else {
					Client o = (Client) PlayerHandler.players[c.duelingWith];
					c.getTradeAndDuel().declineDuel();
					o.getTradeAndDuel().declineDuel();
					c.sendMessage("You can't stake out of Duel Arena.");
			}
				break;
				
		case 29038:
			if (System.currentTimeMillis() - c.graniteMaulDelay < 500) {
				
				return;
			}
			if (c.playerEquipment[c.playerWeapon] == 4153) {
				c.specBarId = 7486;
				c.getCombat().handleGmaul();
				c.graniteMaulDelay = System.currentTimeMillis();
			}
			else {
				c.specBarId = 7486;
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
			}
			break;
			
		case 113090:
			c.setSidebarInterface(2, 638);
			break;
			
		case 109138:
			c.setSidebarInterface(2, 639);
			break;

		case 151:
			
			c.autoRet = 0;
			break;
			
		case 150:
			
			c.autoRet = 1;
			break;

		case 9190:
			if (c.teleAction == 1) {
				c.getPA().spellTeleport(2676, 3715, 0);
			} else if (c.teleAction == 2) {
				c.getPA().spellTeleport(3565, 3314, 0);
			} else if (c.teleAction == 3) {
				c.getPA().spellTeleport(2916, 3612, 0);
			} else if (c.teleAction == 4) {
				c.getPA().spellTeleport(2539, 4716, 0);
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(3046, 9779, 0);
			} else if (c.teleAction == 20) {
				c.getPA().spellTeleport(3222, 3218, 0);
			}
			if (c.dialogueAction == 10) {
				c.getPA().spellTeleport(2845, 4832, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 11) {
				c.getPA().spellTeleport(2786, 4839, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 12) {
				c.getPA().spellTeleport(2398, 4841, 0);
				c.dialogueAction = -1;
			}
			break;

		case 9191:
			if (c.teleAction == 1) {
				c.getPA().spellTeleport(2884, 9798, 0);
			} else if (c.teleAction == 2) {
				c.getPA().spellTeleport(2662, 2650, 0);
			} else if (c.teleAction == 3) {
				c.getPA().spellTeleport(3007, 3849, 0);
			} else if (c.teleAction == 4) {
				c.getPA().spellTeleport(2978, 3616, 0);
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(3079, 9502, 0);

			} else if (c.teleAction == 20) {
				c.getPA().spellTeleport(3210, 3424, 0);
			}
			if (c.dialogueAction == 10) {
				c.getPA().spellTeleport(2796, 4818, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 11) {
				c.getPA().spellTeleport(2527, 4833, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 12) {
				c.getPA().spellTeleport(2464, 4834, 0);
				c.dialogueAction = -1;
			}
			break;

		case 9192:
			if (c.teleAction == 1) {
				c.getPA().spellTeleport(3428, 3537, 0);
			} else if (c.teleAction == 2) {
				c.getPA().spellTeleport(2438, 5168, 0);
				c.sendMessage("To fight Jad, enter the cave.");
			} else if (c.teleAction == 3) {
				c.getPA().spellTeleport(1910, 4367, 0);
				c.sendMessage("Climb down the ladder to get into the lair.");
			} else if (c.teleAction == 4) {
				c.getPA().spellTeleport(3351, 3659, 0);

			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(2813, 3436, 0);
			} else if (c.teleAction == 20) {
				c.getPA().spellTeleport(2757, 3477, 0);
			}
			if (c.dialogueAction == 10) {
				c.getPA().spellTeleport(2713, 4836, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 11) {
				c.getPA().spellTeleport(2162, 4833, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 12) {
				c.getPA().spellTeleport(2207, 4836, 0);
				c.dialogueAction = -1;
			}
			break;

		case 9193:
			if (c.teleAction == 1) {
				c.getPA().spellTeleport(2710, 9466, 0);
			} else if (c.teleAction == 2) {
				c.getPA().spellTeleport(3366, 3266, 0);
			} else if (c.teleAction == 3) {
				c.getPA().spellTeleport(3295, 3921, 0);
			} else if (c.teleAction == 4) {
				c.getPA().spellTeleport(2969, 3914, 0);
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(2724, 3484, 0);
				c.sendMessage("For magic logs, try north of the duel arena.");
			}
			if (c.dialogueAction == 10) {
				c.getPA().spellTeleport(2660, 4839, 0);
				c.dialogueAction = -1;
			} else if (c.teleAction == 20) {
				c.getPA().spellTeleport(2964, 3378, 0);
			}
			break;

		case 9194:
			if (c.teleAction == 1) {
				c.getPA().spellTeleport(3297, 9824, 0);
			} else if (c.teleAction == 2) {
				c.sendMessage("Suggest something for this spot on the forums!");
				c.getPA().closeAllWindows();
			} else if (c.teleAction == 3) {
				c.sendMessage("Suggest something for this spot on the forums!");
				c.getPA().closeAllWindows();
			} else if (c.teleAction == 4) {
				c.getPA().spellTeleport(2964, 3378, 0);
			} else if (c.teleAction == 5) {
				c.getPA().spellTeleport(2812, 3463, 0);
			}
			if (c.dialogueAction == 10 || c.dialogueAction == 11) {
				c.dialogueId++;
				c.getDH().sendDialogues(c.dialogueId, 0);
			} else if (c.dialogueAction == 12) {
				c.dialogueId = 17;
				c.getDH().sendDialogues(c.dialogueId, 0);

			} else if (c.teleAction == 20) {
				c.getPA().spellTeleport(3506, 3496, 0);
			}
			break;

		case 58253:
			c.getItems().writeBonus();
			break;

		case 59004:
			c.getPA().removeAllWindows();
			break;


		case 9169:

			if (c.dialogueAction == 56) {
				c.getPA().startTeleport(2657, 2649, 0, "modern");
			} else if (c.dialogueAction == 57) {

				c.getPA().startTeleport(2540, 4715, 0, "modern");
			}
			else if (c.dialogueAction == 58) {

				c.getPA().startTeleport(3306, 3916, 0, "modern");
				
				c.sendMessage("The Chaos Elemental can be located on the western side of the castle.");
			}
			else if (c.dialogueAction == 59) {
				
				c.getPA().startTeleport(2910, 3610, 4, "modern");
			}

			if (c.dialogueAction != 59) {

				c.dialogueAction = -1;
			}
			break;

		case 9168:
			if (c.dialogueAction == 56) {
				c.getPA().startTeleport(3564, 3291, 0, "modern");
			}
			else if (c.dialogueAction == 57) {

				c.getPA().startTeleport(2980, 3601, 0, "modern");
			}
			else if (c.dialogueAction == 58) {

				c.getPA().startTeleport(1909, 4367, 0, "modern");
			}
			else if (c.dialogueAction == 59) {

				c.getPA().startTeleport(2910, 3610, 8, "modern");
			}
			if (c.dialogueAction != 59) {

				c.dialogueAction = -1;
			}
			break;

		case 9167:
		if (c.dialogueAction == 56) {
				c.getPA().startTeleport(3564, 3291, 0, "modern");
			}
			if (c.dialogueAction == 56) {
				c.getPA().startTeleport(Constants.DUELING_RESPAWN_X, Constants.DUELING_RESPAWN_Y, 0, "modern");
			}
			else if (c.dialogueAction == 57) {
				c.getPA().startTeleport(3243, 3518, 0, "modern");
			}
			else if (c.dialogueAction == 59) {
				
				c.getPA().startTeleport(2910, 3610, 12, "modern");
			}
			if (c.dialogueAction != 59) {
				
				c.dialogueAction = -1;
			}
			break;

		case 9178:
		if (c.dialogueAction == 95) {
		// slayer tower
				c.getPA().startTeleport(3427, 3537, 0, "modern");
				c.sendMessage("The wizard teleports you to Canifis Slayer Tower.");
			}
			if (c.usingGlory)
				c.getPA().startTeleport(Constants.EDGEVILLE_X, Constants.EDGEVILLE_Y,
						0, "modern");
			if (c.dialogueAction == 2)
				c.getPA().startTeleport(3428, 3538, 0, "modern");
			if (c.dialogueAction == 3)
				c.getPA().startTeleport(Constants.EDGEVILLE_X, Constants.EDGEVILLE_Y,
						0, "modern");
			if (c.dialogueAction == 4)
				c.getPA().startTeleport(3565, 3314, 0, "modern");
			if (c.dialogueAction == 20) {
				c.getPA().startTeleport(2897, 3618, 4, "modern");
				c.killCount = 0;
			} else if (c.dialogueAction == 55) {
				c.getPA().startTeleport(2678, 3715, 0, "modern");
			} else if (c.dialogueAction == 56) {
				c.getPA().startTeleport(Constants.DUELING_RESPAWN_X, Constants.DUELING_RESPAWN_Y, 0, "modern");
			}
			c.dialogueAction = -1;
			break;

		case 9179:
		if (c.dialogueAction == 95) {
		//duel arena
				c.getPA().startTeleport(3351, 3269, 0, "modern");
				c.sendMessage("The wizard teleports you to Al-Kharid Duel Arena.");
				}
			if (c.usingGlory)
				c.getPA().startTeleport(Constants.AL_KHARID_X, Constants.AL_KHARID_Y,
						0, "modern");
			if (c.dialogueAction == 2)
				c.getPA().startTeleport(2884, 3395, 0, "modern");
			if (c.dialogueAction == 3)
				c.getPA().startTeleport(3243, 3513, 0, "modern");
			if (c.dialogueAction == 4)
				c.getPA().startTeleport(2444, 5170, 0, "modern");
			if (c.dialogueAction == 20) {
				c.getPA().startTeleport(2897, 3618, 12, "modern");
				c.killCount = 0;
			} else if (c.dialogueAction == 55) {
				c.getPA().startTeleport(2514, 4641, 0, "modern");
			}
			c.dialogueAction = -1;
			break;

		case 9180:
		if (c.dialogueAction == 95) {
		//Varrock Sewers
				c.getPA().startTeleport(3225, 9862, 0, "modern");
				c.sendMessage("The wizard teleports you to Varrock Sewers.");
				}
			if (c.usingGlory)
				c.getPA().startTeleport(Constants.KARAMJA_X, Constants.KARAMJA_Y, 0,
						"modern");
			if (c.dialogueAction == 2)
				c.getPA().startTeleport(2471, 10137, 0, "modern");
			if (c.dialogueAction == 3)
				c.getPA().startTeleport(3363, 3676, 0, "modern");
			if (c.dialogueAction == 4)
				c.getPA().startTeleport(2659, 2676, 0, "modern");
			if (c.dialogueAction == 20) {
				c.getPA().startTeleport(2897, 3618, 8, "modern");
				c.killCount = 0;
			} else if (c.dialogueAction == 55) {
				c.getPA().startTeleport(2716, 9816, 0, "modern");
			} else if (c.dialogueAction == 56) {
				c.getPA().startTeleport(3564, 3291, 0, "modern");
			}
			
			c.dialogueAction = -1;
			break;

		case 9181:
		if (c.dialogueAction == 95) {
		//more teleports 1
				c.dialogueAction = 96;
				}
			if (c.usingGlory)
				c.getPA().startTeleport(Constants.MAGEBANK_X, Constants.MAGEBANK_Y, 0, "modern");
			if (c.dialogueAction == 2)
				c.getPA().startTeleport(2669, 3714, 0, "modern");
			if (c.dialogueAction == 3)
				c.getPA().startTeleport(2540, 4716, 0, "modern");
			if (c.dialogueAction == 4) {
				c.getPA().startTeleport(3366, 3266, 0, "modern");
				c.sendMessage("Dueling is at your own risk. Refunds will not be given for items lost due to glitches.");
			}
			if (c.dialogueAction == 20) {
				c.sendMessage("This will be added shortly");
			} else if (c.dialogueAction == 55) {
				c.getPA().startTeleport(3428, 3538, 0, c.playerMagicBook == 0 ? "modern" : "ancient");
			} else if (c.dialogueAction == 56) {
				c.getPA().startTeleport(2657, 2649, 0, "modern");
			}
			
			c.dialogueAction = -1;
			break;

		case 1093:
		case 1094:
		case 1097:
			if (c.autocastId > 0) {
				c.getPA().resetAutocast();
			} else {
				if (c.playerMagicBook == 1) {
					if (c.playerEquipment[c.playerWeapon] == 4675
							|| (c.playerEquipment[c.playerWeapon] == 15486 || (c.playerEquipment[c.playerWeapon] == 15040 || (c.playerEquipment[c.playerWeapon] == 6914))))
						c.setSidebarInterface(0, 1689);
					else
						c.sendMessage("You can't autocast ancients without an ancient staff.");
				} else if (c.playerMagicBook == 0) {
					if (c.playerEquipment[c.playerWeapon] == 4170) {
						c.setSidebarInterface(0, 12050);
					} else {
						c.setSidebarInterface(0, 1829);
					}
				}

			}
			break;

		case 9157:
			if (c.dialogueAction == 1) {
				int r = 4;
				switch (r) {
				case 0:
					c.getPA().movePlayer(3534, 9677, 0);
					break;

				case 1:
					c.getPA().movePlayer(3534, 9712, 0);
					break;

				case 2:
					c.getPA().movePlayer(3568, 9712, 0);
					break;

				case 3:
					c.getPA().movePlayer(3568, 9677, 0);
					break;
				case 4:
					c.getPA().movePlayer(3551, 9694, 0);
					break;
				}
			} else if (c.dialogueAction == 2) {
				c.getPA().movePlayer(2507, 4717, 0);
			} else if (c.dialogueAction == 7) {
				c.getPA().startTeleport(3088, 3933, 0, "modern");
				c.sendMessage("NOTE: You are now in the wilderness...");
			} else if (c.dialogueAction == 8) {
				c.getPA().resetBarrows();
				c.sendMessage("Your barrows have been reset.");
			} else if (c.dialogueAction == 83) {
				c.getPA().startTeleport(3045, 9788, 0, "modern");
			} else if (c.dialogueAction == 750) {
				c.getPA().fade(2709, 9473, 0, "@dre@A bright light begins to blind you as you follow the portal's path.");
			} else if (c.dialogueAction == 751) {
				if (c.taskAmount > 0) {
					c.getDH().sendDialogues(24, 1597);
				} else {
					c.getDH().sendDialogues(25, 1597);
				}
			} else if (c.dialogueAction == 90) {
				c.getShops().openShop(3);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 91) {
				c.getShops().openShop(4);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 92) {
				c.getPA().fade(2480, 3437, 0, "The Gnome leads you to the Agility Course..");
				c.dialogueAction = -1;
			}
			c.dialogueAction = 0;
			break;

		case 9158:
			if (c.dialogueAction == 8) {
				c.getPA().fixAllBarrows();
			} else if (c.dialogueAction == 751) {
				c.getDH().sendDialogues(26, 1597);
			} else if (c.dialogueAction == 750 || c.dialogueAction == 67) {
				c.getPA().closeAllWindows();
			} else if (c.dialogueAction == 83) {
				c.getPA().startTeleport(2726, 3485, 0, "modern");
			} else if (c.dialogueAction == 90) {
				c.getDH().sendDialogues(83, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 91) {
				c.getDH().sendDialogues(86, 0);
				c.dialogueAction = -1;
			} else if (c.dialogueAction == 92) {
				c.getDH().sendDialogues(86, 0);
				c.dialogueAction = -1;
			}
			c.dialogueAction = 0;
			break;

		case 29188:
			c.specBarId = 7636;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29163:
			c.specBarId = 7611;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 33033:
			c.specBarId = 8505;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29063:
			if (c.getCombat()
					.checkSpecAmount(c.playerEquipment[c.playerWeapon])) {
				c.gfx0(246);
				c.forcedChat("Raarrrrrgggggghhhhhhh!");
				c.startAnimation(1056);
				c.playerLevel[2] = c.getLevelForXP(c.playerXP[2])
						+ (c.getLevelForXP(c.playerXP[2]) * 15 / 100);
				c.getPA().refreshSkill(2);
				c.getItems().updateSpecialBar();
			} else {
				c.sendMessage("You don't have the required special energy to use this attack.");
			}
			break;

		case 48023:
			c.specBarId = 12335;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29138:
			c.specBarId = 7586;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29113:
			c.specBarId = 7561;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29238:
			c.specBarId = 7686;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 26065:
		case 26040:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(0);
			break;

		case 26066:
		case 26048:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(1);
			break;

		case 26069:
		case 26042:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(2);
			break;

		case 26070: 
		case 26043:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(3);
			break;

		case 26071: 
		case 26041:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(4);
			break;

		case 26072:
		case 26045:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(5);
			break;

		case 26073: 
		case 26046:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(6);
			break;

		case 26074:
		case 26047:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(7);
			break;

		case 26076:
		case 26075:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(8);
			break;

		case 2158: 
		case 2157:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(9);
			break;

		case 30136: 
		case 30137:
			c.duelSlot = -1;
			c.getTradeAndDuel().selectRule(10);
			break;

		case 53245: 
			c.duelSlot = 0;
			c.getTradeAndDuel().selectRule(11);
			break;

		case 53246: 
			c.duelSlot = 1;
			c.getTradeAndDuel().selectRule(12);
			break;
		
		case 53247: 
			c.duelSlot = 2;
			c.getTradeAndDuel().selectRule(13);
			break;

		case 53249: 
			c.duelSlot = 3;
			c.getTradeAndDuel().selectRule(14);
			break;

		case 53250: 
			c.duelSlot = 4;
			c.getTradeAndDuel().selectRule(15);
			break;

		case 53251: 
			c.duelSlot = 5;
			c.getTradeAndDuel().selectRule(16);
			break;

		case 53252: 
			c.duelSlot = 7;
			c.getTradeAndDuel().selectRule(17);
			break;

		case 53255: 
			c.duelSlot = 9;
			c.getTradeAndDuel().selectRule(18);
			break;

		case 53254: 
			c.duelSlot = 10;
			c.getTradeAndDuel().selectRule(19);
			break;

		case 53253: 
			c.duelSlot = 12;
			c.getTradeAndDuel().selectRule(20);
			break;

		case 53248:
			c.duelSlot = 13;
			c.getTradeAndDuel().selectRule(21);
			break;

		case 4169: 
			c.usingMagic = true;
			if (!c.getCombat().checkMagicReqs(48)) {
				break;
			}
			if (System.currentTimeMillis() - c.godSpellDelay < Constants.GOD_SPELL_CHARGE) {
				c.sendMessage("You still feel the charge in your body!");
				break;
			}
			c.godSpellDelay = System.currentTimeMillis();
			c.sendMessage("You feel charged with a magical power!");
			c.gfx100(c.MAGIC_SPELLS[48][3]);
			c.startAnimation(c.MAGIC_SPELLS[48][2]);
			c.usingMagic = false;
			break;

		case 152:
			
			c.isRunning2 = false;
			break;
			
		case 153:
			
			c.isRunning2 = true;
			break;

		case 9154:
			c.logout();
			break;

		case 21010:
			c.takeAsNote = true;
			break;

		case 21011:
			c.takeAsNote = false;
			break;

		case 117048:
			c.getPA().startTeleport(3086, 3488, 0, "modern");
			break;

			case 4146:
			c.getPA().startTeleport(2964, 3379, 0, "modern");
			break;
			
			case 6004:
			c.getPA().startTeleport(2662, 3307, 0, "modern");
			break;
			
			case 4143:
			c.getPA().startTeleport(3222, 3218, 0, "modern");
			break;
			
			case 4150:
			c.getPA().startTeleport(2757, 3477, 0, "modern");
			break;
			
			case 6005:
			c.getPA().startTeleport(2931, 4713, 0, "modern");
			break;
			
			case 50245:
			c.getPA().startTeleport(3319, 3337, 0, "ancient");
			break;
			
			case 50253:
			c.getPA().startTeleport(3500, 3484, 0, "ancient");
			break;
			
			case 51005:
			c.getPA().startTeleport(2999, 3469, 0, "ancient");
			break;
			
			case 51013:
			c.getPA().startTeleport(2967, 3697, 0, "ancient");
			break;
			
			case 51023:
			c.getPA().startTeleport(3162, 3667, 0, "ancient");
			break;
			
			case 51031:
			c.getPA().startTeleport(3288, 3886, 0, "ancient");
			break;
			
			case 51039:
			c.getPA().startTeleport(2966, 3874, 0, "ancient");
			break;
			
		case 50235:
		case 4140:

			c.getPA().startTeleport(3085, 3497, 0, c.playerMagicBook == 0 ? "modern" : "ancient");
			break;
			
		case 63084:
			c.getPA().closeAllWindows();
			break;

		case 9125: 
		case 6221: 
		case 22230: 
		case 48010: 
		case 21200: 
		case 1080: 
		case 6168: 
		case 6236: 
		case 17102: 
		case 8234: 
			c.fightMode = 0;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;

		case 9126: 
		case 48008: 
		case 22228: 
		case 21201: 
		case 1078: 
		case 6169: 
		case 33019: 
		case 18078: 
		case 8235:
			c.fightMode = 1;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;

		case 9127: 
		case 48009: 
		case 33018:
		case 6234: 
		case 6219: 
		case 18077: 
		case 18080: 
		case 18079: 
		case 17100: 
			c.fightMode = 3;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;

		case 9128: 
		case 6220: 
		case 22229: 
		case 21203: 
		case 21202:
		case 1079:
		case 6171: 
		case 6170:
		case 33020: 
		case 6235: 
		case 17101: 
		case 8237: 
		case 8236: 
			c.fightMode = 2;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;

		case 21233: 
			c.getCombat().activatePrayer(0);
			break;
			
		case 21234: 
			c.getCombat().activatePrayer(1);
			break;
			
		case 21235: 
			c.getCombat().activatePrayer(2);
			break;
			
		case 70080: 
			c.getCombat().activatePrayer(3);
			break;
			
		case 70082: 
			c.getCombat().activatePrayer(4);
			break;
			
		case 21236: 
			c.getCombat().activatePrayer(5);
			break;
			
		case 21237: 
			c.getCombat().activatePrayer(6);
			break;
			
		case 21238:
			c.getCombat().activatePrayer(7);
			break;
			
		case 21239:
			c.getCombat().activatePrayer(8);
			break;
			
		case 21240:
			c.getCombat().activatePrayer(9);
			break;
			
		case 21241: 
			c.getCombat().activatePrayer(10);

			break;
			
		case 70084:
			c.getCombat().activatePrayer(11);
			break;
			
		case 70086: 
			c.getCombat().activatePrayer(12);
			break;
			
		case 21242:
			c.getCombat().activatePrayer(13);
			break;
			
		case 21243:
			c.getCombat().activatePrayer(14);
			break;
			
		case 21244:
			c.getCombat().activatePrayer(15);
			break;
			
		case 21245: 
			c.getCombat().activatePrayer(16);
			break;
			
		case 21246:
			c.getCombat().activatePrayer(17);
			break;
			
		case 21247: 
			c.getCombat().activatePrayer(18);
			break;
		case 70088: 
			c.getCombat().activatePrayer(19);
			break;
			
		case 70090: 
			c.getCombat().activatePrayer(20);
			break;
			
		case 2171: 
			c.getCombat().activatePrayer(21);
			break;
			
		case 2172: 
			c.getCombat().activatePrayer(22);
			break;
			
		case 2173: 
			c.getCombat().activatePrayer(23);
			break;
			
		case 70092: 
			c.getCombat().activatePrayer(24);
			break;
			
		case 70094:
			c.getCombat().activatePrayer(25);
			break;

		case 13092:
			if (System.currentTimeMillis() - c.lastButton < 400) {
				c.lastButton = System.currentTimeMillis();
				break;
			} else {
				c.lastButton = System.currentTimeMillis();
			}
			Client ot = (Client) PlayerHandler.players[c.tradeWith];
			if (ot == null) {
				c.getTradeAndDuel().declineTrade();
				c.sendMessage("Trade declined as the other player has disconnected.");
				break;
			}
			c.getPA().sendFrame126("Waiting for other player...", 3431);
			ot.getPA().sendFrame126("Other player has accepted", 3431);
			c.goodTrade = true;
			ot.goodTrade = true;
			for (GameItem item : c.getTradeAndDuel().offeredItems) {
				if (item.id > 0) {
					if (ot.getItems().freeSlots() < c.getTradeAndDuel().offeredItems
							.size()) {
						c.sendMessage(ot.playerName
								+ " only has "
								+ ot.getItems().freeSlots()
								+ " free slots, please remove "
								+ (c.getTradeAndDuel().offeredItems.size() - ot
										.getItems().freeSlots()) + " items.");
						ot.sendMessage(c.playerName
								+ " has to remove "
								+ (c.getTradeAndDuel().offeredItems.size() - ot
										.getItems().freeSlots())
								+ " items or you could offer them "
								+ (c.getTradeAndDuel().offeredItems.size() - ot
										.getItems().freeSlots()) + " items.");
						c.goodTrade = false;
						ot.goodTrade = false;
						c.getPA().sendFrame126("Not enough inventory space...",
								3431);
						ot.getPA().sendFrame126(
								"Not enough inventory space...", 3431);
						break;
					} else {
						c.getPA().sendFrame126("Waiting for other player...",
								3431);
						ot.getPA().sendFrame126("Other player has accepted",
								3431);
						c.goodTrade = true;
						ot.goodTrade = true;
					}
				}
			}
			if (c.inTrade && !c.tradeConfirmed && ot.goodTrade && c.goodTrade) {
				c.tradeConfirmed = true;
				if (ot.tradeConfirmed) {
					c.getTradeAndDuel().confirmScreen();
					ot.getTradeAndDuel().confirmScreen();
					break;
				}
			}
			break;

		case 13218:
			if (System.currentTimeMillis() - c.lastButton < 400) {
				c.lastButton = System.currentTimeMillis();
				break;
			} else {
				c.lastButton = System.currentTimeMillis();
			}
			c.tradeAccepted = true;
			Client ot1 = (Client) PlayerHandler.players[c.tradeWith];
			if (ot1 == null) {
				c.getTradeAndDuel().declineTrade();
				c.sendMessage("Trade declined as the other player has disconnected.");
				break;
			}
			if (c.inTrade && c.tradeConfirmed && ot1.tradeConfirmed && !c.tradeConfirmed2) {
				c.tradeConfirmed2 = true;
				if (ot1.tradeConfirmed2) {
					c.acceptedTrade = true;
					ot1.acceptedTrade = true;
					c.getTradeAndDuel().giveItems();
					ot1.getTradeAndDuel().giveItems();
					break;
				}
				ot1.getPA().sendFrame126("Other player has accepted.", 3535);
				c.getPA().sendFrame126("Waiting for other player...", 3535);
			}

			break;
		
		case 74176:
			if (!c.mouseButton) {
				c.mouseButton = true;
				c.getPA().sendFrame36(500, 1);
				c.getPA().sendFrame36(170, 1);
			} else if (c.mouseButton) {
				c.mouseButton = false;
				c.getPA().sendFrame36(500, 0);
				c.getPA().sendFrame36(170, 0);
			}
			break;
			
		case 74184:
			if (!c.splitChat) {
				c.splitChat = true;
				c.getPA().sendFrame36(502, 1);
				c.getPA().sendFrame36(287, 1);
			} else {
				c.splitChat = false;
				c.getPA().sendFrame36(502, 0);
				c.getPA().sendFrame36(287, 0);
			}
			break;
			
		case 74180:
			if (!c.chatEffects) {
				c.chatEffects = true;
				c.getPA().sendFrame36(501, 1);
				c.getPA().sendFrame36(171, 0);
			} else {
				c.chatEffects = false;
				c.getPA().sendFrame36(501, 0);
				c.getPA().sendFrame36(171, 1);
			}
			break;
			
		case 74188:
			if (!c.acceptAid) {
				c.acceptAid = true;
				c.getPA().sendFrame36(503, 1);
				c.getPA().sendFrame36(427, 1);
			} else {
				c.acceptAid = false;
				c.getPA().sendFrame36(503, 0);
				c.getPA().sendFrame36(427, 0);
			}
			break;
			
		case 74192:
			if (!c.isRunning2) {
				c.isRunning2 = true;
				c.getPA().sendFrame36(504, 1);
				c.getPA().sendFrame36(173, 1);
			} else {
				c.isRunning2 = false;
				c.getPA().sendFrame36(504, 0);
				c.getPA().sendFrame36(173, 0);
			}
			break;
			
		case 74201:
			c.getPA().sendFrame36(505, 1);
			c.getPA().sendFrame36(506, 0);
			c.getPA().sendFrame36(507, 0);
			c.getPA().sendFrame36(508, 0);
			c.getPA().sendFrame36(166, 1);
			break;
			
		case 74203:
			c.getPA().sendFrame36(505, 0);
			c.getPA().sendFrame36(506, 1);
			c.getPA().sendFrame36(507, 0);
			c.getPA().sendFrame36(508, 0);
			c.getPA().sendFrame36(166, 2);
			break;

		case 74204:
			c.getPA().sendFrame36(505, 0);
			c.getPA().sendFrame36(506, 0);
			c.getPA().sendFrame36(507, 1);
			c.getPA().sendFrame36(508, 0);
			c.getPA().sendFrame36(166, 3);
			break;

		case 74205:
			c.getPA().sendFrame36(505, 0);
			c.getPA().sendFrame36(506, 0);
			c.getPA().sendFrame36(507, 0);
			c.getPA().sendFrame36(508, 1);
			c.getPA().sendFrame36(166, 4);
			break;
			
		case 74206:
			c.getPA().sendFrame36(509, 1);
			c.getPA().sendFrame36(510, 0);
			c.getPA().sendFrame36(511, 0);
			c.getPA().sendFrame36(512, 0);
			break;
			
		case 74207:
			c.getPA().sendFrame36(509, 0);
			c.getPA().sendFrame36(510, 1);
			c.getPA().sendFrame36(511, 0);
			c.getPA().sendFrame36(512, 0);
			break;
			
		case 74208:
			c.getPA().sendFrame36(509, 0);
			c.getPA().sendFrame36(510, 0);
			c.getPA().sendFrame36(511, 1);
			c.getPA().sendFrame36(512, 0);
			break;
			
		case 74209:
			c.getPA().sendFrame36(509, 0);
			c.getPA().sendFrame36(510, 0);
			c.getPA().sendFrame36(511, 0);
			c.getPA().sendFrame36(512, 1);
			break;
			
		case 168:
			c.startAnimation(855);
			break;
			
		case 169:
			c.startAnimation(856);
			break;
			
		case 162:
			c.startAnimation(857);
			break;
			
		case 164:
			c.startAnimation(858);
			break;
			
		case 165:
			c.startAnimation(859);
			break;
			
		case 161:
			c.startAnimation(860);
			break;
			
		case 170:
			c.startAnimation(861);
			break;
			
		case 171:
			c.startAnimation(862);
			break;
			
		case 163:
			c.startAnimation(863);
			break;
			
		case 167:
			c.startAnimation(864);
			break;
			
		case 172:
			c.startAnimation(865);
			break;
			
		case 166:
			c.startAnimation(866);
			break;
			
		case 52050:
			c.startAnimation(2105);
			break;
			
		case 52051:
			c.startAnimation(2106);
			break;
			
		case 52052:
			c.startAnimation(2107);
			break;
			
		case 52053:
			c.startAnimation(2108);
			break;
			
		case 52054:
			c.startAnimation(2109);
			break;
			
		case 52055:
			c.startAnimation(2110);
			break;
			
		case 52056:
			c.startAnimation(2111);
			break;
			
		case 52057:
			c.startAnimation(2112);
			break;
			
		case 52058:
			c.startAnimation(2113);
			break;
			
		case 43092:
			c.startAnimation(0x558);
			break;
			
		case 2155:
			c.startAnimation(0x46B);
			break;
			
		case 25103:
			c.startAnimation(0x46A);
			break;
			
		case 25106:
			c.startAnimation(0x469);
			break;
			
		case 2154:
			c.startAnimation(0x468);
			break;
			
		case 52071:
			c.startAnimation(0x84F);
			break;
			
		case 52072:
			c.startAnimation(0x850);
			break;
			
		case 59062:
			c.startAnimation(2836);
			break;
			
		case 72032:
			c.startAnimation(3544);
			break;
			
		case 72033:
			c.startAnimation(3543);
			break;
			
		case 72254:
			c.startAnimation(3866);
			break;

		case 118098:
			c.getPA().castVeng();
			break;

		case 24017:
			c.getPA().resetAutocast();
			c.getItems()
					.sendWeapon(
							c.playerEquipment[c.playerWeapon],
							c.getItems().getItemName(
									c.playerEquipment[c.playerWeapon]));
			break;
		}
	}
}