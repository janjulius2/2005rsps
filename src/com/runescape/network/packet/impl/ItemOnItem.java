package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.model.player.content.skill.Firemaking;
import com.runescape.network.packet.PacketDecoder;

public class ItemOnItem implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		
		int usedWithSlot = c.getInStream().readUnsignedWord();
		int itemUsedSlot = c.getInStream().readUnsignedWordA();
		int useWith = c.playerItems[usedWithSlot] - 1;
		int itemUsed = c.playerItems[itemUsedSlot] - 1;

		if(!c.getItems().playerHasItem(useWith, 1, usedWithSlot) || !c.getItems().playerHasItem(itemUsed, 1, itemUsedSlot)) {
			return;
		}
		
		if (Firemaking.isLog(itemUsed)) {
			
			Firemaking.createFire(c, itemUsed, itemUsedSlot);
		}
		
		if (Firemaking.isLog(useWith)) {
			
			Firemaking.createFire(c, useWith, usedWithSlot);
		}
	}
}
