package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;

public class EquipItem implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		
		c.wearId = c.getInStream().readUnsignedWord();
		c.wearSlot = c.getInStream().readUnsignedWordA();
		c.interfaceId = c.getInStream().readUnsignedWordA();

		if(!c.getItems().playerHasItem(c.wearId, 1, c.wearSlot)) {
			
			return;
		}

		if (c.playerIndex > 0 || c.npcIndex > 0) {
			
			c.getCombat().resetPlayerAttack();
		}

		c.getItems().wearItem(c.wearId, c.wearSlot);
		
		c.getPA().closeAllWindows();
		
		c.getTradeAndDuel().declineTrade();
	}
}
