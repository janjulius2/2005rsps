package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;

/**
 * Magic on items
 **/
public class MagicOnItems implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		int slot = c.getInStream().readSignedWord();
		int itemId = c.getInStream().readSignedWordA();
		c.getInStream().readSignedWord();
		int spellId = c.getInStream().readSignedWordA();

		c.usingMagic = true;
		c.getPA().magicOnItems(slot, itemId, spellId);
		c.usingMagic = false;

	}

}
