package com.runescape.network.packet.impl;

import com.runescape.model.player.Client;
import com.runescape.model.player.content.skill.Prayer;
import com.runescape.network.packet.PacketDecoder;

public class ItemOnObject implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		c.getInStream().readUnsignedWord();
		int objectId = c.getInStream().readSignedWordBigEndian();
		int objectY = c.getInStream().readSignedWordBigEndianA();
		c.getInStream().readUnsignedWord();
		int objectX = c.getInStream().readSignedWordBigEndianA();
		int itemId = c.getInStream().readUnsignedWord();
		
		if(!c.getItems().playerHasItem(itemId, 1)) {
            return;
        }
		
		c.turnPlayerTo(objectX, objectY);
		
		if (Prayer.boneOnAltar(itemId, objectId, c)) {
			
		}
	}
}
