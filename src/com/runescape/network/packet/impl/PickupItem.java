package com.runescape.network.packet.impl;

import com.runescape.Application;
import com.runescape.model.player.Client;
import com.runescape.network.packet.PacketDecoder;


public class PickupItem implements PacketDecoder {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		c.pItemY = c.getInStream().readSignedWordBigEndian();
		c.pItemId = c.getInStream().readUnsignedWord();
		c.pItemX = c.getInStream().readSignedWordBigEndian();
		c.setWoodcutting(false);
		if (Math.abs(c.getX() - c.pItemX) > 25 || Math.abs(c.getY() - c.pItemY) > 25) {
			c.resetWalkingQueue();
			return;
		}
		if (c.isFiremkaing) {
			return;
		}
		c.getCombat().resetPlayerAttack();
		if (c.getX() == c.pItemX && c.getY() == c.pItemY) {
			Application.itemHandler.removeGroundItem(c, c.pItemId, c.pItemX, c.pItemY, true);
		} else {
			c.walkingToItem = true;
		}
	}
}
