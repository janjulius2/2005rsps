package com.runescape.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ConnectionValidation {

	public static ArrayList<String> bannedIps = new ArrayList<String>();
	
	public static ArrayList<String> bannedNames = new ArrayList<String>();
	
	public static ArrayList<String> mutedIps = new ArrayList<String>();
	
	public static ArrayList<String> mutedNames = new ArrayList<String>();
	
	public static ArrayList<String> loginLimitExceeded = new ArrayList<String>();

	public static void initialize() {
		banUsers();
		banIps();
		muteUsers();
		muteIps();
	}

	public static void addIpToLoginList(String IP) {
		loginLimitExceeded.add(IP);
	}

	public static void removeIpFromLoginList(String IP) {
		loginLimitExceeded.remove(IP);
	}

	public static void clearLoginList() {
		loginLimitExceeded.clear();
	}

	public static boolean checkLoginList(String complexAddress) {
		loginLimitExceeded.add(complexAddress);
		int accumulator = 0;
		for (String address : loginLimitExceeded) {
			if (complexAddress.equals(address)) {
				accumulator ++;
			}
		}
		if (accumulator > 5) {
			return true;
		}
		return false;
	}

	public static void unMuteUser(String name) {
		mutedNames.remove(name);
		deleteFromFile("./Data/bans/UsersMuted.txt", name);
	}

	public static void unIPMuteUser(String name) {
		mutedIps.remove(name);
		deleteFromFile("./Data/bans/IpsMuted.txt", name);
	}

	public static void addIpToBanList(String IP) {
		bannedIps.add(IP);
	}

	public static void addIpToMuteList(String IP) {
		mutedIps.add(IP);
		addIpToMuteFile(IP);
	}

	public static void removeIpFromBanList(String IP) {
		bannedIps.remove(IP);
	}

	public static boolean isIpBanned(String IP) {
		if (bannedIps.contains(IP)) {
			return true;
		}
		return false;
	}

	public static void addNameToBanList(String name) {
		bannedNames.add(name.toLowerCase());
	}

	public static void addNameToMuteList(String name) {
		mutedNames.add(name.toLowerCase());
		addUserToFile(name);
	}

	public static void removeNameFromBanList(String name) {
		bannedNames.remove(name.toLowerCase());
		deleteFromFile("./Data/bans/UsersBanned.txt", name);
	}

	public static void removeNameFromMuteList(String name) {
		bannedNames.remove(name.toLowerCase());
		deleteFromFile("./Data/bans/UsersMuted.txt", name);
	}

	public static void deleteFromFile(String file, String name) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			ArrayList<String> collection = new ArrayList<String>();
			while (true) {
				String line = reader.readLine();
				if (line == null) {
					break;
				} else {
					line = line.trim();
				}
				if (!line.equalsIgnoreCase(name)) {
					collection.add(line);
				}
			}
			reader.close();
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			for (String line : collection) {
				writer.write(line, 0, line.length());
				writer.newLine();
			}
			writer.flush();
			writer.close();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public static boolean isNamedBanned(String name) {
		if (bannedNames.contains(name.toLowerCase())) {
			return true;
		}
		return false;
	}

	public static void banUsers() {
		try {
			BufferedReader input = new BufferedReader(new FileReader("./Data/bans/UsersBanned.txt"));
			String data = null;
			try {
				while ((data = input.readLine()) != null) {
					addNameToBanList(data);
				}
			} finally {
				input.close();
			}
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	public static void muteUsers() {
		try {
			BufferedReader in = new BufferedReader(new FileReader("./Data/bans/UsersMuted.txt"));
			String data = null;
			try {
				while ((data = in.readLine()) != null) {
					mutedNames.add(data);
				}
			} finally {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void banIps() {
		try {
			BufferedReader in = new BufferedReader(new FileReader("./Data/bans/IpsBanned.txt"));
			String data = null;
			try {
				while ((data = in.readLine()) != null) {
					addIpToBanList(data);
				}
			} finally {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void muteIps() {
		try {
			BufferedReader in = new BufferedReader(new FileReader("./Data/bans/IpsMuted.txt"));
			String data = null;
			try {
				while ((data = in.readLine()) != null) {
					mutedIps.add(data);
				}
			} finally {
				in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void addNameToFile(String Name) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("./Data/bans/UsersBanned.txt", true));
			try {
				out.newLine();
				out.write(Name);
			} finally {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void addUserToFile(String Name) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("./Data/bans/UsersMuted.txt", true));
			try {
				out.newLine();
				out.write(Name);
			} finally {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void addIpToFile(String Name) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("./Data/bans/IpsBanned.txt", true));
			try {
				out.newLine();
				out.write(Name);
			} finally {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void addIpToMuteFile(String Name) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("./Data/bans/IpsMuted.txt", true));
			try {
				out.newLine();
				out.write(Name);
			} finally {
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}