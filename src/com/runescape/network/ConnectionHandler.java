package com.runescape.network;

import org.apache.mina.common.IdleStatus;
import org.apache.mina.common.IoHandler;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;

import com.runescape.model.player.Client;

public class ConnectionHandler implements IoHandler {

	@Override
	public void exceptionCaught(IoSession arg0, Throwable arg1) throws Exception {
		
	}

	@Override
	public void messageReceived(IoSession session, Object component) throws Exception {
		if (session.getAttachment() != null) {
			Client client = (Client) session.getAttachment();
			client.queueMessage((Packet) component);
		}
	}

	@Override
	public void messageSent(IoSession arg0, Object arg1) throws Exception {

	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		if (session.getAttachment() != null) {
			Client player = (Client) session.getAttachment();
			player.disconnected = true;
		}
		HostList.getHostList().remove(session);
	}

	@Override
	public void sessionCreated(IoSession session) throws Exception {
		if (!HostList.getHostList().add(session)) {
			session.close();
		} else {
			session.setAttribute("inList", Boolean.TRUE);
		}
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus arg1) throws Exception {
		session.close();
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		session.setIdleTime(IdleStatus.BOTH_IDLE, 60);
		session.getFilterChain().addLast("protocolFilter", new ProtocolCodecFilter(new CodecFactory()));
	}
}