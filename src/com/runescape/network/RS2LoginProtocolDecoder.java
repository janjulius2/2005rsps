package com.runescape.network;

import java.math.BigInteger;

import org.apache.mina.common.ByteBuffer;
import org.apache.mina.common.IoFuture;
import org.apache.mina.common.IoFutureListener;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.runescape.Application;
import com.runescape.Constants;
import com.runescape.model.player.Client;
import com.runescape.model.player.PlayerAccount;
import com.runescape.world.PlayerHandler;
import com.runescape.world.utilities.ISAACNumberGenerator;

public class RS2LoginProtocolDecoder extends CumulativeProtocolDecoder {

	private static final BigInteger RSA_MODULUS = new BigInteger("111635244960310350061876141122122451276671926096693999876643308378298606251342298770784235561994940470395754470685829318722150017925990041323523225734555816512454397238045167724430569970118676666061278782715908644863043435525213833424793948968240988554539774440864602451274011372325179360911371639772016545453");

	private static final BigInteger RSA_EXPONENT = new BigInteger("96352405987151608270900772427374711330469009867090286449216911644085946299146697742792777889806427021498326924246516249042806594198447086187422238791448320181588598245013394708812097670719666308048211770150047524434642479849559017107456625788640620417061557127486889638403549825625276180695173438670487637933");

	@Override
	public boolean doDecode(IoSession session, ByteBuffer in, ProtocolDecoderOutput out) {
		synchronized (session) {
			Object loginStageObj = session.getAttribute("LOGIN_STAGE");
			int loginStage = 0;
			if (loginStageObj != null) {
				loginStage = (Integer) loginStageObj;
			}
			switch (loginStage) {
			
			case 0:
				if (2 <= in.remaining()) {
					int protocol = in.get() & 0xff;
					@SuppressWarnings("unused")
					int nameHash = in.get() & 0xff;
					if (protocol == 14) {
						long serverSessionKey = ((long) (java.lang.Math.random() * 99999999D) << 32)+ (long) (java.lang.Math.random() * 99999999D);
						StaticPacketBuilder s1Response = new StaticPacketBuilder();
						s1Response.setBare(true).addBytes(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 }).addByte((byte) 0).addLong(serverSessionKey);
						session.setAttribute("SERVER_SESSION_KEY", serverSessionKey);
						session.write(s1Response.toPacket());
						session.setAttribute("LOGIN_STAGE", 1);
					}
					return true;
				} else {
					in.rewind();
					return false;
				}
				
			case 1:
				int loginPacketSize = -1,
				loginEncryptPacketSize = -1;
				if (2 <= in.remaining()) {
					in.get();
					loginPacketSize = in.get() & 0xff;
					loginEncryptPacketSize = loginPacketSize - (36 + 1 + 1 + 2);
					if (loginPacketSize <= 0 || loginEncryptPacketSize <= 0) {
						System.out.println("Zero or negative login size.");
						session.close();
						return false;
					}
				} else {
					in.rewind();
					return false;
				}
				if (loginPacketSize <= in.remaining()) {
					int magic = in.get() & 0xff;
					int version = in.getUnsignedShort();
					if (magic != 255) {
						session.close();
						return false;
					}
					@SuppressWarnings("unused")
					int lowMem = in.get() & 0xff;
					for (int i = 0; i < 9; i++) {
						in.getInt();
					}
					loginEncryptPacketSize--;
					if(loginEncryptPacketSize != (in.get() & 0xff)) {
						System.out.println("Encrypted size mismatch.");
						session.close();
						return false;
					}
                    byte[] encryptionBytes = new byte[loginEncryptPacketSize];
                    in.get(encryptionBytes);
                    ByteBuffer rsaBuffer = ByteBuffer.wrap(new BigInteger(encryptionBytes)
                            .modPow(RSA_EXPONENT, RSA_MODULUS).toByteArray());
					if((rsaBuffer.get() & 0xff) != 10) {
						session.close();
						return false;
					}
					long clientSessionKey = rsaBuffer.getLong();
					long serverSessionKey = rsaBuffer.getLong();
					int uid = rsaBuffer.getInt();
	
					
					String name = readRS2String(rsaBuffer);
					String pass = readRS2String(rsaBuffer);
					int sessionKey[] = new int[4];
					sessionKey[0] = (int) (clientSessionKey >> 32);
					sessionKey[1] = (int) clientSessionKey;
					sessionKey[2] = (int) (serverSessionKey >> 32);
					sessionKey[3] = (int) serverSessionKey;
					ISAACNumberGenerator inC = new ISAACNumberGenerator(sessionKey);
					for (int i = 0; i < 4; i++) {
						sessionKey[i] += 50;
					}
					ISAACNumberGenerator outC = new ISAACNumberGenerator(sessionKey);
					load(session, uid, name, pass, inC, outC, version);
					session.getFilterChain().remove("protocolFilter");
					session.getFilterChain().addLast("protocolFilter", new ProtocolCodecFilter(new GameCodecFactory(inC)));
					return true;
				} else {
					in.rewind();
					return false;
				}
			}
		}
		return false;
	}

	private synchronized void load(final IoSession session, final int uid, String name, String pass, final ISAACNumberGenerator inC, ISAACNumberGenerator outC, int version) {
		session.setAttribute("opcode", -1);
		session.setAttribute("size", -1);
		int loginDelay = 1;
		int returnCode = 2;
		name = name.trim();
		name = name.toLowerCase();
		pass = pass.toLowerCase();
		if (!name.matches("[A-Za-z0-9 ]+")) {
			returnCode = 4;
		}
		if (name.length() > 12) {
			returnCode = 8;
		}
		Client cl = new Client(session, -1);
		cl.playerName = name;
		cl.playerName2 = cl.playerName;
		cl.playerPass = pass;
		cl.setInStreamDecryption(inC);
		cl.setOutStreamDecryption(outC);
		cl.outStream.packetEncryption = outC;
		cl.saveCharacter = false;
		char first = name.charAt(0);
		cl.properName = Character.toUpperCase(first) + name.substring(1, name.length());
		if (ConnectionValidation.isNamedBanned(cl.playerName)) {
			returnCode = 23;
		}
		if (PlayerHandler.isPlayerOn(name)) {
			returnCode = 5;
		}
		if (PlayerHandler.getPlayerCount() >= Constants.MAX_PLAYERS) {
			returnCode = 7;
		}
		if (Application.update) {
			returnCode = 14;
		}
		if (returnCode == 2) {
			int load = PlayerAccount.loadGame(cl, cl.playerName, cl.playerPass);
			if (load == 0)
				cl.addStarter = true;
			if (load == 3) {
				returnCode = 3;
				cl.saveFile = false;
			} else {
				for (int i = 0; i < cl.playerEquipment.length; i++) {
					if (cl.playerEquipment[i] == 0) {
						cl.playerEquipment[i] = -1;
						cl.playerEquipmentN[i] = 0;
					}
				}
				if (!Application.playerHandler.newPlayerClient(cl)) {
					returnCode = 7;
					cl.saveFile = false;
				} else {
					cl.saveFile = true;
				}
			}
		}
		cl.packetType = -1;
		cl.packetSize = 0;
		StaticPacketBuilder bldr = new StaticPacketBuilder();
		bldr.setBare(true);
		bldr.addByte((byte) returnCode);
		if (returnCode == 2) {
			cl.saveCharacter = true;
			if (cl.playerRights == 3) {
				bldr.addByte((byte) 2);
			} else {
				bldr.addByte((byte) cl.playerRights);
			}
		} else if (returnCode == 21) {
			bldr.addByte((byte) loginDelay);
		} else {
			bldr.addByte((byte) 0);
		}
		cl.isActive = true;
		bldr.addByte((byte) 0);
		Packet pkt = bldr.toPacket();
		session.setAttachment(cl);
		session.write(pkt).addListener(new IoFutureListener() {
			@Override
			public void operationComplete(IoFuture arg0) {
				session.getFilterChain().remove("protocolFilter");
				session.getFilterChain().addFirst("protocolFilter", new ProtocolCodecFilter(new GameCodecFactory(inC)));
			}
		});
	}

	private synchronized String readRS2String(ByteBuffer in) {
		StringBuilder sb = new StringBuilder();
		byte b;
		while ((b = in.get()) != 10) {
			sb.append((char) b);
		}
		return sb.toString();
	}

	@Override
	public void dispose(IoSession session) throws Exception {
		super.dispose(session);
	}
}