package com.runescape.network;

import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

import com.runescape.world.utilities.ISAACNumberGenerator;

/**
 * Provides access to the encoders and decoders for the 508 protocol.
 * 
 * @author Graham
 */
public class GameCodecFactory implements ProtocolCodecFactory {

	/**
	 * The encoder.
	 */
	private ProtocolEncoder encoder = new RS2ProtocolEncoder();

	/**
	 * The decoder.
	 */
	private ProtocolDecoder decoder;

	/**
	 * The default class constructor.
	 * 
	 * @param inC The encryption.
	 */
	public GameCodecFactory(ISAACNumberGenerator inC) {
		decoder = new RS2ProtocolDecoder(inC);
	}

	@Override
	public ProtocolEncoder getEncoder() throws Exception {
		return encoder;
	}

	@Override
	public ProtocolDecoder getDecoder() throws Exception {
		return decoder;
	}
}