package com.runescape.network;

import org.apache.mina.common.ByteBuffer;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.runescape.model.player.Client;
import com.runescape.world.utilities.ISAACNumberGenerator;

public class RS2ProtocolDecoder extends CumulativeProtocolDecoder {

	private ISAACNumberGenerator isaac;

	protected RS2ProtocolDecoder(ISAACNumberGenerator isaac) {
		this.isaac = isaac;
	}

	@Override
	protected boolean doDecode(IoSession session, ByteBuffer in, ProtocolDecoderOutput out) throws Exception {
		synchronized (session) {
			int opcode = (Integer) session.getAttribute("opcode");
			int size = (Integer) session.getAttribute("size");
			if (opcode == -1) {
				if (in.remaining() >= 1) {
					opcode = in.get() & 0xFF;
					opcode = (opcode - isaac.getNextKey()) & 0xFF;
					size = Client.PACKET_SIZES[opcode];
					session.setAttribute("opcode", opcode);
					session.setAttribute("size", size);
				} else {
					return false;
				}
			}
			if (size == -1) {
				if (in.remaining() >= 1) {
					size = in.get() & 0xFF;
					session.setAttribute("size", size);
				} else {
					return false;
				}
			}
			if (in.remaining() >= size) {
				byte[] data = new byte[size];
				in.get(data);
				ByteBuffer payload = ByteBuffer.allocate(data.length);
				payload.put(data);
				payload.flip();
				out.write(new Packet(session, opcode, data));
				session.setAttribute("opcode", -1);
				session.setAttribute("size", -1);
				return true;
			}
			return false;
		}
	}

	@Override
	public void dispose(IoSession session) throws Exception {
		super.dispose(session);
	}
}