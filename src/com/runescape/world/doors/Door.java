package com.runescape.world.doors;

public final class Door {
	
	private final int index;
	
	private final int x;
	
	private final int y;
	
	private final int plane;
	
	private final int rotation;
	
	public Door(final int index, final int x, final int y, final int plane, final int rotation) {
		
		this.index = index;
		
		this.x = x;
		
		this.y = y;
		
		this.plane = plane;
		
		this.rotation = rotation;
	}

	public int getIndex() {
		
		return index;
	}

	public int getX() {
		
		return x;
	}

	public int getY() {
		
		return y;
	}

	public int getPlane() {
		
		return plane;
	}

	public int getRotation() {
		
		return rotation;
	}
}