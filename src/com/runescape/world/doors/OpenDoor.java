package com.runescape.world.doors;

import java.util.HashSet;
import java.util.Set;

import com.runescape.model.player.Client;
import com.runescape.world.cache.region.Region;

/**
 * A basic system for globally setting specific doors to an open state. 
 * This does not support the manual opening or closing of doors. This system was 
 * written for the simple and sole purpose of gaining access to buildings in a formal fashion.
 */
public final class OpenDoor {

	/**
	 * Stores all of the current door states.
	 */
	public static final Set<Door> loaded_doors = new HashSet<Door>();

	/**
	 * The doors that will be opened. See below for individual variable documentation.
	 */
	public static final int[][] DOORS_TO_OPEN = {

		{1530, 3037, 3347, 0, 3, 4, 0, -1},

		{1530, 3046, 3353, 0, 2, 3, 1, 0},

		{1530, 2947, 3379, 0, 0, 3, 1, 0},

		{1530, 2982, 3371, 0, 3, 4, 0, -1},

		{1512, 2989, 3368, 0, 3, 4, 0, -1},

		{1512, 3063, 3380, 0, 0, 1, 0, 0}
	};

	public static final void loadRegional(final Client player) {

		for (final Door door : loaded_doors) {

			if (door == null) {

				continue;
			}

			if (player.heightLevel == door.getPlane()) {

				player.getPA().checkObjectSpawn(door.getIndex(), door.getX(), door.getY(), door.getRotation(), 0);
			}
		}
	}

	public static final void loadInitial() {

		for (final int[] valid_door : DOORS_TO_OPEN) {

			if (valid_door == null) {

				continue;
			}

			/*
			 * The object index of the door.
			 */
			final int door_index = valid_door[0];

			/*
			 * The X coordinate of the door.
			 */
			final int door_x = valid_door[1];

			/*
			 * The Y coordinate of the door.
			 */
			final int door_y = valid_door[2];

			/*
			 * The plane of the door.
			 */
			final int door_z = valid_door[3];

			/*
			 * The original face of the door.
			 */
			final int original_face = valid_door[4];

			/*
			 * The modified face of the door.
			 */
			final int new_face = valid_door[5];

			/*
			 * The X coordinate modification.
			 */
			final int x_off = valid_door[6];

			/*
			 * The Y coordinate modification.
			 */
			final int y_off = valid_door[7];

			/*
			 * Removes the validated door and adds an emptied spot into the registry.
			 */
			final Door remove = new Door(-1, door_x, door_y, door_z, original_face);

			loaded_doors.add(remove);

			/*
			 * Adds the new door into the registry at the specified coordinates.
			 */
			final Door add = new Door(door_index, door_x + x_off, door_y + y_off, door_z, new_face);

			loaded_doors.add(add);

			/*
			 * Clips the modification.
			 */
			Region.addObject(add.getIndex(), add.getX(), add.getY(), add.getPlane(), 0, original_face);
		}
		
		System.out.println("Set " + DOORS_TO_OPEN.length + " doors to an open state.");
	}
}