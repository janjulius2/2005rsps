package com.runescape.world.process.task.impl;

import com.runescape.Application;
import com.runescape.world.process.event.CycleEventHandler;
import com.runescape.world.process.task.Task;

public class WorldUpdateTask extends Task {

	@Override
	protected void execute() {

		CycleEventHandler.getSingleton().process();

		Application.itemHandler.process();

		Application.shopHandler.process();

		Application.objectHandler.process();
	}
}