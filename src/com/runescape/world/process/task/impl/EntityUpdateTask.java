package com.runescape.world.process.task.impl;

import com.runescape.Application;
import com.runescape.world.process.task.Task;

public class EntityUpdateTask extends Task {

	@Override
	protected void execute() {

		Application.playerHandler.process();

		Application.npcHandler.process();
	}
}