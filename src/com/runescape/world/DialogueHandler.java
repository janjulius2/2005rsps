package com.runescape.world;

import com.runescape.model.npc.NPCHandler;
import com.runescape.model.player.Client;
import com.runescape.model.player.content.skill.Slayer;
import com.runescape.world.utilities.Utilities;

public class DialogueHandler {

	private Client c;

	public DialogueHandler(Client client) {
		this.c = client;
	}

	public void sendDialogues(int dialogue, int npcId) {
		c.talkingNpc = npcId;
		switch (dialogue) {
		
		case 0:
			c.talkingNpc = -1;
			c.getPA().removeAllWindows();
			c.nextChat = 0;
			break;
			
		case 1:
			sendStatement("You found a hidden tunnel! Do you want to enter it?");
			c.dialogueAction = 1;
			c.nextChat = 2;
			break;
			
		case 2:
			sendOption2("Yea! I'm fearless!", "No way! That looks scary!");
			c.dialogueAction = 1;
			c.nextChat = 0;
			break;
			
		case 21:
			sendNpcChat4(
					"This portal leads to a dangerous cave full of many", 
					"powerful monsters. Once inside, other players will be able", 
					"to attack you. If death should happen to find you,", 
					"your items will not be safe. Do you still wish to continue?",
					599, "Gate Keeper");
			c.nextChat = 22;
			break;
			
		case 23:
			sendOption2("Request A New Slayer Task", "Remove An Exisiting Slayer Task");
			
			c.dialogueAction = 751;
			break;
			
		case 24:
			
			sendNpcChat4("", "It appears you are still hunting @dre@" +  NPCHandler.getNpcListName(c.slayerTask).replaceAll("_", " ") + "s@bla@.", "Please return to me once your task is complete.", "", npcId, "");
			
			c.nextChat = 0;
			break;
			
			
			
		case 25:
			Slayer.assignTask(c);

			sendNpcChat4(
					"For your next Task please kill @dbl@" + c.taskAmount + " @dre@" + NPCHandler.getNpcListName(c.slayerTask).replaceAll("_", " ") + "s@bla@.", 
					"Once you have completed your task, please return to me.", 
					"@dre@" + NPCHandler.getNpcListName(c.slayerTask).replaceAll("_", " ") + "s @bla@can be located in the @dre@",
					"" + Slayer.findTaskLocation(c) + "@bla@.", npcId, "");
			
			
			
			c.nextChat = 0;
			break;
			
		case 26:
			
			sendNpcChat4("", "I have removed your current Slayer Task.", "If you would like a new one, simply speak to me again.", "", npcId, "");
			
			c.slayerTask = 0;
			
			c.taskAmount = 0;
			
			c.nextChat = 0;
			break;
			
		case 27:
			
			sendNpcChat4("You currently have @red@" + c.gambles + " @bla@available gambles. You randomly", "receive gambles for performing regular everyday tasks.", 
					"If you wish to make a donation of @red@one dollar @bla@to the", 
					"server, you will receive one gamble as a reward.", npcId, "");
			
			c.nextChat = 0;
			
			break;
			
		case 28:
			
			sendNpcChat4("", "I have taken the liberty of repairing your damaged", "barrows items.", "",  npcId, "");
			
			c.getPA().fixAllBarrows();
			
			c.nextChat = 0;
			
			break;
			
		case 77:
			sendNpcChat4(
					"", 
					"The Void Knight noticed your lack of zeal. Please", 
					"try harder in the future.", 
					"",
					c.talkingNpc, "Void Knight");
			break;
			
		case 78:
			sendNpcChat4(
					"", 
					"The Void Knight was killed, another of our Order has", 
					"fallen and the island is lost.", 
					"",
					c.talkingNpc, "Squire");
			break;
			
		case 79:
			sendNpcChat4(
					"Congratulations! You managed to destroy all the portals!", 
					"We've awarded your eight Void Knight Commendation",
					"points. Please accept these coins as a reward.",
					"",
					c.talkingNpc, "Squire");
			break;
			
		case 80:
			sendNpcChat4("You must defend the void knight while the portals are", "unsummoned. The ritual takes twenty minutes though,",
					"so you can help out by destroying them yourselves.", "Now GO GO GO!", c.talkingNpc, "Squire");
			break;
			
		case 81:
			
			sendNpcChat2(
					 
					"Hello traveler, I can offer you a wide variety of", 
					"PKing supplies. Would you care to view my stock?", 
					npcId, "Vigroy");
			
			c.nextChat = 82;
			break;
			
		case 82:
			
			sendOption2("Yes, I would like to view your store's stock.", "No thank you, I'm not interested right now.");
			
			c.dialogueAction = 90;
			break;
			
		case 83:
			
			sendPlayerChat1("No thank you, I'm not interested right now.");
			
			c.nextChat = 0;
			break;
	
		case 84:
			
			sendNpcChat2(
					 
					"Hello traveler, I can offer you a wide variety of fishing", 
					"and vanity supplies. Would you care to view my stock?", 
					npcId, "Master Fisherman");
			
			c.nextChat = 85;
			break;
			
		case 85:
			
			sendOption2("Yes, I would like to view your store's stock.", "No thank you, I'm not interested right now.");
			
			c.dialogueAction = 91;
			break;
			
		case 86:
			
			sendPlayerChat1("No thank you, I'm not interested right now.");
			
			c.nextChat = 0;
			break;
			
		case 87:

			sendNpcChat2(
					 
					"Greetings human, would you care to take a run through", 
					"the Gnome Agility course? It is quite the workout.", 
					npcId, "Gnome");
			
			c.nextChat = 88;
			break;
			
		case 88:
			
			sendOption2("Yes, I would like to run your course.", "No thank you, I'm not interested right now.");
			
			c.dialogueAction = 92;
			break;
			
		case 89:

			sendNpcChat2(
					 
					"Move faster human! I've never seen such a sorry", 
					"excuse for a warrior! Cross that log!", 
					npcId, "Gnome");
			
			c.nextChat = 0;
			break;
			
		case 90:

			sendNpcChat3(
					 
					"Hello " + Utilities.formatPlayerName(c.playerName) + ", I'm the great Wizard Mizgog!", 
					"I have the power to teleport you to many locations",
					"around 05Prime. Where would you like me to send you?", 
					
					npcId, "Wizard Mizgog");
			
			c.nextChat = 91;
			break;
			
		case 91:

			sendOption4(
					"Teleport me to the Canifis Slayer Tower.", 
					"Teleport me to the Al-Kharid Duel Arena.", 
					"Teleport me to the Varrock Sewer Dungeon.", 
					"Show me more Teleports.");
			
			c.dialogueAction = 95;
			break;
			
		case 92:

			sendOption4(
					"I would like to be teleported to the Barrows Field.", 
					"I would like to be teleported to the Castle Wars lobby.", 
					"I would like to be teleported to the Al-Kharid Duel Areana.",  
					"Show me more Teleports.");
			
			c.dialogueAction = 96;
			break;
			
		case 93:

			sendOption4(
					"I would like to be teleported to the Chaos Elemental's Castle.", 
					"I would like to be teleported to the King Black Dragon's lair.", 
					"I would like to be teleported to the Kalphite Queen's Hive.",  
					"Show me more Teleports.");
			
			c.dialogueAction = 97;
			break;
			
		}
	}

	public void sendStartInfo(String text, String text1, String text2, String text3, String title) {
		c.getPA().sendFrame126(title, 6180);
		c.getPA().sendFrame126(text, 6181);
		c.getPA().sendFrame126(text1, 6182);
		c.getPA().sendFrame126(text2, 6183);
		c.getPA().sendFrame126(text3, 6184);
		c.getPA().sendFrame164(6179);
	}

	public void sendOption2(String s, String s1) {
		c.getPA().sendFrame126("Select an Option", 2460);
		c.getPA().sendFrame126(s, 2461);
		c.getPA().sendFrame126(s1, 2462);
		c.getPA().sendFrame164(2459);
	}

	public void sendOption4(String s, String s1, String s2, String s3) {
		c.getPA().sendFrame126("Select an Option", 2481);
		c.getPA().sendFrame126(s, 2482);
		c.getPA().sendFrame126(s1, 2483);
		c.getPA().sendFrame126(s2, 2484);
		c.getPA().sendFrame126(s3, 2485);
		c.getPA().sendFrame164(2480);
	}

	public void sendOption3(String s, String s1, String s2) {
        c.getPA().sendFrame126("Select an Option", 2470);
        c.getPA().sendFrame126(s, 2471);
        c.getPA().sendFrame126(s1, 2472);
        c.getPA().sendFrame126(s2, 2473);
        c.getPA().sendFrame164(2469);
}
	
	public void sendOption5(String s, String s1, String s2, String s3, String s4) {
		c.getPA().sendFrame126("Select an Option", 2493);
		c.getPA().sendFrame126(s, 2494);
		c.getPA().sendFrame126(s1, 2495);
		c.getPA().sendFrame126(s2, 2496);
		c.getPA().sendFrame126(s3, 2497);
		c.getPA().sendFrame126(s4, 2498);
		c.getPA().sendFrame164(2492);
	}

	public void sendStatement(String s) {
		c.getPA().sendFrame126(s, 357);
		c.getPA().sendFrame126("Click here to continue", 358);
		c.getPA().sendFrame164(356);
	}

	private void sendPlayerChat1(String s) {
        c.getPA().sendFrame200(969, 591);
        c.getPA().sendFrame126(c.playerName, 970);
        c.getPA().sendFrame126(s, 971);
        c.getPA().sendFrame185(969);
        c.getPA().sendFrame164(968);
	}
	public void sendNpcChat2(String s, String s1, int ChatNpc, String name) {
		c.getPA().sendFrame200(4888, 591);
		c.getPA().sendFrame126(name, 4889);
		c.getPA().sendFrame126(s, 4890);
		c.getPA().sendFrame126(s1, 4891);
		c.getPA().sendFrame75(ChatNpc, 4888);
		c.getPA().sendFrame164(4887);
	}
	
	public void sendNpcChat3(String one, String two, String three, int npc, String name) {
		c.getPA().sendFrame200(4894, 591);
		c.getPA().sendFrame126(name, 4895);
		c.getPA().sendFrame126(one, 4896);
		c.getPA().sendFrame126(two, 4897);
		c.getPA().sendFrame126(three, 4898);
		c.getPA().sendFrame75(npc, 4894);
		c.getPA().sendFrame164(4893);
	}
	public void sendNpcChat4(String s, String s1, String s2, String s3, int ChatNpc, String name) {
		c.getPA().sendFrame200(4901, 591);
		c.getPA().sendFrame126(name, 4902);
		c.getPA().sendFrame126(s, 4903);
		c.getPA().sendFrame126(s1, 4904);
		c.getPA().sendFrame126(s2, 4905);
		c.getPA().sendFrame126(s3, 4906);
		c.getPA().sendFrame75(ChatNpc, 4901);
		c.getPA().sendFrame164(4900);
	} 
}