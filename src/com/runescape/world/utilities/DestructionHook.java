package com.runescape.world.utilities;

import com.runescape.model.player.Client;
import com.runescape.model.player.Player;
import com.runescape.model.player.PlayerAccount;
import com.runescape.world.PlayerHandler;

public final class DestructionHook extends Thread {

	public DestructionHook() {

		System.out.println("The Shut-Down hook has successfully been registered.");
	}

	@Override
	public void run() {

		for (Player player : PlayerHandler.players) {

			if (player != null) {

				PlayerAccount.saveGame((Client) player);
			}
		}
	}
}