package com.runescape;

public final class Constants {

	public static final int ADDITIONAL_WILDERNESS_LEVELS = 2;

	public static final boolean MULTI_SIGN_ENABLED = false;

	public static final int SERVER_ADDRESS = 43594;

	public static final int AUTO_SAVE_MINUTES = 5;

	public static boolean SEND_SERVER_PACKETS = true;

	public static final boolean SERVER_DEBUG = true;

	public static final int CLIENT_VERSION = 1;

	public static int MESSAGE_DELAY = 6000;

	public static final int ITEM_LIMIT = 16000; 

	public static final int MAXITEM_AMOUNT = Integer.MAX_VALUE;

	public static final int BANK_SIZE = 352;

	public static final int MAX_PLAYERS = 350;

	public static int REGION_SIZE = 0;

	public static int REGION_AMOUNT = 70;

	public static final boolean ADMIN_CAN_PVP = true; 

	public static int REGION_DECREASE = 6;

	public static int REGION_NORMALREGION = 32;

	public static final int CONNECTION_DELAY = 100; 

	public static final int IPS_ALLOWED = 2; 

	public static final boolean WORLD_LIST_FIX = true; 

	public static final int[] ITEM_SELLABLE = { 3842, 3844, 3840, 8844, 8845,
		8846, 8847, 8848, 8849, 8850, 10551, 6570, 7462, 7461, 7460, 7459,
		7458, 7457, 7456, 7455, 7454, 8839, 8840, 8842, 11663, 11664,
		11665, 10499, 9748, 9754, 9751, 9769, 9757, 9760, 9763, 9802, 9808,
		9784, 9799, 9805, 9781, 9796, 9793, 9775, 9772, 9778, 9787, 9811,
		9766, 9749, 9755, 9752, 9770, 9758, 9761, 9764, 9803, 9809, 9785,
		9800, 9806, 9782, 9797, 9794, 9776, 9773, 9779, 9788, 9812, 9767,
		9747, 9753, 9750, 9768, 9756, 9759, 9762, 9801, 9807, 9783, 9798,
		9804, 9780, 9795, 9792, 9774, 9771, 9777, 9786, 9810, 9765, 995, 9813, 9814, 7386, 7390, 7394, 7668, 7639 }; 

	public static final int[] ITEM_TRADEABLE = { 8850, 10551, 8839, 8840, 8842,
		11663, 11664, 11665, 3842, 3844, 3840, 8844, 8845, 8846, 8847,
		8848, 8849, 8850, 10551, 6570, 7462, 7461, 7460, 7459, 7458, 7457,
		7456, 7455, 7454, 8839, 8840, 8842, 11663, 11664, 11665, 10499,
		9748, 9754, 9751, 9769, 9757, 9760, 9763, 9802, 9808, 9784, 9799,
		9805, 9781, 9796, 9793, 9775, 9772, 9778, 9787, 9811, 9766, 9749,
		9755, 9752, 9770, 9758, 9761, 9764, 9803, 9809, 9785, 9800, 9806,
		9782, 9797, 9794, 9776, 9773, 9779, 9788, 9812, 9767, 9747, 9753,
		9750, 9768, 9756, 9759, 9762, 9801, 9807, 9783, 9798, 9804, 9780,
		9795, 9792, 9774, 9771, 9777, 9786, 9810, 9765, 9813, 9814, 7386, 7390, 7394, 7668, 7639 }; 

	public static final int[] UNDROPPABLE_ITEMS = {}; 

	public static final int[] FUN_WEAPONS = { 2460, 2461, 2462, 2463, 2464, 2465, 2466, 2467, 2468, 2469, 2470, 2471, 2471, 2473, 2474, 2475, 2476, 2477 };

	public static final boolean ADMIN_CAN_TRADE = true; 

	public static final boolean ADMIN_CAN_SELL_ITEMS = true; 														

	public static final boolean ADMIN_DROP_ITEMS = true; 

	public static final int START_LOCATION_X = 3085; 

	public static final int START_LOCATION_Y = 3497;

	public static final int RESPAWN_X = 3085; 

	public static final int RESPAWN_Y = 3497;

	public static final int DUELING_RESPAWN_X = 3362;

	public static final int DUELING_RESPAWN_Y = 3263;

	public static final int RANDOM_DUELING_RESPAWN = 5; 

	public static final int NO_TELEPORT_WILD_LEVEL = 20; 

	public static final int SKULL_TIMER = 1200;

	public static final int TELEBLOCK_DELAY = 20000; 

	public static final boolean SINGLE_AND_MULTI_ZONES = true; 

	public static final boolean COMBAT_LEVEL_DIFFERENCE = true; 

	public static final boolean itemRequirements = true;

	public static final int MELEE_EXP_RATE = 500;

	public static final int RANGE_EXP_RATE = 500;

	public static final int MAGIC_EXP_RATE = 500;

	public static final double SERVER_EXP_BONUS = 1;

	public static final int INCREASE_SPECIAL_AMOUNT = 17500; 

	public static final boolean PRAYER_POINTS_REQUIRED = true;

	public static final boolean PRAYER_LEVEL_REQUIRED = true;

	public static final boolean MAGIC_LEVEL_REQUIRED = true;

	public static final int GOD_SPELL_CHARGE = 300000;

	public static final boolean RUNES_REQUIRED = true; 

	public static final boolean CORRECT_ARROWS = true; 											

	public static final boolean CRYSTAL_BOW_DEGRADES = true; 

	public static final int SAVE_TIMER = 120;

	public static final int NPC_RANDOM_WALK_DISTANCE = 5; 

	public static final int NPC_FOLLOW_DISTANCE = 10; 

	public static final int[] UNDEAD_NPCS = { 90, 91, 92, 93, 94, 103, 104, 73, 74, 75, 76, 77 }; 

	public static final int EDGEVILLE_X = 3085;

	public static final int EDGEVILLE_Y = 3500;

	public static final String EDGEVILLE = "";

	public static final int AL_KHARID_X = 3293;

	public static final int AL_KHARID_Y = 3174;

	public static final String AL_KHARID = "";

	public static final int KARAMJA_X = 3087;

	public static final int KARAMJA_Y = 3500;

	public static final String KARAMJA = "";

	public static final int MAGEBANK_X = 2538;

	public static final int MAGEBANK_Y = 4716;

	public static final String MAGEBANK = "";

	public static final int VARROCK_X = 3087;

	public static final int VARROCK_Y = 3500;

	public static final String VARROCK = "";

	public static final int LUMBY_X = 3222;

	public static final int LUMBY_Y = 3218;

	public static final String LUMBY = "";

	public static final int FALADOR_X = 2964;

	public static final int FALADOR_Y = 3378;

	public static final String FALADOR = "";

	public static final int CAMELOT_X = 2757;

	public static final int CAMELOT_Y = 3477;

	public static final String CAMELOT = "";

	public static final int ARDOUGNE_X = 2662;

	public static final int ARDOUGNE_Y = 3305;

	public static final String ARDOUGNE = "";

	public static final int WATCHTOWER_X = 3087;

	public static final int WATCHTOWER_Y = 3500;

	public static final String WATCHTOWER = "";

	public static final int TROLLHEIM_X = 3243;

	public static final int TROLLHEIM_Y = 3513;

	public static final String TROLLHEIM = "";

	public static final int PADDEWWA_X = 3098;

	public static final int PADDEWWA_Y = 9884;

	public static final int SENNTISTEN_X = 3322;

	public static final int SENNTISTEN_Y = 3336;

	public static final int KHARYRLL_X = 3492;

	public static final int KHARYRLL_Y = 3471;

	public static final int LASSAR_X = 3006;

	public static final int LASSAR_Y = 3471;

	public static final int DAREEYAK_X = 3161;

	public static final int DAREEYAK_Y = 3671;

	public static final int CARRALLANGAR_X = 3156;

	public static final int CARRALLANGAR_Y = 3666;

	public static final int ANNAKARL_X = 3288;

	public static final int ANNAKARL_Y = 3886;

	public static final int GHORROCK_X = 2977;

	public static final int GHORROCK_Y = 3873;

	public static final int TIMEOUT = 20;

	public static final int CYCLE_TIME = 600;

	public static final int BUFFER_SIZE = 10000;

	public static final int WOODCUTTING_EXPERIENCE = 40;

	public static final int MINING_EXPERIENCE = 40;

	public static final int SMITHING_EXPERIENCE = 40;

	public static final int FARMING_EXPERIENCE = 40;

	public static final int FIREMAKING_EXPERIENCE = 50;

	public static final int HERBLORE_EXPERIENCE = 40;

	public static final int FISHING_EXPERIENCE = 40;

	public static final int AGILITY_EXPERIENCE = 3000;

	public static final int PRAYER_EXPERIENCE = 80;

	public static final int RUNECRAFTING_EXPERIENCE = 40;

	public static final int CRAFTING_EXPERIENCE = 40;

	public static final int THIEVING_EXPERIENCE = 40;

	public static final int SLAYER_EXPERIENCE = 50;

	public static final int COOKING_EXPERIENCE = 40;

	public static final int FLETCHING_EXPERIENCE = 40;
	
	public static final int NORMAL = 9846;
	
	public static final int ANNOYED = 9781;
	
	public static final int MYES = 9742;
	
	public static final int OMG = 9746;
	
	public static final int SIGH = 9757;
	
	public static final int CRY = 9765;
	
	public static final int HOLYSHIT = 9777;
	
	public static final int NODONTDOTHAT = 9773;
	
	public static final int ANGRY = 9785;
	
	public static final int SHAKE = 9793;
	
	public static final int MAD = 9785;
	
	public static final int EVIL = 9798;
	
	public static final int SLEEP = 9802;
	
	public static final int SLEEPY = 9803;
	
	public static final int NODUDE = 9823;
	
	public static final int QUESTIONING = 9827;
	
	public static final int DRUNK = 9835;
	
	public static final int HAHA = 9841;
	
	public static final int HAPPY = 9847;
	
	public static final int LAUGH = 9851;
	
	public static final int BORED = 9854;
	
	public static final int GRUMPY = 9863;
	
	public static final int CONTENT = 9745;
	
	public static final int REALLY_SAD = 9760;
	
	public static final int SAD = 9765;
	
	public static final int DEPRESSED = 9770;
	
	public static final int WORRIED = 9775;
	
	public static final int SCARED = 9780;
	
	public static final int MEAN_FACE = 9785;
	
	public static final int MEAN_HEAD_BANG = 9790;
	
	public static final int CALM = 9805;
	
	public static final int CALM_TALK = 9810;
	
	public static final int TOUGH = 9815;
	
	public static final int SNOBBY = 9820;
	
	public static final int CONFUSED = 9860;
	
	public static final int DRUNK_HAPPY_TIRED = 9835;
	
	public static final int TALKING_ALOT = 9845;
	
	public static final int HAPPY_TALKING = 9850;
	
	public static final int BAD_ASS = 9855;
	
	public static final int THINKING = 9860;
	
	public static final int COOL_YES = 9864;
	
	public static final int LAUGH_EXCITED = 9851;
	
	public static final int SECRELTY_TALKING = 983;
}