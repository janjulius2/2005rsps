package com.runescape.model.player.content;

import com.runescape.Application;
import com.runescape.model.npc.NPC;
import com.runescape.model.player.Client;
import com.runescape.world.process.task.Task;

public class Pets {
	
	/**
	 * The player instantiation.
	 */
	private Client player;
	
	/**
	 * The pet of the player.
	 */
	private NPC pet;
	
	/**
	 * The numerical index of the players pet.
	 */
	private int petIndex;
	
	/**
	 * If the player has dropped their pet.
	 */
	private boolean hasDroppedPet;
	
	/**
	 * The default class constructor.
	 * 
	 * @param player The instantiation of the player object.
	 */
	public Pets(Client player) {
		this.setPlayer(player);
	}

	/**
	 * Returns if the pet has been dropped.
	 * 
	 * @return The returned value.
	 */
	public boolean isHasDroppedPet() {
		return hasDroppedPet;
	}

	/**
	 * Modifies whether or not the pet has been dropped.
	 * 
	 * @param hasDroppedPet The new modification.
	 */
	public void setHasDroppedPet(boolean hasDroppedPet) {
		this.hasDroppedPet = hasDroppedPet;
	}

	/**
	 * Returns the player instance.
	 * 
	 * @return The returned instance.
	 */
	public Client getPlayer() {
		return player;
	}

	/**
	 * Modifies the player instance.
	 * 
	 * @param player The new modification.
	 */
	public void setPlayer(Client player) {
		this.player = player;
	}

	/**
	 * Returns the pet instance.
	 * 
	 * @return The returned instance.
	 */
	public NPC getPet() {
		return pet;
	}

	/**
	 * Modifies the pet instance.
	 * 
	 * @param pet The new modification.
	 */
	public void setPet(NPC pet) {
		this.pet = pet;
	}

	/**
	 * Returns the pet index.
	 * 
	 * @return The returned index.
	 */
	public int getPetIndex() {
		return petIndex;
	}

	/**
	 * Modifies the pet index.
	 * 
	 * @param petIndex The new modification.
	 */
	public void setPetIndex(int petIndex) {
		this.petIndex = petIndex;
	}
	
	/**
	 * An enumeration to store the pet data.
	 */
	public enum Pet {
		CAT_1(772, 1565);
		
		/*
		 * The index of the pet.
		 */
		private int index;
		
		/*
		 * The item correspondence.
		 */
		private int item;
		
		/*
		 * The default class constructor.
		 */
		Pet(int index, int item) {
			this.index = index;
			this.item = item;
		}

		/*
		 * Returns the numerical index of the pet.
		 */
		public int getIndex() {
			return index;
		}

		/*
		 * Returns the numerical item of the item of correspondence.
		 */
		public int getItem() {
			return item;
		}
	}
	
	/**
	 * Handles the request of dropping the pet.
	 * 
	 * @param petIndex The index of the pet to be dropped.
	 * 
	 * @param removeItem If the item should be removed.
	 */
	public void dropPet(int petIndex, boolean removeItem) {
		for (Pet pet : Pet.values()) {
			if (isHasDroppedPet()) {
				return;
			}
			if (pet.getIndex() == petIndex) {
				getPlayer().startAnimation(827);
				setHasDroppedPet(true);
				setPetIndex(petIndex);
				if (removeItem) {
					getPlayer().getItems().deleteItem(pet.getItem(), getPlayer().getItems().getItemSlot(pet.getItem()), 1);
				}
				Application.npcHandler.spawnNpc(getPlayer(), petIndex, player.absX, player.absY, player.heightLevel, 0, 0, 0, 0, 0, true, false);
			}
		}
	}
	
	/**
	 * Handles the request of picking up the pet.
	 * 
	 * @param playerPet The pet being retrieved.
	 * 
	 * @return The result of the operation.
	 */
	public boolean pickupPet(NPC playerPet) {
		if (playerPet == null || getPet() == null) {
			return false;
		}
		if (playerPet.petOwner.playerId != getPlayer().playerId && playerPet.npcType == getPet().npcType) {
			getPlayer().sendMessage("This is not your pet.");
			return false;
		}
		for (final Pet pet : Pet.values()) {
			if (!isHasDroppedPet()) {
				return false;
			}
			if (getPlayer().getItems().freeSlots() == 0) {
				getPlayer().sendMessage("You dont have the required free space to pick-up your pet.");
				return false;
			}
			if (pet.getIndex() == playerPet.npcType) {
				getPet().isDead = true;
				getPet().needRespawn = false;
				getPet().applyDead = true;
				getPlayer().startAnimation(827);
				Application.scheduler.schedule(new Task(1) {

					@Override
					protected void execute() {
						getPlayer().getItems().addItem(pet.getItem(), 1);
						setPet(null);
						setHasDroppedPet(false);
						setPetIndex(-1);
						stop();
					}
				});
			}
		}
		return true;
	}
}