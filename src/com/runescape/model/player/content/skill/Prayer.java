package com.runescape.model.player.content.skill;

import com.runescape.model.player.Client;

public final class Prayer {

	private static final int[][] BONE_COMPONENTS = {

		{526, 100}, {532, 250}, {534, 300}, {536, 700}
	};

	public static final boolean boneOnAltar(final int index, final int objectIndex, final Client player) {

		for (final int[] bone : BONE_COMPONENTS) {

			final int boneIndex = bone[0];

			final int boneExperience = bone[1];

			if (objectIndex == 409) {

				if (index == boneIndex) {

					if (System.currentTimeMillis() - player.buryDelay < 1500) {

						return false;
					} 

					player.buryDelay = System.currentTimeMillis();

					player.startAnimation(832);

					player.gfx0(520);

					player.sendMessage("The Gods are pleased with your offering of " + player.getItems().getItemName(boneIndex) + ".");

					player.getItems().deleteItem(index, 1);

					player.getPA().addSkillXP(boneExperience * 2, player.playerPrayer);
				}
			}
		}

		return true;
	}

	public static final boolean buryBone(final int index, final int slot, final Client player) {
		
		for (final int[] bone : BONE_COMPONENTS) {

			final int boneIndex = bone[0];

			final int boneExperience = bone[1];

			if (boneIndex == index) {

				if (System.currentTimeMillis() - player.buryDelay < 1600) {
					
					return false;
				} 

				player.buryDelay = System.currentTimeMillis();

				player.startAnimation(827);

				player.sendMessage("You dig a hole and bury the " + player.getItems().getItemName(boneIndex) + ".");

				player.getItems().deleteItem(index, slot, 1);

				player.getPA().addSkillXP(boneExperience, player.playerPrayer);
			}
		}
		return true;
	}
}