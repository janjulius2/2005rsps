package com.runescape.model.player.content.skill;

import com.runescape.model.object.Object;
import com.runescape.model.player.Client;
import com.runescape.world.ObjectHandler;
import com.runescape.world.utilities.Utilities;

public final class Thieving {

	private final static int EMPTY_STALL = 4797;

	private final static int RESPAWN_PULSES = 5;

	private final static int[][] THIEVING_VALUES = {
		{ 4876, 1, 995, 6000, 1000 },

		{ 4874, 40, 995, 12000, 3000 },

		{ 4875, 70, 995, 17000, 5000 },

		{ 4877, 90, 995, 27000, 15000 }
	};

	public static final boolean performTheft(final Client player, final int objectIndex, final int objectX, final int objectY) {

		if (System.currentTimeMillis() - player.thievingDelay < 3000) {

			return false;
		}

		for (final int[] values : THIEVING_VALUES) {

			final int object = values[0];

			final int level = values[1];

			final int reward = values[2];

			int rewardAmount = values[3];

			final int experience = values[4];

			if (objectIndex == object) {

				if (player.playerLevel[17] < level) {
					
					player.getDH().sendStatement("You need a thieving level of " + level + " to steal from this market stall.");
					
					return false;
				}

				final int random = Utilities.random(10);

				if (random == 5) {
					
					player.hit(5);
					
					player.sendMessage("Your hands accidently slip while stealing loot from the market stall.");
				}

				if (random == 10) {
					
					rewardAmount = rewardAmount * 2;
					
					player.sendMessage("You manage to steal double the loot from the market stall.");
				}

				player.startAnimation(832);

				player.turnPlayerTo(objectX, objectY);

				player.getItems().addItem(reward, rewardAmount);

				player.getPA().addSkillXP(experience, 17);

				player.thievingDelay = System.currentTimeMillis();

				ObjectHandler.placeObject(new Object(EMPTY_STALL, objectX, objectY, player.heightLevel, 0, 10, objectIndex, RESPAWN_PULSES));

				player.sendMessage("You successfully steal " + rewardAmount + " " + player.getItems().getItemName(reward) + " from the market stall.");
			}
		}
		return true;
	}
}