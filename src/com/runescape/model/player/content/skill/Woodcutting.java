package com.runescape.model.player.content.skill;

import com.runescape.model.player.Client;
import com.runescape.world.process.event.CycleEvent;
import com.runescape.world.process.event.CycleEventContainer;
import com.runescape.world.process.event.CycleEventHandler;
import com.runescape.world.utilities.Utilities;

public final class Woodcutting {

	public enum TreeDefinition {

		REGULAR(1276, 1, 250, 1511),
		REGULAR_TWO(1278, 1, 250, 1511),
		OAK(1281, 15, 300, 1521),
		WILLOW(1308, 30, 400, 1519),
		MAPLE(1307, 45, 600, 1517),
		YEW(1309, 60, 700, 1515),
		MAGIC(1306, 75, 900, 1513);

		private final int treeIndex;

		private final int levelRequirment;

		private final int expereinceReceived;

		private final int logReceived;

		private TreeDefinition(final int treeIndex, final int levelRequirment, final int experienceReceived, final int logReceived) {
			this.treeIndex = treeIndex;
			this.levelRequirment = levelRequirment;
			this.expereinceReceived = experienceReceived;
			this.logReceived = logReceived;
		}

		public final int getTreeIndex() {
			return treeIndex;
		}

		public final int getLevelRequirment() {
			return levelRequirment;
		}

		public final int getExpereinceReceived() {
			return expereinceReceived;
		}

		public final int getLogReceived() {
			return logReceived;
		}
	}

	public enum AxeDefinition {

		BRONZE(1351, 1, 879),
		IRON(1349, 1, 877),
		STEEL(1353, 5, 875),
		BLACK(1361, 10, 873),
		MITHIRIL(1355, 20, 871),
		ADAMANT(1357, 30, 869),
		RUNE(1359, 40, 867),
		DRAGON(6739, 50, 2846);

		private final int axeIndex;

		private final int levelRequirment;

		private final int animation;

		private AxeDefinition(final int axeIndex, final int levelRequirment, final int animation) {
			this.axeIndex = axeIndex;
			this.levelRequirment = levelRequirment;
			this.animation = animation;
		}

		public final int getAnimation() {
			return animation;
		}

		public final int getLevelRequirment() {
			return levelRequirment;
		}

		public final int getAxeIndex() {
			return axeIndex;
		}
	}

	public static final boolean hasAxe(Client player) {
		return getAxe(player) != null;
	}

	public static final AxeDefinition getAxe(Client player) {
		for (AxeDefinition axe : AxeDefinition.values()) {
			if (axe.getAxeIndex() == player.playerEquipment[player.playerWeapon] || player.getItems().playerHasItem(axe.getAxeIndex()) && player.playerLevel[player.playerWoodcutting] >= axe.getLevelRequirment()) {
				return axe;
			}
		}
		return null;
	}

	public static final void performAnimation(final Client player) {
		if (hasAxe(player)) {
			player.startAnimation(getAxe(player).getAnimation());
		}
	}

	public static final boolean chop(final Client player, final int objectIndex, final int objectX, final int objectY) {

		for (final TreeDefinition tree : TreeDefinition.values()) {

			if (tree.getTreeIndex() == objectIndex) {

				if (!hasAxe(player)) {
					player.getDH().sendStatement("You do not have an axe which you have the level requirment to use.");

					player.nextChat = 0;

					return false;
				}

				if (player.getItems().freeSlots() == 0) {
					player.getDH().sendStatement("You do not have the required inventory space to continue.");

					player.nextChat = 0;

					return false;
				}

				if (player.playerLevel[player.playerWoodcutting] < tree.getLevelRequirment()) {
					player.getDH().sendStatement("You need a Woodcutting level of @blu@" + tree.getLevelRequirment() + "@bla@ to attempt to cut this tree.");

					return false;
				}

				if (player.isWoodcutting() || player.isMining()) {

					player.sendMessage("You are already busy.");

					return false;
				}

				player.turnPlayerTo(objectX, objectY);

				player.sendMessage("You swing your axe at the tree.");

				player.setWoodcutting(true);

				performAnimation(player);

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {

						if (player.isWoodcutting() == false || player.getItems().freeSlots() == 0) {
							container.stop();
						} else {
							performAnimation(player);
						}
					}

					@Override
					public void stop() {
						player.startAnimation(65535);

					}
				}, 4);

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {

						if (player.isWoodcutting() == false || player.getItems().freeSlots() == 0) {
							container.stop();

						} else {

							player.getItems().addItem(tree.getLogReceived(), 1);

							player.getPA().addSkillXP(tree.getExpereinceReceived(), player.playerWoodcutting);

							player.sendMessage("You manage to cut some " + player.getItems().getItemName(tree.getLogReceived()) + ".");
						}
					}

					@Override
					public void stop() {

					}

				}, getWoodcuttingSpeed(player));
			}
		}
		return true;
	}

	/** 
	 * At some point in time a real formula processing in whole seconds 
	 * should be written. 
	 */
	public static final int getWoodcuttingSpeed(final Client player) {
		int dummyFormula = Utilities.random(10);

		if (dummyFormula < 5) { 
			dummyFormula = 5; 
		}

		return dummyFormula;
	}
}