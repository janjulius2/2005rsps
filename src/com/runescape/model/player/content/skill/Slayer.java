package com.runescape.model.player.content.skill;

import com.runescape.model.npc.NPCHandler;
import com.runescape.model.player.Client;
import com.runescape.world.PlayerHandler;
import com.runescape.world.utilities.Utilities;

public final class Slayer {

	private static final Object[][] SLAYER_TASKS = {
		
		{0, 2889, 2500, "Teleports Menu Inside The Magic Book", 1},
		{1, 1265, 2500, "Teleports Menu Inside The Magic Book", 1},
		{2, 1648, 4000, "Slayer Tower Teleport Inside The Magic Book", 5},
		{3, 941, 5500,  "Widlerness Dungeon Portal At Home", 10},
		{4, 1612, 5500, "Teleports Menu Inside The Magic Book", 15},
		{5, 1590, 6000, "Widlerness Dungeon Portal At Home", 20},
		{6, 1592, 8000, "Widlerness Dungeon Portal At Home", 30},
		{7, 1643, 9000, "Slayer Tower Teleport Inside The Magic Book", 45},
		{8, 1618, 9500, "Slayer Tower Teleport Inside The Magic Book", 50},
		{9, 1624, 9750, "Slayer Tower Teleport Inside The Magic Book", 65},
		{10, 1610, 10000, "Slayer Tower Teleport Inside The Magic Book", 75},
		{11, 1613, 15000, "Slayer Tower Teleport Inside The Magic Book", 80},
		{12, 1615, 25000, "Slayer Tower Teleport Inside The Magic Book", 85},
		{13, 2783, 30000, "Widlerness Dungeon Portal At Home", 90}
	};

	public static final String findTaskLocation(Client player) {
		
		for (final Object[] configurations : SLAYER_TASKS) {

			if ((Integer) configurations[1] == player.slayerTask) {
				
				return (String) configurations[3];
			}
		}
		return "Unable To Locate Task Location";
	}

	public static final void handleSlayerTaskDeath(int accumulator) {

		if (NPCHandler.npcs[accumulator].npcType == 772) {
			
			return;
		}
		Client player = (Client) PlayerHandler.players[NPCHandler.npcs[accumulator].killedBy];

		if (NPCHandler.npcs[accumulator].npcType == player.slayerTask) {
			
			for (final Object[] configurations : SLAYER_TASKS) {
				
				if ( (Integer) configurations[1]  == player.slayerTask) {

					player.taskAmount --;
					player.getPA().addSkillXP( (Integer) configurations[2], player.playerSlayer);

					if (player.taskAmount == 0) {
						player.getPA().addSkillXP( (Integer) configurations[2] * 5, player.playerSlayer);
						player.sendMessage("You have completed your current Slayer Task. Please return to Vannaka to receive a new one.");
						player.slayerTask = 0;
						player.taskAmount = 0;
					}
				}
			}
		}
	}

	public static final void assignTask(Client player) {
		
		int random = Utilities.random(SLAYER_TASKS.length - 1);

		for (final Object[] configurations : SLAYER_TASKS) {

			if (random == (Integer) configurations[0]) {

				if (player.playerLevel[player.playerSlayer] >= (Integer) configurations[4]) {
					player.slayerTask = (Integer) configurations[1];
					player.taskAmount = (10 + Utilities.random(20));
				} else {
					assignTask(player);
				}
			}
		}
	}
}