package com.runescape.model.player.content.skill;

import com.runescape.Application;
import com.runescape.model.object.Object;
import com.runescape.model.player.Client;
import com.runescape.world.ObjectHandler;
import com.runescape.world.cache.region.Region;
import com.runescape.world.process.event.CycleEvent;
import com.runescape.world.process.event.CycleEventContainer;
import com.runescape.world.process.event.CycleEventHandler;

public final class Firemaking {

	public enum FiremkaingData {
		NORMAL(1511, 1000, 1, 2732),
		OAK(1521, 2000, 15, 2732),
		WILLOW(1519, 3000, 30, 2732),
		MAPLE(1517, 4000, 45, 2732),
		YEW(1515, 5000, 60, 2732),
		MAGIC(1513, 7500, 75, 2732);

		private final int itemIndex;

		private final int experience;

		private final int level;

		private final int objectIndex;

		private FiremkaingData(int itemIndex, int experience, int level, int objectIndex) {
			this.itemIndex = itemIndex;
			this.experience = experience;
			this.level = level;
			this.objectIndex = objectIndex;
		}
	}

	public static final FiremkaingData forLog(int logIndex) {

		for (FiremkaingData fire : FiremkaingData.values()) {

			if (fire.itemIndex == logIndex) {

				return fire;
			}
		}

		return null;
	}

	public static final boolean isLog(int id) {

		return forLog(id) != null;
	}

	public static final void createFire(final Client player, final int logIndex, final int fromSlot) {

		final FiremkaingData firemkaing = forLog(logIndex);

		if (firemkaing != null) {

			if (System.currentTimeMillis() - player.lastLight > 2000) {

				if (player.playerLevel[player.playerFiremaking] >= firemkaing.level) {

					if (player.getItems().playerHasItem(590) && player.getItems().playerHasItem(firemkaing.itemIndex)) {

						if (Application.objectHandler.getObject(player.absX, player.absY, player.heightLevel) != null) {

							player.sendMessage("You can't ignite a fire here.");

							return;
						}

						if (player.isWoodcutting()) {
							
							player.setWoodcutting(false);
						}
						
						if (player.arenas() || player.isBanking || player.inBarrowsWaves() || player.inTrade) {

							player.sendMessage("You can't ignite a fire right now.");

							return;
						}

						player.lastLight = System.currentTimeMillis();

						player.isFiremkaing = true;

						final int positionX = player.getX();

						final int positionY = player.getY();

						player.resetWalkingQueue();

						player.getPA().resetFollow();

						Application.itemHandler.createGroundItem(player, 1511, positionX, positionY, 1, player.playerId);

						player.getItems().deleteItem(firemkaing.itemIndex, fromSlot, 1);

						player.startAnimation(733);

						CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

							@Override
							public void execute(CycleEventContainer container) {

								final Object fire = new Object(firemkaing.objectIndex, positionX, positionY, 0, 0, 10, -1, 100);

								ObjectHandler.placeObject(fire);
			
								player.getPA().addSkillXP(firemkaing.experience, player.playerFiremaking);

								Application.itemHandler.removeGroundItem(player, 1511, positionX, positionY, false);

								container.stop();
							}

							@Override
							public void stop() {

								player.isFiremkaing = false;

								player.startAnimation(65535);
							}
						}, 3);

						if (Region.getClipping(player.getX() - 1, player.getY(), player.heightLevel, -1, 0)) {

							player.getPA().walkTo(-1, 0);

						} else if (Region.getClipping(player.getX() + 1, player.getY(), player.heightLevel, 1, 0)) {

							player.getPA().walkTo(1, 0);

						} else if (Region.getClipping(player.getX(), player.getY() - 1, player.heightLevel, 0, -1)) {

							player.getPA().walkTo(0, -1);

						} else if (Region.getClipping(player.getX(), player.getY() + 1, player.heightLevel, 0, 1)) {

							player.getPA().walkTo(0, 1);
						}

						player.turnPlayerTo(player.getX() + 1, player.getY());

					}
				} else {

					player.getDH().sendStatement("You need a Firemkaing level of @blu@" + firemkaing.level + " @bla@to light these logs.");
					
					player.nextChat = 0;
				}
			}
		}
	}
}