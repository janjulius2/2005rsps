package com.runescape.model.player.content.skill;

import com.runescape.Constants;
import com.runescape.model.player.Client;
import com.runescape.world.process.event.CycleEvent;
import com.runescape.world.process.event.CycleEventContainer;
import com.runescape.world.process.event.CycleEventHandler;

public final class Agility {

	public static void agilityWalk(final Client player, final int walkAnimation, final int x, final int y) {

		player.isRunning2 = false;

		player.getPA().sendFrame36(173, 0);

		player.playerWalkIndex = walkAnimation;

		player.getPA().requestUpdates();
		player.getPA().walkTo(x ,y);
	}

	public static void resetAgilityWalk(final Client player) {

		player.isRunning2 = true;
		player.getPA().sendFrame36(173, 1);
		player.getCombat().getPlayerAnimIndex(player.getItems().getItemName(player.playerEquipment[player.playerWeapon]).toLowerCase());
		player.getPA().requestUpdates();
	}

	public static final int[] AGILITY_OBSTACLES = {
		2295, 2285, 2286, 2313, 2312, 2314, 2315, 154, 4058		
	};

	public static boolean agilityObstacle(final Client player, final int objectType) {

		for (int i = 0; i < AGILITY_OBSTACLES.length; i++) {

			if (objectType == AGILITY_OBSTACLES[i]) {

				return true;
			}
		}

		return false;		
	}

	public static void agilityCourse(final Client player, final int objectType) {

		if (player.isFiremkaing) {
			
			return;
		}
		switch (objectType) {

		case 2295:

			if (player.absX == 2474 && player.absY == 3436) {

				player.doingAgility = true;

				agilityWalk(player, 762, 0, -7);

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {

						if (player.absX == 2474 && player.absY == 3429) {

							container.stop();
						}
					}
					@Override
					public void stop() {

						resetAgilityWalk(player);

						player.getPA().addSkillXP((int) 7.5 * Constants.AGILITY_EXPERIENCE, player.playerAgility);

						player.logBalance = true;
						player.doingAgility = false;
					}
				}, 1);
			}
			break;

		case 2285:

			player.startAnimation(828);

			CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {

					container.stop();
				}
				@Override
				public void stop() {

					player.getPA().movePlayer(player.absX, 3424, 1);
					player.getPA().addSkillXP((int) 7.5 * Constants.AGILITY_EXPERIENCE, player.playerAgility);
					player.obstacleNetUp = true;
				}
			}, 2);
			break;

		case 2313:

			player.startAnimation(828);

			CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {
				@Override
				public void execute(CycleEventContainer container) {

					container.stop();
				}
				@Override
				public void stop() {

					player.getPA().movePlayer(2473, 3420, 2);
					player.getPA().addSkillXP((int) 5 * Constants.AGILITY_EXPERIENCE, player.playerAgility);
					player.treeBranchUp = true;
				}
			}, 2);
			break;

		case 2312:

			if (player.absX == 2477 && player.absY == 3420) {

				player.doingAgility = true;

				agilityWalk(player, 762, 6, 0);

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {

						if (player.absX == 2483 && player.absY == 3420) {

							container.stop();
						}
					}

					@Override
					public void stop() {

						resetAgilityWalk(player);

						player.getPA().addSkillXP((int) 7 * Constants.AGILITY_EXPERIENCE, player.playerAgility);

						player.balanceRope = true;
						player.doingAgility = false;
					}
				}, 1);
			}
			break;

		case 2314:
		case 2315:

			player.startAnimation(828);

			CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {

					container.stop();
				}

				@Override
				public void stop() {

					player.getPA().movePlayer(player.absX, player.absY, 0);
					player.getPA().addSkillXP((int) 5 * Constants.AGILITY_EXPERIENCE, player.playerAgility);
					player.treeBranchDown = true;
				}
			}, 2);

			break;
		case 2286:

			player.doingAgility = true;

			player.startAnimation(828);

			CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {

					player.getPA().movePlayer(player.absX, 3427, 0);

					container.stop();
				}

				@Override
				public void stop() {

					player.turnPlayerTo(player.absX, 3426);

					player.getPA().addSkillXP((int) 8 * Constants.AGILITY_EXPERIENCE, player.playerAgility);
					player.obstacleNetOver = true;
					player.doingAgility = false;
				}
			}, 1);
			break;

		case 154:
		case 4058:

			if (player.absX == 2484 && player.absY == 3430 || player.absX == 2487 && player.absY == 3430) {

				player.doingAgility = true;

				agilityWalk(player, 749, 0, 7);

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {

						if (player.absY == 3437) {

							container.stop();
						}
					}

					@Override
					public void stop() {

						player.startAnimation(748);

						resetAgilityWalk(player);

						if (player.logBalance && player.obstacleNetUp && player.treeBranchUp && player.balanceRope && player.treeBranchDown && player.obstacleNetOver) {

							player.getPA().addSkillXP((int) 47 * Constants.AGILITY_EXPERIENCE, player.playerAgility);

							player.sendMessage("You have completed the full gnome agility course.");

						} else {

							player.getPA().addSkillXP((int) 7 * Constants.AGILITY_EXPERIENCE, player.playerAgility);
						}

						player.logBalance = false;
						player.obstacleNetUp = false;
						player.treeBranchUp = false;
						player.balanceRope = false;
						player.treeBranchDown = false;
						player.obstacleNetOver = false;
						player.doingAgility = false;
					}
				}, 1);
			}
			break;
		}
	}
}