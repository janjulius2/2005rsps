package com.runescape.model.player.content.skill;

import com.runescape.model.player.Client;
import com.runescape.world.process.event.CycleEvent;
import com.runescape.world.process.event.CycleEventContainer;
import com.runescape.world.process.event.CycleEventHandler;
import com.runescape.world.utilities.Utilities;

public final class Mining {

	public enum RockDefinition {

		COPPER(2090, 1, 2500, 436),
		TIN(2095, 1, 2500, 438),
		TIN_TWO(2094, 1, 2500, 438),
		IRON(2093, 15, 3850, 440),
		IRON_TWO(2092, 15, 3850, 440),
		COAL(2097, 30, 5000, 453),
		COAL_TWO(2096, 30, 5000, 453),
		MITHIRIL(2103, 55, 7000, 447),
		ADAMANT(2105, 70, 9200, 449),
		RUNE(2107, 85, 10000, 451);

		private final int rockIndex;

		private final int levelRequirment;

		private final int expereinceReceived;

		private final int oreReceived;

		private RockDefinition(final int rockIndex, final int levelRequirment, final int experienceReceived, final int oreReceived) {
			this.rockIndex = rockIndex;
			this.levelRequirment = levelRequirment;
			this.expereinceReceived = experienceReceived;
			this.oreReceived = oreReceived;
		}

		public final int getRockIndex() {
			return rockIndex;
		}

		public final int getLevelRequirment() {
			return levelRequirment;
		}

		public final int getExpereinceReceived() {
			return expereinceReceived;
		}

		public final int getOreReceived() {
			return oreReceived;
		}
	}

	public enum PickAxeDefinition {

		BRONZE(1265, 1, 625),
		IRON(1267, 1,  626),
		STEEL(1269, 5, 627),
		MITHIRIL(1273, 20, 629),
		ADAMANT(1271, 30, 628),
		RUNE(1275, 40, 630);

		private final int axeIndex;

		private final int levelRequirment;

		private final int animation;

		private PickAxeDefinition(final int axeIndex, final int levelRequirment, final int animation) {
			this.axeIndex = axeIndex;
			this.levelRequirment = levelRequirment;
			this.animation = animation;
		}

		public final int getAnimation() {
			return animation;
		}

		public final int getLevelRequirment() {
			return levelRequirment;
		}

		public final int getAxeIndex() {
			return axeIndex;
		}
	}

	public static final boolean hasPickAxe(Client player) {
		return getPickAxe(player) != null;
	}

	public static final PickAxeDefinition getPickAxe(Client player) {
		for (PickAxeDefinition axe : PickAxeDefinition.values()) {
			if (axe.getAxeIndex() == player.playerEquipment[player.playerWeapon] || player.getItems().playerHasItem(axe.getAxeIndex()) && player.playerLevel[player.playerMining] >= axe.getLevelRequirment()) {
				return axe;
			}
		}
		return null;
	}

	public static final void performAnimation(final Client player) {
		if (hasPickAxe(player)) {
			player.startAnimation(getPickAxe(player).getAnimation());
		}
	}

	public static final boolean mine(final Client player, int objectIndex, int objectX, int objectY) {

		for (final RockDefinition rock : RockDefinition.values()) {

			if (rock.getRockIndex() == objectIndex) {

				if (!hasPickAxe(player)) {
					player.getDH().sendStatement("You do not have a pick which you have the level requirment to use.");

					player.nextChat = 0;

					return false;
				}

				if (player.getItems().freeSlots() == 0) {
					player.getDH().sendStatement("You do not have the required inventory space to continue.");

					player.nextChat = 0;

					return false;
				}

				if (player.playerLevel[player.playerMining] < rock.getLevelRequirment()) {
					player.getDH().sendStatement("You need a Mining level of @blu@" + rock.getLevelRequirment() + "@bla@ to attempt to mine this rock.");

					player.nextChat = 0;

					return false;
				}

				if (player.isWoodcutting() || player.isMining()) {

					player.sendMessage("You are already busy.");
					return false;
				}

				player.setMining(true);

				player.turnPlayerTo(objectX, objectY);

				player.sendMessage("You swing your pick-axe at the rock.");

				performAnimation(player);

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						if (player.isMining() == false || player.getItems().freeSlots() == 0) {
							container.stop();
						} else {
							performAnimation(player);
						}
					}

					@Override
					public void stop() {
						player.startAnimation(65535);
					}
				}, 4);

				CycleEventHandler.getSingleton().addEvent(player, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {

						if (player.isMining() == false || player.getItems().freeSlots() == 0) {
							container.stop();

						} else {

							player.getItems().addItem(rock.getOreReceived(), 1);

							player.getPA().addSkillXP(rock.getExpereinceReceived(), player.playerMining);

							player.sendMessage("You manage to mine some " + player.getItems().getItemName(rock.getOreReceived()) + ".");

							container.stop();

							player.resetSkillingActions();
						}
					}

					@Override
					public void stop() {

					}
				}, getMiningSpeed(player));
			}
		}
		return true;
	}

	/** 
	 * At some point in time a real formula processing in whole seconds 
	 * should be written. 
	 */
	public static final int getMiningSpeed(final Client player) {
		int dummyFormula = Utilities.random(10);

		if (dummyFormula < 5) { 
			dummyFormula = 5; 
		}

		return dummyFormula;
	}
}