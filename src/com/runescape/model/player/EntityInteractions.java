package com.runescape.model.player;

import com.runescape.Application;
import com.runescape.Constants;
import com.runescape.model.npc.NPC;
import com.runescape.model.npc.NPCHandler;
import com.runescape.model.player.content.TanHide;
import com.runescape.model.player.content.skill.Agility;
import com.runescape.model.player.content.skill.Mining;
import com.runescape.model.player.content.skill.Thieving;
import com.runescape.model.player.content.skill.Woodcutting;
import com.runescape.world.cache.region.Region;

public class EntityInteractions {

	private Client player;

	public EntityInteractions(Client player) {
		
		this.player = player;
	}

	public void firstClickObject(int objectType, int obX, int obY) {
		
		if (player.playerRights > 1) {
			
			player.sendMessage("Index: " + objectType + " X: " + obX + " Y: " + obY);
		}

		player.clickObjectType = 0;

		if (player.isFiremkaing) {
			
			return;
		}
		
		if (Region.objectExists(objectType, obX, obY, player.heightLevel) == false && objectType != 2213) {

			return;
		}

		if (Agility.agilityObstacle(player, objectType)) {

			if (System.currentTimeMillis() - player.agilityDelay < 2000) {
				
				return;
			}

			Agility.agilityCourse(player, objectType);
			
			player.agilityDelay = System.currentTimeMillis();
		}
		
		if (Thieving.performTheft(player, objectType, obX, obY)) {
			
		}
		
		if (Woodcutting.chop(player, objectType, obX, obY)) {
			
		}
		
		if (Mining.mine(player, objectType, obX, obY)) {
			
		}

		player.turnPlayerTo(obX, obY);
		
		switch (objectType) {
			
		case 1530:
		case 1512:
			player.sendMessage("This door appears to be stuck.");
			break;
			
		case 13619:
			player.getPA().movePlayer(3088, 3495, 0);
			break;
			
		case 10:
			player.getDH().sendDialogues(29, 2024);
			break;

		case 10251:
			player.getDH().sendDialogues(21, 599);
			break;
			
		case 1765:
			player.getPA().movePlayer(2271, 4680, 0);
			break;
			
		case 272:
			player.getPA().movePlayer(player.absX, player.absY, 1);
			break;

		case 273:
			player.getPA().movePlayer(player.absX, player.absY, 0);
			break;
			
		case 410:
			if (player.playerMagicBook == 0) {
				player.playerMagicBook = 2;
				player.setSidebarInterface(6, 29999);
				player.sendMessage("A Lunar wisdomin fills your mind.");
				player.getPA().resetAutocast();
			} else {
				player.setSidebarInterface(6, 1151); 
				player.playerMagicBook = 0;
				player.sendMessage("You feel a drain on your memory.");
				player.autocastId = -1;
				player.getPA().resetAutocast();
			}
			break;
			
		case 6552:
			if (player.playerLevel[player.playerMagic] < 50) {
				
				player.getDH().sendStatement("A @blu@Magic @bla@level of @blu@50 @bla@is required to pay respects at this altar.");
				player.nextChat = -1;
				return;
			}
			if (player.playerMagicBook == 0) {
				player.playerMagicBook = 1;
				player.startAnimation(645);
				player.setSidebarInterface(6, 12855);
				player.getDH().sendStatement("An ancient wisdom fills your mind.");
				player.nextChat = -1;
				player.getPA().resetAutocast();
			} else {
				player.setSidebarInterface(6, 1151); 
				player.startAnimation(645);
				player.playerMagicBook = 0;
				player.getDH().sendStatement("You feel a sudden drain of your memory.");
				player.nextChat = -1;
				player.autocastId = -1;
				player.getPA().resetAutocast();
			}
			break;

		case 1816:
			player.getPA().startTeleport2(2271, 4680, 0);
			break;
			
		case 1817:
			player.getPA().startTeleport(3067, 10253, 0, "modern");
			break;
			
		case 1814:
			player.getPA().startTeleport(3153, 3923, 0, "modern");
			break;
			
		case 1733:
			player.getPA().movePlayer(player.absX, player.absY + 6393, 0);
			break;

		case 1734:
			player.getPA().movePlayer(player.absX, player.absY - 6396, 0);
			break;

		case 2213:
		case 14367:
		case 11758:
		case 3193:
			if (player.inBank()) {
				
				player.getPA().openUpBank();
			}
			break;

		case 10177:
			player.getPA().movePlayer(1890, 4407, 0);
			break;
			
		case 10230:
			player.getPA().movePlayer(2900, 4449, 0);
			break;
			
		case 10229:
			player.getPA().movePlayer(1912, 4367, 0);
			break;

		case 14829:
		case 14830:
		case 14827:
		case 14828:
		case 14826:
		case 14831:
			Application.objectHandler.startObelisk(objectType);
			break;

		case 409:
			if (player.playerLevel[5] < player.getPA().getLevelForXP(player.playerXP[5])) {
				player.startAnimation(645);
				player.playerLevel[5] = player.getPA().getLevelForXP(player.playerXP[5]);
				player.sendMessage("Saradomin shines down upon you and recharges your Prayer points.");
				player.getPA().refreshSkill(5);
			} else {
				player.getDH().sendStatement("Your Prayer points are already full.");
			}
			break;

		case 2879:
			player.getPA().movePlayer(2538, 4716, 0);
			break;
			
		case 2878:
			player.getPA().movePlayer(2509, 4689, 0);
			break;
			
		case 5960:
			player.getPA().startTeleport2(3090, 3956, 0);
			break;

		case 1815:
			player.getPA().startTeleport2(Constants.EDGEVILLE_X, Constants.EDGEVILLE_Y, 0);
			break;

		case 9706:
			player.getPA().startTeleport2(3105, 3951, 0);
			break;
			
		case 9707:
			player.getPA().startTeleport2(3105, 3956, 0);
			break;

		case 5959:
			player.getPA().startTeleport2(2539, 4712, 0);
			break;
		}
	}

	public void secondClickObject(int objectType, int obX, int obY) {
		player.clickObjectType = 0;
		if (Region.objectExists(objectType, obX, obY, player.heightLevel) == false && objectType != 2213) {

			return;
		}
		if (Thieving.performTheft(player, objectType, obX, obY)) {
			
		}
		
		switch (objectType) {
			
		case 2213:
		case 14367:
		case 11758:
			if (player.inBank()) {
				
				player.getPA().openUpBank();
			}
			break;
		}
	}

	public void thirdClickObject(int objectType, int obX, int obY) {
		player.clickObjectType = 0;

		switch (objectType) {

		}
	}

	public void firstClickNpc(int npcType) {
		NPC npc = NPCHandler.npcs[player.npcClickIndex];
		if (player.isFiremkaing) {
			
			return;
		}
		if (player.getPets().pickupPet(npc)) {
			
		}
		player.clickNpcType = 0;
		player.npcClickIndex = 0;
		switch (npcType) {
			
		case 804:
			
			TanHide.sendTanningInterface(player);
			break;
			
		case 706:
			player.getDH().sendDialogues(90, npcType);
			break;
			
		case 511:
			player.getDH().sendDialogues(81, npcType);
			break;
			
		case 484:
			player.getDH().sendDialogues(89, npcType);
			break;
			
		case 162:
			player.getDH().sendDialogues(87, npcType);
			break;
			
		case 308:
			player.getDH().sendDialogues(84, npcType);
			break;
			
		case 2000:
			player.getShops().openShop(14);
			break;
			
		case 36:
			player.getShops().openShop(9);
			break;
			
		case 2024:
			player.getDH().sendDialogues(28, npcType);
			break;
			
		case 945:
			player.getDH().sendDialogues(27, npcType);
			break;
			
		case 1597:
			player.getDH().sendDialogues(23, npcType);
			break;

		case 2258:
			player.getDH().sendDialogues(17, npcType);
			break;
			
		case 1599:
			if (player.slayerTask <= 0) {
				player.getDH().sendDialogues(11, npcType);
			} else {
				player.getDH().sendDialogues(13, npcType);
			}
			break;
			
		case 549:
			player.getShops().openShop(25);
			break;
			
		case 1835:
			player.getShops().openShop(46);
			break;
			
		case 919:
			player.getShops().openShop(10);
			break;
			
		case 542:
			player.getShops().openShop(9);
			break;
			
		case 1208:
			player.getShops().openShop(5);
			break;

		case 461:
			player.getShops().openShop(2);
			break;

		case 683:
			player.getShops().openShop(3);
			break;
			
		case 2538:
			player.getShops().openShop(6);
			break;
			
		case 1209:
			player.getShops().openShop(4);
			break;

		case 57:
			player.getShops().openShop(11);
			break;
			
		case 519:
			player.getShops().openShop(8);
			break;
			
		case 1282:
			player.getShops().openShop(7);
			break;
			
		case 1152:
			player.getDH().sendDialogues(16, npcType);
			break;
			
		case 494:
			player.getPA().openUpBank();
			break;
			
		case 2566:
			player.getShops().openSkillCape();
			break;
			
		case 3788:
			player.sendMessage("You currently have " + player.pcPoints + " Pest Control Points.");
			player.getShops().openShop(13);
			break;
			
		case 905:
			player.getDH().sendDialogues(5, npcType);
			break;
			
		case 460:
			player.getDH().sendDialogues(3, npcType);
			break;
			
		case 462:
			player.getDH().sendDialogues(7, npcType);
			break;

		case 522:
		case 523:
			player.getShops().openShop(1);
			break;
			
		case 946:
			player.getShops().openShop(5);
			break;
			
		case 550:
			player.getShops().openShop(6);
			break;
			
		case 599:
			player.getPA().showInterface(3559);
			player.canChangeAppearance = true;
			break;
			
		case 904:
			player.sendMessage("You have " + player.magePoints + " points.");
			break;
			
		default:
			player.getDH().sendDialogues(20, npcType);
			break;
		}
	}

	public void secondClickNpc(int npcType) {
		player.clickNpcType = 0;
		player.npcClickIndex = 0;
		if (player.isFiremkaing) {
			
			return;
		}
		switch (npcType) {
		
		case 804:
			
			TanHide.sendTanningInterface(player);
			break;
		case 2000:
			player.getShops().openShop(14);
			break;
			
		case 520:
			player.getShops().openShop(1);
			break;
			
			case 521:
			player.getShops().openShop(1);
			break;
		
		case 1282:
			player.getShops().openShop(10);
			break;
			
		case 494:
			player.getPA().openUpBank();
			break;

		case 904:
			player.getShops().openShop(17);
			break;
			
		case 522:
		case 523:
			player.getShops().openShop(1);
			break;
			
		case 541:
			player.getShops().openShop(2);
			break;

		case 461:
			player.getShops().openShop(5);
			break;
			
		case 946:
			player.getShops().openShop(5);
			break;

		case 683:
			player.getShops().openShop(6);
			break;
			
		case 550:
			player.getShops().openShop(6);
			break;
			
		case 2538:
			player.getShops().openShop(7);
			break;

		case 519:
			player.getShops().openShop(8);
			break;
		}
	}

	public void thirdClickNpc(int npcType) {
		player.clickNpcType = 0;
		player.npcClickIndex = 0;
		switch (npcType) {

		}
	}
}