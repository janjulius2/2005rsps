package com.runescape.model.player;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Future;

import org.apache.mina.common.IoSession;

import com.runescape.Application;
import com.runescape.Constants;
import com.runescape.model.item.ItemAssistant;
import com.runescape.model.item.ItemClassification;
import com.runescape.model.npc.NPCHandler;
import com.runescape.model.player.content.Combat;
import com.runescape.model.player.content.Edibles;
import com.runescape.model.player.content.Pets;
import com.runescape.model.player.content.PotionCombination;
import com.runescape.model.player.content.PotionConsumption;
import com.runescape.model.player.content.TradingAndDueling;
import com.runescape.model.shop.ShopAssistant;
import com.runescape.network.HostList;
import com.runescape.network.Packet;
import com.runescape.network.StaticPacketBuilder;
import com.runescape.network.packet.PacketRegistration;
import com.runescape.world.DialogueHandler;
import com.runescape.world.PlayerHandler;
import com.runescape.world.process.event.CycleEvent;
import com.runescape.world.process.event.CycleEventContainer;
import com.runescape.world.process.event.CycleEventHandler;
import com.runescape.world.utilities.Stream;
import com.runescape.world.utilities.Utilities;

public class Client extends Player {

	public byte buffer[] = null;

	public Stream inStream = null, outStream = null;

	private IoSession session;

	private ItemAssistant itemAssistant = new ItemAssistant(this);

	private ShopAssistant shopAssistant = new ShopAssistant(this);

	private TradingAndDueling tradeAndDuel = new TradingAndDueling(this);

	private PlayerAssistant playerAssistant = new PlayerAssistant(this);

	private Combat combatAssistant = new Combat(this);

	private EntityInteractions actionHandler = new EntityInteractions(this);

	private DialogueHandler dialogueHandler = new DialogueHandler(this);

	private Queue<Packet> queuedPackets = new LinkedList<Packet>();

	private Pets pets = new Pets(this);

	private PotionConsumption potions = new PotionConsumption(this);

	private PotionCombination potionMixing = new PotionCombination(this);

	private Edibles food = new Edibles(this);

	public int lowMemoryVersion = 0;

	public int timeOutCounter = 0;

	public int returnCode = 2;

	private Future<?> currentTask;

	public Client(IoSession s, int playerId) {
		super(playerId);
		this.session = s;
		synchronized (this) {
			outStream = new Stream(new byte[Constants.BUFFER_SIZE]);
			outStream.currentOffset = 0;
			inStream = new Stream(new byte[Constants.BUFFER_SIZE]);
			inStream.currentOffset = 0;
			buffer = new byte[Constants.BUFFER_SIZE];
		}
	}

	public void resetSkillingActions() {
		setWoodcutting(false);
		setMining(false);
	}

	public void flushOutStream() {
		if (disconnected || outStream.currentOffset == 0) {
			return;
		}
		synchronized (this) {
			StaticPacketBuilder out = new StaticPacketBuilder().setBare(true);
			byte[] temp = new byte[outStream.currentOffset];
			System.arraycopy(outStream.buffer, 0, temp, 0, temp.length);
			out.addBytes(temp);
			session.write(out.toPacket());
			outStream.currentOffset = 0;
		}
	}

	public void sendClan(String name, String message, String clan, int rights) {
		outStream.createFrameVarSizeWord(217);
		outStream.writeString(name);
		outStream.writeString(message);
		outStream.writeString(clan);
		outStream.writeWord(rights);
		outStream.endFrameVarSize();
	}

	public static final int PACKET_SIZES[] = { 0, 0, 0, 1, -1, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 8, 0, 6, 2, 2, 0, 0, 2, 0, 6, 0, 12, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 8, 4, 0, 0, 2, 2, 6, 0, 6, 0, -1, 0, 0, 0, 0, 0, 0, 0, 12,
			0, 0, 0, 8, 8, 12, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 2, 2, 8, 6,
			0, -1, 0, 6, 0, 0, 0, 0, 0, 1, 4, 6, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0,
			-1, 0, 0, 13, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0,
			0, 1, 0, 6, 0, 0, 0, -1, 0, 2, 6, 0, 4, 6, 8, 0, 6, 0, 0, 0, 2, 0,
			0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 1, 2, 0, 2, 6, 0, 0, 0, 0, 0, 0,
			0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 3, 0,
			2, 0, 0, 8, 1, 0, 0, 12, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0,
			0, 4, 0, 4, 0, 0, 0, 7, 8, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, -1, 0, 6,
			0, 1, 0, 0, 0, 6, 0, 6, 8, 1, 0, 0, 4, 0, 0, 0, 0, -1, 0, -1, 4, 0,
			0, 6, 6, 0, 0, 0 };

	@Override
	public void destruct() {
		if (session == null) {
			return;
		}
		if(disconnected == true){
		     getTradeAndDuel().declineTrade();
	 	}
		if (duelStatus >= 1 && duelStatus <= 5) {
			getTradeAndDuel().bothDeclineDuel();
			saveCharacter = true;
			return;
		}

		CycleEventHandler.getSingleton().stopEvents(this);
		PlayerAccount.saveGame(this);
		Utilities.println("The account of the user " + Utilities.formatPlayerName(playerName) + " has been removed.");
		HostList.getHostList().remove(session);
		disconnected = true;
		session.close();
		session = null;
		inStream = null;
		outStream = null;
		isActive = false;
		buffer = null;
		super.destruct();
	}

	public void sendMessage(String s) {
		if (getOutStream() != null) {
			outStream.createFrameVarSize(253);
			outStream.writeString(s);
			outStream.endFrameVarSize();
		}
	}

	public void setSidebarInterface(int menuId, int form) {
		if (getOutStream() != null) {
			outStream.createFrame(71);
			outStream.writeWord(form);
			outStream.writeByteA(menuId);
		}
	}
	
	public void setText() {
		
		getPA().sendFrame126("@or2@05Prime@yel@, always use the", 2451);
		
		getPA().sendFrame126("The Bank Of 05Prime", 5383);
		
		getPA().sendFrame126("@gre@Home Teleport", 1300);
		
		getPA().sendFrame126("Teleports you to Edgeville", 1301);
		
		getPA().sendFrame126("@gre@Home Teleport", 13037);
		
		getPA().sendFrame126("100%", 149);
	}
	
	@Override
	public void initialize() {
		outStream.createFrame(249);
		outStream.writeByteA(1);
		outStream.writeWordBigEndianA(playerId);
		for (int j = 0; j < PlayerHandler.players.length; j++) {
			if (j == playerId) {
				continue;
			}
			if (PlayerHandler.players[j] != null) {
				if (PlayerHandler.players[j].playerName
						.equalsIgnoreCase(playerName))
					disconnected = true;
			}
		}
		for (int i = 0; i < 25; i++) {
			getPA().setSkillLevel(i, playerLevel[i], playerXP[i]);
			getPA().refreshSkill(i);
		}
		for (int p = 0; p < PRAYER.length; p++) {
			prayerActive[p] = false;
			getPA().sendFrame36(PRAYER_GLOW[p], 0);
		}
		correctCoordinates();

		getPA().handleWeaponStyle();
		accountFlagged = getPA().checkForFlags();
		getPA().sendFrame36(108, 0);
		getPA().sendFrame36(173, 0);
		getPA().sendFrame107();
		getPA().setChatOptions(0, 0, 0);
		setSidebarInterface(1, 3917);
		setSidebarInterface(2, 638);
		setSidebarInterface(3, 3213);
		setSidebarInterface(4, 1644);
		setSidebarInterface(5, 5608);
		if (playerMagicBook == 0) {
			setSidebarInterface(6, 1151);
		} else {
			if (playerMagicBook == 2) {
				setSidebarInterface(6, 29999);
			} else {
				setSidebarInterface(6, 12855);
			}
		}

		setSidebarInterface(8, 5065);
		setSidebarInterface(9, 5715);
		setSidebarInterface(10, 2449);
		setSidebarInterface(11, 904);
		setSidebarInterface(12, 147);
		setSidebarInterface(13, -1);
		setSidebarInterface(0, 2423);

		setText();
		getPA().sendFrame36(173, 0);
		sendMessage("Welcome to 05Prime Alpha.");
		//sendMessage("You can now use ::Setlevel as a player.");
		getPA().showOption(4, 0, "Trade With", 3);
		getPA().showOption(5, 0, "Follow", 4);
		getItems().resetItems(3214);
		calculateCombatLevel();
		getItems().sendWeapon(playerEquipment[playerWeapon],
				getItems().getItemName(playerEquipment[playerWeapon]));
		getItems().resetBonus();
		getItems().getBonus();
		getItems().writeBonus();
		getItems().setEquipment(playerEquipment[playerHat], 1, playerHat);
		getItems().setEquipment(playerEquipment[playerCape], 1, playerCape);
		getItems().setEquipment(playerEquipment[playerAmulet], 1, playerAmulet);
		getItems().setEquipment(playerEquipment[playerArrows],
				playerEquipmentN[playerArrows], playerArrows);
		getItems().setEquipment(playerEquipment[playerChest], 1, playerChest);
		getItems().setEquipment(playerEquipment[playerShield], 1, playerShield);
		getItems().setEquipment(playerEquipment[playerLegs], 1, playerLegs);
		getItems().setEquipment(playerEquipment[playerHands], 1, playerHands);
		getItems().setEquipment(playerEquipment[playerFeet], 1, playerFeet);
		getItems().setEquipment(playerEquipment[playerRing], 1, playerRing);
		getItems().setEquipment(playerEquipment[playerWeapon],
				playerEquipmentN[playerWeapon], playerWeapon);
		getCombat().getPlayerAnimIndex(
				getItems().getItemName(playerEquipment[playerWeapon])
						.toLowerCase());
		getPA().logIntoPM();
		getItems().addSpecialBar(playerEquipment[playerWeapon]);
		Utilities.println("The account of the user " + Utilities.formatPlayerName(playerName) + " has been accepted.");
		handler.updatePlayer(this, outStream);
		handler.updateNPC(this, outStream);
		isFullHelm = ItemClassification.isFullHelm(playerEquipment[playerHat]);
		isFullMask = ItemClassification.isFullMask(playerEquipment[playerHat]);
		isFullBody = ItemClassification.isFullBody(playerEquipment[playerChest]);
		flushOutStream();

		if (addStarter) {
			getPA().addStarter();
		}
		
		
		getPA().sendFrame36(172, autoRet == 1 ? 0 : 1);

		if (getPets().getPetIndex() > 0) {
			getPets().dropPet(getPets().getPetIndex(), true);
		}
	}

	@Override
	public void update() {
		handler.updatePlayer(this, outStream);
		handler.updateNPC(this, outStream);
		flushOutStream();
	}

	public void logout() {
		if (System.currentTimeMillis() - logoutDelay > 10000) {
			outStream.createFrame(109);
			properLogout = true;
			PlayerAccount.saveGame(this);
		} else {
			sendMessage("You must wait a few seconds from being out of combat to logout.");
		}
	}

	public int packetSize = 0, packetType = -1;

	public int hitpoints1;
	public int hitpoints;

	public int spawnNpc;

	public boolean isUsingSpecial;

	@Override
	public void process() {

		updateInterfaces();
		
		if (followId > 0) {
			getPA().followPlayer();
		} else if (followId2 > 0) {
			getPA().followNpc();
		}
        
		if (System.currentTimeMillis() - lastPoison > 20000 && poisonDamage > 0) {
			int damage = poisonDamage / 2;
			if (damage > 0) {
				sendMessage("The poison damages you");
				if (!getHitUpdateRequired()) {
					setHitUpdateRequired(true);
					setHitDiff(damage);
					updateRequired = true;
					poisonMask = 1;
				} else if (!getHitUpdateRequired2()) {
					setHitUpdateRequired2(true);
					setHitDiff2(damage);
					updateRequired = true;
					poisonMask = 2;
				}
				lastPoison = System.currentTimeMillis();
				poisonDamage--;
				dealDamage(damage);
			} else {
				poisonDamage = -1;
				sendMessage("You are no longer poisoned.");
			}
		}
		if (System.currentTimeMillis() - duelDelay > 800 && duelCount > 0) {
			if (duelCount != 1) {
				forcedChat("" + (--duelCount));
				duelDelay = System.currentTimeMillis();
			} else {
				damageTaken = new int[Constants.MAX_PLAYERS];
				forcedChat("FIGHT!");
				duelCount = 0;
			}
		}
		if (System.currentTimeMillis() - specDelay > Constants.INCREASE_SPECIAL_AMOUNT) {
			specDelay = System.currentTimeMillis();
			if (specAmount < 10) {
				specAmount += .5;
				if (specAmount > 10)
					specAmount = 10;
				getItems().addSpecialBar(playerEquipment[playerWeapon]);
			}
		}
		if (clickObjectType > 0
				&& goodDistance(objectX + objectXOffset, objectY
						+ objectYOffset, getX(), getY(), objectDistance)) {
			if (clickObjectType == 1) {
				getActions().firstClickObject(objectId, objectX, objectY);
			}
			if (clickObjectType == 2) {
				getActions().secondClickObject(objectId, objectX, objectY);
			}
			if (clickObjectType == 3) {
				getActions().thirdClickObject(objectId, objectX, objectY);
			}
		}
		if ((clickNpcType > 0) && NPCHandler.npcs[npcClickIndex] != null) {
			if (goodDistance(getX(), getY(),
					NPCHandler.npcs[npcClickIndex].getX(),
					NPCHandler.npcs[npcClickIndex].getY(), 1)) {
				if (clickNpcType == 1) {
					turnPlayerTo(NPCHandler.npcs[npcClickIndex].getX(),
							NPCHandler.npcs[npcClickIndex].getY());
					NPCHandler.npcs[npcClickIndex].facePlayer(playerId);
					getActions().firstClickNpc(npcType);
				}
				if (clickNpcType == 2) {
					turnPlayerTo(NPCHandler.npcs[npcClickIndex].getX(),
							NPCHandler.npcs[npcClickIndex].getY());
					NPCHandler.npcs[npcClickIndex].facePlayer(playerId);
					getActions().secondClickNpc(npcType);
				}
				if (clickNpcType == 3) {
					turnPlayerTo(NPCHandler.npcs[npcClickIndex].getX(),
							NPCHandler.npcs[npcClickIndex].getY());
					NPCHandler.npcs[npcClickIndex].facePlayer(playerId);
					getActions().thirdClickNpc(npcType);
				}
			}
		}
		if (walkingToItem) {
			if (getX() == pItemX && getY() == pItemY
					|| goodDistance(getX(), getY(), pItemX, pItemY, 1)) {
				walkingToItem = false;
				Application.itemHandler.removeGroundItem(this, pItemId, pItemX,
						pItemY, true);
			}
		}

		getCombat().handlePrayerDrain();
		if (System.currentTimeMillis() - singleCombatDelay > 3300) {
			underAttackBy = 0;
		}
		if (System.currentTimeMillis() - singleCombatDelay2 > 3300) {
			underAttackBy2 = 0;
		}
		if (System.currentTimeMillis() - restoreStatsDelay > 60000) {
			restoreStatsDelay = System.currentTimeMillis();
			for (int level = 0; level < playerLevel.length; level++) {
				if (playerLevel[level] < getLevelForXP(playerXP[level])) {
					if (level != 5) {
						playerLevel[level] += 1;
						getPA().setSkillLevel(level, playerLevel[level],
								playerXP[level]);
						getPA().refreshSkill(level);
					}
				} else if (playerLevel[level] > getLevelForXP(playerXP[level])) {
					playerLevel[level] -= 1;
					getPA().setSkillLevel(level, playerLevel[level],
							playerXP[level]);
					getPA().refreshSkill(level);
				}
			}
		}
		if (System.currentTimeMillis() - teleGrabDelay > 1550 && usingMagic) {
			usingMagic = false;
			if (Application.itemHandler.itemExists(teleGrabItem, teleGrabX,
					teleGrabY)) {
				Application.itemHandler.removeGroundItem(this, teleGrabItem,
						teleGrabX, teleGrabY, true);
			}
		}
		if (skullTimer > 0) {
			skullTimer--;
			if (skullTimer == 1) {
				isSkulled = false;
				attackedPlayers.clear();
				headIconPk = -1;
				skullTimer = -1;
				getPA().requestUpdates();
			}
		}
		if (isDead && respawnTimer == -6) {
			getPA().applyDead();
		}
		if (respawnTimer == 7) {
			respawnTimer = -6;
			getPA().giveLife();
		} else if (respawnTimer == 12) {
			respawnTimer--;
			startAnimation(0x900);
			poisonDamage = -1;
		}
		if (respawnTimer > -6) {
			respawnTimer--;
		}
		if (freezeTimer > -6) {
			freezeTimer--;
			if (frozenBy > 0) {
				if (PlayerHandler.players[frozenBy] == null) {
					freezeTimer = -1;
					frozenBy = -1;
				} else if (!goodDistance(absX, absY,
						PlayerHandler.players[frozenBy].absX,
						PlayerHandler.players[frozenBy].absY, 20)) {
					freezeTimer = -1;
					frozenBy = -1;
				}
			}
		}
		if (hitDelay > 0) {
			hitDelay--;
		}
		if (teleTimer > 0) {
			teleTimer--;
			if (!isDead) {
				if (teleTimer == 1 && newLocation > 0) {
					teleTimer = 0;
					getPA().changeLocation();
				}
				if (teleTimer == 5) {
					teleTimer--;
					getPA().processTeleport();
				}
				if (teleTimer == 9 && teleGfx > 0) {
					teleTimer--;
					gfx100(teleGfx);
				}
			} else {
				teleTimer = 0;
			}
		}
		if (hitDelay == 1) {
			if (oldNpcIndex > 0) {
				getCombat().delayedHit(oldNpcIndex);
			}
			if (oldPlayerIndex > 0) {
				getCombat().playerDelayedHit(oldPlayerIndex);
			}
		}
		if (attackTimer > 0) {
			attackTimer--;
		}
		if (attackTimer == 1) {
			if (npcIndex > 0 && clickNpcType == 0) {
				getCombat().attackNpc(npcIndex);
			}
			if (playerIndex > 0) {
				getCombat().attackPlayer(playerIndex);
			}
		} else if (attackTimer <= 0 && (npcIndex > 0 || playerIndex > 0)) {
			if (npcIndex > 0) {
				attackTimer = 0;
				getCombat().attackNpc(npcIndex);
			} else if (playerIndex > 0) {
				attackTimer = 0;
				getCombat().attackPlayer(playerIndex);
			}
		}
		if (timeOutCounter > Constants.TIMEOUT) {
			disconnected = true;
		} else {
			timeOutCounter++;
		}
		if (inTrade && tradeResetNeeded) {
			Client o = (Client) PlayerHandler.players[tradeWith];
			if (o != null) {
				if (o.tradeResetNeeded) {
					getTradeAndDuel().resetTrade();
					o.getTradeAndDuel().resetTrade();
				}
			}
		}
	}

	public void setCurrentTask(Future<?> task) {
		currentTask = task;
	}

	public Future<?> getCurrentTask() {
		return currentTask;
	}

	public synchronized Stream getInStream() {
		return inStream;
	}

	public synchronized int getPacketType() {
		return packetType;
	}

	public synchronized int getPacketSize() {
		return packetSize;
	}

	public synchronized Stream getOutStream() {
		return outStream;
	}

	public ItemAssistant getItems() {
		return itemAssistant;
	}

	public PlayerAssistant getPA() {
		return playerAssistant;
	}

	public DialogueHandler getDH() {
		return dialogueHandler;
	}

	public ShopAssistant getShops() {
		return shopAssistant;
	}

	public TradingAndDueling getTradeAndDuel() {
		return tradeAndDuel;
	}

	public Combat getCombat() {
		return combatAssistant;
	}

	public EntityInteractions getActions() {
		return actionHandler;
	}

	public IoSession getSession() {
		return session;
	}

	public PotionConsumption getPotions() {
		return potions;
	}

	public PotionCombination getPotMixing() {
		return potionMixing;
	}

	public Edibles getFood() {
		return food;
	}

	public Pets getPets() {
		return pets;
	}

	public void queueMessage(Packet arg1) {
		synchronized (queuedPackets) {
			queuedPackets.add(arg1);
		}
	}

	@Override
	public synchronized boolean processQueuedPackets() {
		Packet p = null;
		synchronized (queuedPackets) {
			p = queuedPackets.poll();
		}
		if (p == null) {
			return false;
		}
		inStream.currentOffset = 0;
		packetType = p.getId();
		packetSize = p.getLength();
		inStream.buffer = p.getData();
		if (packetType > 0) {
			PacketRegistration.processPacket(this, packetType, packetSize);
		}
		timeOutCounter = 0;
		return true;
	}

	public synchronized boolean processPacket(Packet p) {
		synchronized (this) {
			if (p == null) {
				return false;
			}
			inStream.currentOffset = 0;
			packetType = p.getId();
			packetSize = p.getLength();
			inStream.buffer = p.getData();
			if (packetType > 0) {
				PacketRegistration.processPacket(this, packetType, packetSize);
			}
			timeOutCounter = 0;
			return true;
		}
	}

	public void correctCoordinates() {

		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				container.stop();
			}

			@Override
			public void stop() {
				if (inPcGame() || inPcBoat()) {
					getPA().movePlayer(2657, 2639, 0);
				}
				if (inBarrowsWaves()) {
					getPA().movePlayer(3086, 3488, 0);
				}
				
				if (arenas()) {
					getPA().movePlayer(3365, 3268, 0);
				}
			}
		}, 2);
	}

	public void hit(int damage) {
		setHitDiff2(damage);
		setHitUpdateRequired2(true);
		playerLevel[3] -= 4;
		getPA().refreshSkill(damage);
	}

	public void updateInterfaces() {
		
		if (inWild()) {
			int modY = absY > 6400 ? absY - 6400 : absY;
			wildLevel = (((modY - 3520) / 8) + 1) + 4;
			getPA().walkableInterface(197);

			getPA().sendFrame126("@yel@Level: " + wildLevel, 199);
			
			getPA().showOption(3, 0, "Attack", 1);

		} else if (inDuelArena()) {
			getPA().walkableInterface(201);
			if (duelStatus == 5) {
				getPA().showOption(3, 0, "Attack", 1);
			} else {
				getPA().showOption(3, 0, "Challenge", 1);
			}
		} else if (inCwGame || inPits) {
			getPA().showOption(3, 0, "Attack", 1);
		} else if (getPA().inPitsWait()) {
			getPA().showOption(3, 0, "Null", 1);
		} else if (!inCwWait && !isFading) {
			getPA().sendFrame99(0);
			getPA().walkableInterface(-1);
			getPA().showOption(3, 0, "Null", 1);
		}
		if (Constants.MULTI_SIGN_ENABLED) {
			
			if (!hasMultiSign && inMulti()) {
				hasMultiSign = true;
				getPA().multiWay(1);
			}
			if (hasMultiSign && !inMulti()) {
				hasMultiSign = false;
				getPA().multiWay(-1);
			}
		}
	}
}