package com.runescape.model.npc;

public class NPCDefinition {
	
	public int npcId;
	
	public String npcName;
	
	public int npcCombat;
	
	public int npcHealth;

	public NPCDefinition(int _npcId) {
		npcId = _npcId;
	}
}
