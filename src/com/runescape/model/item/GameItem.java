package com.runescape.model.item;

public final class GameItem {
	
	public int id, amount;
	
	public boolean stackable = false;

	public GameItem(int id, int amount) {
		
		if (ItemClassification.ITEM_IS_STACKABLE[id]) {
			
			stackable = true;
		}
		
		this.id = id;
		
		this.amount = amount;
	}
}