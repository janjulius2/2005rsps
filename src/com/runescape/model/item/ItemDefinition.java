package com.runescape.model.item;

public class ItemDefinition {
	
	public int itemId;
	
	public String itemName;
	
	public String itemDescription;
	
	public double ShopValue;
	
	public double LowAlch;
	
	public double HighAlch;
	
	public int[] Bonuses = new int[100];

	public ItemDefinition(int _itemId) {
		
		itemId = _itemId;
	}
}
