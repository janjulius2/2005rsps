package com.runescape.model.item;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.runescape.Application;
import com.runescape.Constants;

public class ItemClassification {
	
	public static final boolean[] ITEM_IS_STACKABLE = new boolean[Constants.ITEM_LIMIT];

	public static final boolean[] ITEM_IS_NOTE = new boolean[Constants.ITEM_LIMIT];

	public static final int[] TARGET_SLOTS = new int[Constants.ITEM_LIMIT];

	public static boolean isFullBody(int itemId) {

		String weapon = getItemName(itemId);

		if (weapon == null) {

			return false;
		}
		for (int i = 0; i < ItemConstants.FULL_BODY.length; i++) {

			if (weapon.endsWith(ItemConstants.FULL_BODY[i])) {

				return true;
			}
		}
		return false;
	}

	public static boolean isFullHelm(int itemId) {

		String weapon = getItemName(itemId);

		if (weapon == null) {

			return false;
		}
		for (int i = 0; i < ItemConstants.FULL_HAT.length; i++) {

			if (weapon.endsWith(ItemConstants.FULL_HAT[i])) {

				return true;
			}
		}
		return false;
	}

	public static boolean isFullMask(int itemId) {
		String weapon = getItemName(itemId);

		if (weapon == null) {

			return false;
		}
		for (int i = 0; i < ItemConstants.FULL_MASK.length; i++) {

			if (weapon.endsWith(ItemConstants.FULL_MASK[i])) {

				return true;
			}
		}
		return false;
	}

	public static String getItemName(int id) {
		
		for (int j = 0; j < Application.itemHandler.ItemList.length; j++) {

			if (Application.itemHandler.ItemList[j] != null) {

				if (Application.itemHandler.ItemList[j].itemId == id) {

					return Application.itemHandler.ItemList[j].itemName;
				}
			}
		}
		return null;
	}

	static {
		int counter = 0;
		int accumulator;

		try {

			FileInputStream dataIn = new FileInputStream(new File("./Data/data/stackable.dat"));

			while ((accumulator = dataIn.read()) != -1) {
				if (accumulator == 0) {

					ITEM_IS_STACKABLE[counter] = false;
				} else {

					ITEM_IS_STACKABLE[counter] = true;
				}

				counter++;
			}
			dataIn.close();
		} catch (IOException exception) {

			exception.printStackTrace();
		}
		counter = 0;
		try {

			FileInputStream dataIn = new FileInputStream(new File("./Data/data/notes.dat"));

			while ((accumulator = dataIn.read()) != -1) {
				if (accumulator == 0) {

					ITEM_IS_NOTE[counter] = true;
				} else {

					ITEM_IS_NOTE[counter] = false;
				}
				counter++;
			}
			dataIn.close();
		} catch (IOException exception) {
			System.out.println("Critical error while loading notedata! Trace:");

			exception.printStackTrace();
		}
		counter = 0;
		try {
			FileInputStream dataIn = new FileInputStream(new File("./Data/data/equipment.dat"));
			while ((accumulator = dataIn.read()) != -1) {

				TARGET_SLOTS[counter++] = accumulator;
			}
			dataIn.close();
		} catch (IOException exception) {

			System.out.println("Critical error while loading notedata! Trace:");

			exception.printStackTrace();
		}
	}
}