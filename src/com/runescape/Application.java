package com.runescape;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.mina.common.IoAcceptor;
import org.apache.mina.transport.socket.nio.SocketAcceptor;
import org.apache.mina.transport.socket.nio.SocketAcceptorConfig;

import com.runescape.model.npc.NPCDrop;
import com.runescape.model.npc.NPCHandler;
import com.runescape.model.player.Client;
import com.runescape.network.ConnectionHandler;
import com.runescape.network.ConnectionValidation;
import com.runescape.world.ItemHandler;
import com.runescape.world.ObjectHandler;
import com.runescape.world.PlayerHandler;
import com.runescape.world.ShopHandler;
import com.runescape.world.cache.region.ObjectDef;
import com.runescape.world.cache.region.Region;
import com.runescape.world.process.event.CycleEventHandler;
import com.runescape.world.process.task.TaskScheduler;
import com.runescape.world.process.task.impl.EntityUpdateTask;
import com.runescape.world.process.task.impl.WorldUpdateTask;
import com.runescape.world.utilities.DestructionHook;

public final class Application {

	public static boolean update = false;

	private static IoAcceptor acceptor;

	private static ConnectionHandler connectionHandler;

	public static final ItemHandler itemHandler = new ItemHandler();

	public static final PlayerHandler playerHandler = new PlayerHandler();

	public static final NPCHandler npcHandler = new NPCHandler();

	public static ShopHandler shopHandler = new ShopHandler();

	public static final ObjectHandler objectHandler = new ObjectHandler();

	public static final NPCDrop npcDrops = new NPCDrop();

	public static final TaskScheduler scheduler = new TaskScheduler();

	private static final SocketAcceptorConfig socketAcceptor = new SocketAcceptorConfig();

	public static final void main(String... commandLine) throws IOException {

		ObjectDef.loadConfig();

		Region.load();

		ConnectionValidation.initialize();

		final InetSocketAddress address = new InetSocketAddress(Constants.SERVER_ADDRESS);

		Runtime.getRuntime().addShutdownHook(new DestructionHook());

		bindNetworking(address);

		submitUpdateTasks();

		System.out.println("Networking has been fixed to the address : [" + address.toString() + "].");

		System.out.println(scheduler.getTasks().size() + " task(s) have been submitted into the Task-Scheduler.");

		System.out.println(CycleEventHandler.getSingleton().getEventsCount() + " event(s) have been submitted into the Event-Handler.");
		
		System.out.println(Runtime.getRuntime().availableProcessors() + " core(s) are available and ready for use.");

		System.out.println("05Prime has successfully initialized and is now ready for connections.");
	}

	public static final void bindNetworking(InetSocketAddress address) throws IOException {
	
		connectionHandler = new ConnectionHandler();
		socketAcceptor.getSessionConfig().setTcpNoDelay(false);
		socketAcceptor.setReuseAddress(true);
		socketAcceptor.setBacklog(100);
		
		acceptor = new SocketAcceptor();
		acceptor.bind(address, connectionHandler, socketAcceptor);
	}

	public static final void submitUpdateTasks() {

		scheduler.schedule(new EntityUpdateTask());

		scheduler.schedule(new WorldUpdateTask());
	}

	public static void yell(String message) {

		for (int i = 0; i < PlayerHandler.players.length; i++) {

			if (PlayerHandler.players[i] != null) {

				Client player = (Client) PlayerHandler.players[i];

				player.sendMessage(message);
			}
		}
	}
}